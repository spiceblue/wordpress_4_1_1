<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="style.css">
  <style>
  body{background:#f5f7fa !important;}
  .table-striped>tbody>tr:nth-of-type(odd){background-color: #fff !important;}
  table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after{bottom: 15px !important;}
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding: 15px !important;}
.table-bordered>thead>tr>td, .table-bordered>thead>tr>th{border-bottom-width: 0px !important;}
a{text-decoration:none !important;}
a:hover{text-decoration:none !important;} 
a:visited{text-decoration:none !important;}
  </style>
  
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

</head>
<body>

<div style="background: linear-gradient(153.92deg, #4c3380 0%, #082D5B 50%); padding-bottom:200px;">

<div class="container">    
  <div class="row mgt5">
   
    <div class="col-sm-6 logo">
      <a href="index.html">Dashboard</a>
    </div>
	
    <div class="col-sm-6 logout">
	<a href="#">Logout</a>
    </div>
	
  </div>
</div>

<div class="container">    
<div class="row mgt4">
<div class="col-sm-12">
<div class="title">Spiceblue</div>

<ul id="mn">
  <li><a href="result.php">Email List</a></li>
  <li><a href="rfp.php">RFP</a></li>
</ul> 

</div>
</div>
</div>
  
</div>

<?php
$servername = "localhost";
$username = "root";
$password = "rootp";
$dbname = "spicexm8_wp888";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$result = $conn->query("SELECT COUNT(*) FROM sb_emails");
$row = $result->fetch_row();
?>


<div class="container" style="margin-top:-170px; background-color:#FFFFFF; padding:50px; margin-bottom:100px;">
<div class="row">
<div class="col-sm-12">

<div class="page-title mgb3"><span class="blk">Page signups</span> (<?php echo $row[0]; ?>)</div>

<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
        <thead>
            <tr class="tbl-title" style="background-color:#ebeff5;">
                <th>Company</th>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile no</th>
                <th>Budget</th>
				<th>Known By</th>
				<th>Details</th>
            </tr>
        </thead>
        <tbody>
		
<?php

$sql = "SELECT * FROM sb_emails";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
  ?>
		
            <tr>
                <td class="tb-data"><?php echo $row["company"]; ?></td>
                <td class="tb-data"><?php echo $row["name"]; ?></td>
                <td class="tb-data"><?php echo $row["email"]; ?></td>
                <td class="tb-data"><?php echo $row["phone"]; ?></td>
                <td class="tb-data"><?php echo $row["budget"]; ?></td>
				<td class="tb-data"><?php echo $row["discover"]; ?></td>
				<td class="tb-data"><a href="details.php?email=<?php echo $row["email"] ?>">View</a></td>
            </tr>
<?php			
  }
} else {
    echo "0 results";
}
$conn->close();
?>			

        </tbody>
    </table>

</div>
</div>
</div>


</body>
</html>
