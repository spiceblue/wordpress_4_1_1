<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="style.css">
  <style>
  body{background:#f5f7fa !important;}
  .table-striped>tbody>tr:nth-of-type(odd){background-color: #fff !important;}
  table.dataTable thead .sorting:after, table.dataTable thead .sorting_asc:after, table.dataTable thead .sorting_desc:after, table.dataTable thead .sorting_asc_disabled:after, table.dataTable thead .sorting_desc_disabled:after{bottom: 15px !important;}
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{padding: 15px !important;}
.table-bordered>thead>tr>td, .table-bordered>thead>tr>th{border-bottom-width: 0px !important;}
  </style>
  
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

</head>
<body>

<div style="background: #082D5B; padding-bottom:200px;">
<div class="container">    
<div class="row mgt2">
<div class="col-sm-12">
<div class="title">All Time Design</div>

<ul id="mn">
  <li><a href="atd.php">Home</a></li>
  <li><a href="pricing.php">Pricing Page</a></li>
  <li><a href="design.php">Design Day Page</a></li>
</ul> 

</div>
</div>
</div>
  
</div>

<?php
$servername = "spiceblue.cyynsionfwhu.us-west-2.rds.amazonaws.com";
$username = "wordpressuser";
$password = "root12345";
$dbname = "wordpressdb";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$result = $conn->query("SELECT COUNT(*) FROM wp_emails where page='pricing' and email != ''");
$row = $result->fetch_row();
?>


<div class="container" style="margin-top:-170px; background-color:#FFFFFF; padding:50px; margin-bottom:100px;">
<div class="row">
<div class="col-sm-12">

<div class="page-title mgb3"><span class="blk">Pricing Page signups</span> (<?php echo $row[0]; ?>)</div>

<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
        <thead>
            <tr class="tbl-title" style="background-color:#ebeff5;">
                <th>Email</th>
                <th>Date & Time</th>
                <th>IP Address</th>
                <th>Country</th>
            </tr>
        </thead>
        <tbody>
		
<?php

$sql = "SELECT * FROM wp_emails where page='pricing' and email != ''";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
  ?>
		
            <tr>
                <td class="tb-data"><?php echo $row["email"]; ?></td>
                <td class="tb-data"><?php echo $row["dtime"]; ?></td>
                <td class="tb-data"><?php echo $row["ip"]; ?></td>
                <td class="tb-data"><?php echo $row["country"]; ?></td>
            </tr>
<?php			
  }
} else {
    echo "0 results";
}
$conn->close();
?>			

        </tbody>
    </table>

</div>
</div>
</div>


</body>
</html>
