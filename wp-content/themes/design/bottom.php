<script  src="<?php echo get_template_directory_uri(); ?>/assets/js/popup.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.11.1.min.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/custom_spiceblue.js"></script>  
 <script src='<?php echo get_template_directory_uri(); ?>/assets/js/nprogress.js'></script>
 
 <script src='<?php echo get_template_directory_uri(); ?>/assets/js/jquery-2.2.4.min.js'></script>
<script src='<?php echo get_template_directory_uri(); ?>/assets/js/select2.full.js'></script>
<script  src="<?php echo get_template_directory_uri(); ?>/assets/js/select2.js"></script>
 
<script>
jQuery(document).ready(function($){
	var logouturl =  '<?php echo admin_url( 'admin-ajax.php' ); ?>';
	$('.looutuser').on('click', function(){		
		$.ajax({
			type:'post',
			url:logouturl,
			data:{
				action:'logout_user'
			},
			success:function(data){
				location.reload();
			}
		})
	});
});
</script>
</body>
</html>