<?php
/**
 * Template Name: Demo Page
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage All Time Design
 * @since All Time Design 2.0
 */

//get_header(); ?>



<!DOCTYPE html>
<html>
<head>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php wp_title(''); ?></title>
  <?php do_action( 'wpseo_head' );  ?>
  <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png"/>
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/SF-Pro-Display-Regular.otf" as="font" type="font/otf" crossorigin>
	  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/navbar.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/default.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/additional.css" type="text/css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/popup.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/other-pages.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/base.css" data-rel-css="" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/example2.css" data-rel-css="" />
  <link href='<?php echo get_template_directory_uri(); ?>/assets/css/nprogress.css' rel='stylesheet' />
  <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script> 
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/other-pages.js"></script>
  <link rel='stylesheet prefetch' href='<?php echo get_template_directory_uri(); ?>/assets/css/select2.min.css'>
  <!--slide up-->
  
  <!---lightbox--->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/hes-gallery.css">

  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css" type="text/css" />
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js" type="text/javascript"></script>
  <script src="https://js.stripe.com/v3/"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/index.js" data-rel-js></script>
  <!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P2NJWB5');</script>
	<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P2NJWB5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<section class="menu design-menu">
    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
  <div class="container dis">
  
  <?php if ( is_user_logged_in() ) { 
  $current_user = wp_get_current_user();
	$customer_status = get_user_meta( $current_user->ID, 'status', true);
  
	  if ( $customer_status == 'active' ){
			echo '<div class="only-mobile">
		  <a class="btn btn-sm btn-primary display-4 btn-top" href="'.site_url('dashboard').'">Make Request</a>
		  </div>';
			
		}else{
			echo '<div class="only-mobile">
		  <a class="btn btn-sm btn-primary display-4 btn-top" href="'.site_url('pricing/').'" >Get Started</a>
		  </div>';
		}
	}else{
		echo '<div class="only-mobile">
      <a class="btn btn-sm btn-primary display-4 btn-top" href="'.site_url('pricing/').'" >Get Started</a>
      </div>';
	}
  
  ?> 
  
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-caption-wrap">
        <a class="navbar-caption text-white display-4" href="<?php echo get_home_url(); ?>">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/Logo.svg">
		</a></span>
            </div>
        </div>

 <?php if ( is_user_logged_in() ) { ?>   
<div class="collapse navbar-collapse mg2" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
      
      
	  <?php 
	  $current_user = wp_get_current_user();
	$customer_status = get_user_meta( $current_user->ID, 'status', true);
	if ( $customer_status == 'active' ){
		echo '<li class="nav-item">
      <a class="nav-link link text-black display-4" href="'.site_url('dashboard').'" aria-expanded="true">Dashboard</a>
      </li>';
		
	}else{
		echo '<li class="nav-item">
      <a class="nav-link link text-black display-4" href="'.site_url('pricing/').'" aria-expanded="true">Pricing</a>
      </li>';
	}
	  ?>
            <li class="nav-item">
      <a class="nav-link link text-black display-4" href="<?php echo site_url('works/'); ?>" aria-expanded="false">Works</a>
      </li>
      <li class="nav-item pr1">
      <a class="nav-link link text-black display-4" href="<?php echo site_url('contact/'); ?>" aria-expanded="false">Contact</a>
      </li>
      <li class="nav-item">
		<a  class="nav-link link text-black display-4 looutuser"  aria-expanded="false">Log out</a>
      </li>
      </ul>
      <?php 
		$style_prc = ( get_the_ID() == 58 ) ? 'style="visibility: hidden;"' : '';
		if ( $customer_status == 'active' ){
			echo '<div class="navbar-buttons design-section-btn" '.$style_prc.'><a class="btn btn-sm btn-primary display-4" href="'.site_url('dashboard').'">Make Request</a></div>';		
		}else{
			echo '<div class="navbar-buttons design-section-btn" '.$style_prc.' ><a class="btn btn-sm btn-primary display-4" href="'.site_url('pricing/').'" >Get Started</a></div>';
		}
	  ?>   
      
</div>
<?php } else { ?>
<div class="collapse navbar-collapse mg2" id="navbarSupportedContent">
    <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
        <li class="nav-item">
		  <a class="nav-link link text-black display-4" href="<?php echo site_url('pricing/'); ?>" aria-expanded="true">Pricing</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link link text-black display-4" href="<?php echo site_url('works/'); ?>" aria-expanded="false">Works</a>
		</li>
		<li class="nav-item pr1">
		  <a class="nav-link link text-black display-4" href="<?php echo site_url('contact/'); ?>" aria-expanded="false">Contact</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link link text-black display-4" href="<?php echo site_url('sign-in/'); ?>" aria-expanded="false">Sign in</a>
		</li>
	</ul>
	<div class="navbar-buttons design-section-btn" <?php if( get_the_ID() == 58 ){  ?> style="visibility: hidden;" <?php } ?>>
		<a class="btn btn-sm btn-primary display-4" href="<?php echo site_url('pricing/'); ?>">Get Started</a>
	</div>
</div>
<?php } ?>

</div>
</nav>
</section>


<?php /*?><section class="header1 design-parallax many-parallax-background">
    <div class="container">
        <div class="row">
            <div class="many-white col-md-10">
                <h1 class="many-section-title align-center many-bold pb-3 many-fonts-style display-1">
<!--<span class="banner-txt">We are All Time Design</span><br>-->
Get unlimited graphic designs for a flat monthly fee</h1>
                
<div class="navbar-buttons design-section-btn mgb1">
<a class="btn btn-sm btn-primary display-4" href="<?php echo site_url(); ?>/pricing/">Get Started</a>
</div>
               </div>
        </div>
    </div>
</section><?php */?>
<style>
.container-fluid{
    padding-left: 0rem;
    padding-right: 0rem;
    overflow: hidden;
}
.dm-title1{color: #000811;	font-family: "SF Pro Display";	font-size: 50px;	font-weight: 900;	line-height: 66px; padding-top:200px;}
#dm ul li{background: url('<?php echo get_template_directory_uri(); ?>/assets/images/list.png') no-repeat left top; padding-left: 30px; background-position: 0px 9px;}
ul li{line-height:35px;}
.mgt5{margin-top:250px;}
.btn-primary.focus, .btn-primary:focus{box-shadow:none;}
button.close{display:none;}
.modal-header{display: block;}
.modal-header{margin-left:0px;}
.frm-titl{color: #2C2F33; font-family: "SF Pro Display"; font-size: 16px; font-weight: 500; line-height: 19px; margin-bottom:10px; margin-top:20px;}
.mrg1{margin-left:40px; margin-right:40px;}
.modal-dialog{margin: 77px auto;}
.frm-ct{color: #000;	font-family: "SF Pro Display";	font-size: 15px !important; line-height: 18px;}
::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  color: #9E9FA5;
  font-size: 15px !important;
}
::-moz-placeholder { /* Firefox 19+ */
  color: #9E9FA5;
  font-size: 15px !important;
}
:-ms-input-placeholder { /* IE 10+ */
  color: #9E9FA5;
  font-size: 15px !important;
}
:-moz-placeholder { /* Firefox 18- */
  color: #9E9FA5;
  font-size: 15px !important;
}
input[type=email]:focus{border: 1px solid #0d83dd !important;}
.btn-sm{padding: 0px 20px;}

#counter {
    position: relative;
    display: inline;
}
#over {
    position:absolute;
    left:36%;
    top:40%;
}
.sec1{position:absolute; z-index:10;}
.sec1-mg{margin-top:70px;}
.testi-mg{margin-top:80px;}
@media screen and (max-width: 1024px) {
.dm-title1 {font-size: 40px; line-height: 50px; padding-top: 120px;}
.mgt5{margin-top: 100px;}
#over{left: 29%; top: 33%;}
.mgb4{margin-bottom: 100px;}
}

@media screen and (max-width: 768px) {
.sec1{position:relative;}
#over{left: 35%; top: 36%;}
.testi-txt1{padding-top: 10px; padding-left: 0px;}
.sec1-mg{margin-top:50px;}
}

@media screen and (max-width: 767px) {
#over{left: 23%; top: 24%;}
.dm-title1{font-size: 30px; line-height: 40px; padding-top: 50px;}
.mgt5{margin-top: 30px;}
.btn-sm1{margin-top: 30px;}
.testi-mg{margin-top:20px;}
.pdt3{padding-bottom: 40px;}
}
</style>

<section>
    <div class="container-fluid sec1">
        <div class="row">
            <div class="col-lg-6 col-md-12" id="dm">
               &nbsp;
            </div>
			<div class="col-lg-6 col-md-12 sec1-mg" id="counter">
			<a data-toggle="modal" data-target="#myModal" href="#">
			<img id="over" src="<?php echo get_template_directory_uri(); ?>/assets/images/play-button.png" alt="Play" />
			</a>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo.png" width="100%" />
            </div>
		</div>
	</div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12" id="dm" style="z-index:999 !important;">
                <div class="dm-title1">Curious About Our<br />All Time Design?</div>
				<ul style="padding-top:30px;">
				<li>Demos the core All Time Design graphic design service</li>
				<li>Shows exactly how to use the tool</li>
				<li>Talks about who is and isn’t a good fit for the service</li>
				<li>Explains why All Time Design was founded</li>
				<li>Recommends keeping your freelance designer!</li>
				</ul>
				<a class="btn btn-sm btn-primary display-4 btn-sm1" data-toggle="modal" data-target="#myModal" href="#">Watch Demo Now</a>
            </div>
			<div class="col-lg-6 col-md-12">
			&nbsp;
            </div>
		</div>
	</div>
</section>


<!-- The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Tell us about your self</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <form class="container" novalidate="" action="<?php echo site_url('video/'); ?>" method="POST" id="myForm">
		<div class="mrg1">
          <div class="frm-titl">Full Name</div>
		  <input type="text" class="form-control" name="fname" id="inputSuccess1" required>
		  <div class="frm-titl">Email</div>
		  <input type="email" class="form-control" name="email" required id="inputSuccess2">
        </div>
		<div style="margin:40px; margin-top:30px;">
		<button type="submit" class="btn btn-sm btn-primary display-4" href="javascript:void(0)" id="btnSubmit" style="width:100%;">Watch the demo</button>
		</div>
		</form>
		<script>
$("#btnSubmit").click(function(event) {

    // Fetch form to apply custom Bootstrap validation
    var form = $("#myForm")

    if (form[0].checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    }
    
    form.addClass('was-validated');
    // Perform ajax submit here...
    
});
</script>
      </div>
    </div>
  </div>
  

<section>
<div class="container mgt5"><div class="media-container-row" align="center">
		<div class="card p-3a col-12 col-md-12 col-lg-12 mgb4"  style="background-color:#FAFAFA; border-radius:50px;">
		<div class="testim testi-mg">
		All time design has made it quick and easy for our team to create a variety of eye-catching marketing materials. Being able to communicate directly with the designer about our needs, which helps to ensure that we get the best possible final product, every time
		</div>
		
<div class="pdt3" align="center" style="width:350px;">
<div style="float: left; width: 45%; text-align: right; padding-right: 20px;"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/10/22104254/Testimonials.png"></div>
<div style="float:right; width:55%; text-align: left; margin-top:4px;">
<div class="testi-txt" style="text-align:left !important;">by Olia Gozha</div><div class="testi-txt1" style="text-align:left !important;">Founder of SF</div>
</div>
</div>
		
		</div>
		


</div><hr /></div>
</section>

<section class="features1" style="background-color:#000000;">
    <div class="container">
	
	       
	    <div class="media-container-row mgb2 pdt1 pdb4">
             <div class="card col-6 col-md-3 col-lg-3">
                <a class="navbar-caption text-white display-4 mggt1" href="<?php echo get_home_url(); ?>">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/Logo_Footer.png">
		</a>
            </div>
			
			<div class="card col-6 col-md-3 col-lg-3">
                <div class="card-box four-box" >
                   <ul>
				   <li><a href="<?php echo site_url('scope/'); ?>">Scope of work</a></li>
				   <li><a href="<?php echo site_url('blog/'); ?>">Blog</a></li>
				   </ul>
                </div>
            </div>
			
			<div class="card col-6 col-md-3 col-lg-3">
                <div class="card-box four-box">
                   <ul>
				   <li><a href="<?php echo site_url('pricing/'); ?>">Pricing</a></li>
				   <li><a href="<?php echo site_url('contact/'); ?>">Contact</a></li>
				   </ul>
                </div>
            </div>			
			
            <div class="card col-6 col-md-3 col-lg-3">
                <div class="card-box four-box">
                    <ul>
				   <li><a href="<?php echo site_url('terms/'); ?>">Term & Conditions</a></li>
				   <li><a href="<?php echo site_url('privacy/'); ?>">Privacy Policy</a></li>
				   </ul>
                </div>
            </div>
			
			<!--<div class="card p-3a col-12 col-md-3 col-lg-3">
                <div class="card-box four-box">
				<div class="ft-title">Contact</div>
                    <ul>
				   <li><a href="mailto:hello@alltimedesign.com">hello@alltimedesign.com</a></li>
				   <li>+0 347 8739 943</li>
				   </ul>
                </div>
            </div>-->
			

        </div>
		
<div class="media-container-row pdb2">
<div class="card col-12 col-md-12 col-lg-12 footer-copy">
&copy; ALL TIME DESIGN
</div>
</div>
	
    </div>
</section>


<script type="text/javascript">
            new WOW().init();
        </script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/smooth-scroll.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/script.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jarallax.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/script.js"></script>
  <script  src="<?php echo get_template_directory_uri(); ?>/assets/js/popup.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/l10n.js" data-rel-js></script>
  <?php if( get_the_ID() == 58 ){ ?>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/example2.js" data-rel-js></script>
  <?php } ?>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/custom_spiceblue.js"></script>  
  <script src='<?php echo get_template_directory_uri(); ?>/assets/js/nprogress.js'></script>
  

<script src='<?php echo get_template_directory_uri(); ?>/assets/js/select2.full.js'></script>
<script  src="<?php echo get_template_directory_uri(); ?>/assets/js/select2.js"></script>

<!---lightbox--->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/hes-gallery.js"></script>

</body>
</html>  