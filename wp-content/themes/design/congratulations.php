<?php
/**
 * Template Name: Congratulations Page
 * The template for displaying pages
 *
 * This is the template that displays all Backend pages.
 *
 * @package WordPress
 * @subpackage All Time Design
 * @since All Time Design 1.0
 */
get_header();
?>
<style>
.cong-title{color: #333333;	font-family: "SF Pro Display";	font-size: 32px;	font-weight: 600;	letter-spacing: 0.27px;	line-height: 36px;	text-align: center; margin-top:-60px;}
.cong-cont{color: #333333;	font-family: "SF Pro Display";	font-size: 20px;	letter-spacing: 0.33px;	line-height: 31px;	text-align: center; padding-left:20%; padding-right:20%;
padding-top:20px;}
</style>
<section>
<div class="container"><div class="media-container-row pdlr1">

<div class="col-12 col-md-12 col-lg-12">
<div align="center">

<div><img src="<?php echo get_template_directory_uri(); ?>/assets/images/cong.png"></div>
<div class="cong-title">Congratulations!</div>
<div class="cong-cont">Your payment was successful and your<br />
all time design account credentials had<br />
been sent to your mail.<br /><br />

You can reach us at<br />
<a href="mailto:hello@alltimedesign.com">hello@alltimedesign.com</a> in case<br />
of any queries.</div>

</div>
</div>

</div></div>
</section>
