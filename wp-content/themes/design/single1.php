 <!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php wp_title(''); ?></title>
  <?php do_action( 'wpseo_head' );  ?>
  <link rel="shortcut icon" type="image/png" href="favicon.png"/>
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/navbar.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/default.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/additional.css" type="text/css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/popup.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/other-pages.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/base.css" data-rel-css="" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/example2.css" data-rel-css="" />
  <link href='<?php echo get_template_directory_uri(); ?>/assets/css/nprogress.css' rel='stylesheet' />
  <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script> 
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/other-pages.js"></script>
  <link rel='stylesheet prefetch' href='<?php echo get_template_directory_uri(); ?>/assets/css/select2.min.css'>
  <!--slide up-->
  
  <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.png">
		<script src="https://code.jquery.com/jquery-3.1.0.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel='stylesheet' id='dm-style-css'  href='<?php echo get_template_directory_uri(); ?>/blog/css/stylesf269.css?ver=1.0.1' type='text/css' media='all' />
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/blog/js/jquery-3.3.1.js'></script>

  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css" type="text/css" />
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js" type="text/javascript"></script>
  <script src="https://js.stripe.com/v3/"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/index.js" data-rel-js></script>
  <!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P2NJWB5');</script>
	<!-- End Google Tag Manager -->
	<style>body{overflow-y: scroll !important;}
	.background:before {
  content: '';
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background-image: linear-gradient(to bottom right, #000000, #000000);
  opacity: .6;
}
	</style>
</head>
<body>
 
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P2NJWB5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<section class="menu design-menu">
    <nav class="navbar navbar-expand beta-menu align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
  <div class="container dis">
  
  <?php if ( is_user_logged_in() ) { 
  $current_user = wp_get_current_user();
	$customer_status = get_user_meta( $current_user->ID, 'status', true);
  
	  if ( $customer_status == 'active' ){
			echo '<div class="only-mobile">
		  <a class="btn btn-sm btn-primary display-4 btn-top" href="'.site_url('dashboard').'">Make Request</a>
		  </div>';
			
		}else{
			echo '<div class="only-mobile">
		  <a class="btn btn-sm btn-primary display-4 btn-top" href="'.site_url('pricing/').'" >Get Started</a>
		  </div>';
		}
	}else{
		echo '<div class="only-mobile">
      <a class="btn btn-sm btn-primary display-4 btn-top" href="'.site_url('pricing/').'" >Get Started</a>
      </div>';
	}
  
  ?> 
  
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-caption-wrap">
        <a class="navbar-caption text-white display-4" href="<?php echo get_home_url(); ?>">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/Logo.svg">
		</a></span>
            </div>
        </div>

 <?php if ( is_user_logged_in() ) { ?>   
<div class="collapse navbar-collapse mg2" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
      
      
	  <?php 
	  $current_user = wp_get_current_user();
	$customer_status = get_user_meta( $current_user->ID, 'status', true);
	if ( $customer_status == 'active' ){
		echo '<li class="nav-item">
      <a class="nav-link link text-black display-4" href="'.site_url('dashboard').'" aria-expanded="true">Dashboard</a>
      </li>';
		
	}else{
		echo '<li class="nav-item">
      <a class="nav-link link text-black display-4" href="'.site_url('pricing/').'" aria-expanded="true">Pricing</a>
      </li>';
	}
	  ?>
            <li class="nav-item">
      <a class="nav-link link text-black display-4" href="<?php echo site_url('works/'); ?>" aria-expanded="false">Works</a>
      </li>
      <li class="nav-item pr1">
      <a class="nav-link link text-black display-4" href="<?php echo site_url('contact/'); ?>" aria-expanded="false">Contact</a>
      </li>
      <li class="nav-item">
		<a  class="nav-link link text-black display-4 looutuser"  aria-expanded="false">Log out</a>
      </li>
      </ul>
      <?php 
		$style_prc = ( get_the_ID() == 58 ) ? 'style="visibility: hidden;"' : '';
		if ( $customer_status == 'active' ){
			echo '<div class="navbar-buttons design-section-btn" '.$style_prc.'><a class="btn btn-sm btn-primary display-4" href="'.site_url('dashboard').'">Make Request</a></div>';		
		}else{
			echo '<div class="navbar-buttons design-section-btn" '.$style_prc.' ><a class="btn btn-sm btn-primary display-4" href="'.site_url('pricing/').'" >Get Started</a></div>';
		}
	  ?>   
      
</div>
<?php } else { ?>
<div class="collapse navbar-collapse mg2" id="navbarSupportedContent">
    <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
        <li class="nav-item">
		  <a class="nav-link link text-black display-4" href="<?php echo site_url('pricing/'); ?>" aria-expanded="true">Pricing</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link link text-black display-4" href="<?php echo site_url('works/'); ?>" aria-expanded="false">Works</a>
		</li>
		<li class="nav-item pr1">
		  <a class="nav-link link text-black display-4" href="<?php echo site_url('contact/'); ?>" aria-expanded="false">Contact</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link link text-black display-4" href="<?php echo site_url('sign-in/'); ?>" aria-expanded="false">Sign in</a>
		</li>
	</ul>
	<div class="navbar-buttons design-section-btn" <?php if( get_the_ID() == 58 ){  ?> style="visibility: hidden;" <?php } ?>>
		<a class="btn btn-sm btn-primary display-4" href="<?php echo site_url('pricing/'); ?>">Get Started</a>
	</div>
</div>
<?php } ?>

</div>
</nav>
</section>


<article>
<section>
<div class="intro__item-title navbar__hide-trigger">

<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>
<div class="post" id="post-<?php the_ID(); ?>">
	
<!-- HEADER -->
<section class="header inverse">
<div class="content">
		<div class="container">
			<div class="wrap align-center">
				<div class="fix-10-12 title-container">
					<div class="meta ae-1 fromTop"><strong class="categories"><a href="<?php echo get_site_url(); ?>/blog/?category_name=<?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?>" title="Spiceblue" class="post_tag category-ux-design" data-wpel-link="internal" style="color: #fff;"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></a></strong> <span style="color: #fff;"><?php  the_author(); ?></span></div>

					<h1 itemprop="headline" class="ae-2 fromAbove fix-9-12 superSlow" style="color: #fff;"><?php the_title(); ?></h1>
					
				</div></div></div>
			</div>
	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
	<div class="background fromAbove ae-2 superSlow" style="background-image: url('<?php echo $thumb['0'];?>')"></div>
</section>



<!-- article -->
<article class="article full-article ae-1 do">
	<div class="content">

		<div class="article-content">

			<p class="meta">
				<strong class="categories"><a href="../design/ux-design/index.html" title="View all posts in UX Design" class="post_tag category-ux-design" data-wpel-link="internal">UX Design</a></strong><a href="../author/marcintreder/index.html" title="Posts by Marcin Treder" rel="author" data-wpel-link="internal">Marcin Treder</a> • <span itemprop="datePublished" content="2017-08-15">August 15, 2017</span> • <a href="#comments" class="leave-a-comment" data-wpel-link="internal">17 Comments</a> 			</p>

			<div class="share-top-container">
				<div class="share share-top" data-post-url="index.html">
					<span class="total"><span class="total-number"></span> Shares</span>
					<ul>
						<li class="background-facebook facebook"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#facebook2"></use></svg>Share</li>
						<li class="background-twitter twitter"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#twitter"></use></svg>Tweet</li>
						<li class="background-linkedin linkedin"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#linkedin"></use></svg>Share</li>
						<li class="background-pinterest pinterest"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#pinterest"></use></svg>Pin It</li>
						<li class="background-googlePlus googlePlus"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#googlePlus"></use></svg>Share</li>
					</ul>
				</div>
			</div>

			<p><strong>The Peak Point of eCommerce and SaaS – the Credit Card Payment Form</strong></p>
<p>If you’re selling something online, you know it’s not the easiest thing to do. You need to find a prospective customer, present your product in a good light, drive her through a process full of forms and finally&#8230; collect her money. At every step you’re risking a sudden loss of your client. That’s painful.</p>
<p>There’s lots of things that can happen. The human attention span is extremely limited and vulnerable to distractions. The whole process is easy to complicate and tough to design. The last step in the process &#8211; the credit card payment form &#8211; is especially challenging. People won’t risk placing their credit card number in a badly designed or confusing form. The risk of losing money will be too great.</p>
<p>Minimizing the feeling of risk and avoiding confusion &#8211; that’s what you’re aiming at when designing a credit card form. That’s your ultimate UX Design task.</p>
<p>Wouldn’t it be sad to lose a customer at the last step of the selling process? You did everything else well and just the last tiny step somehow went wrong. The payment didn’t go through and the only thing that you can do is burst into tears.</p>

					</div>

		
	</div>
</article>


<!-- article -->
<article class="article full-article ae-1 do">
<div class="article-content pd-blog">

		<div class="article-content">
			<div class="share-top-container">
				<div class="share share-top" data-post-url="<?php esc_url(the_permalink()); ?>">
					<span class="total"><span class="total-number"></span> Shares</span>
					<ul>
						<li class="background-facebook facebook"><i class="fa fa-facebook-square"></i> Share</li>
						<li class="background-twitter twitter"><i class="fa fa-twitter-square"></i> Tweet</li>
						<li class="background-linkedin linkedin"><i class="fa fa-linkedin-square"></i> Share</li>
						<li class="background-pinterest pinterest"><i class="fa fa-pinterest"></i> Pin It</li>
						<li class="background-googlePlus googlePlus"><i class="fa fa-google-plus-square"></i> Share</li>
					</ul>
				</div>
			</div>



			<div class="meta">
				<strong class="categories">
					<a href="<?php echo get_site_url(); ?>/blog/?category_name=<?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?>" title="Spiceblue" class="post_tag category-ux-design" data-wpel-link="internal"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></a>
				</strong>
				<a href="#" title="Spiceblue" rel="author" data-wpel-link="internal"><?php  the_author(); ?></a> • <span itemprop="datePublished" content="2017-08-15"><?php the_date(); ?></span> <!--• <a href="#comments" class="leave-a-comment" data-wpel-link="internal">17 Comments</a>--> 			
			</div>

			<div><?php the_content(); ?></div>
			
			<div class="subscr">
			<img src="<?php echo get_template_directory_uri(); ?>/smile.svg" width="20" height="20">
			<?php //mailchimpSF_signup_form(); ?>
			</div>
</div>
</article>


  <?php endwhile; ?>

    <div class="navigation" align="center" style="color: cadetblue; display:none;">
      <?php previous_post_link('%link') ?> <?php next_post_link(' %link') ?>
    </div>

  <?php endif; ?>
	
	


</div>
</section>
</article>


<?php get_footer(); ?>
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/blog/js/scripts.min20b9.js?ver=1.0.2'></script>

<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/blog/js/functions.min20b9.js?ver=1.0.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var dm_vars = {"ajax_url":"#","sharrre_url":"#"};
/* ]]> */
</script>

<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/blog/js/sharrre.min.js'></script>
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/blog/js/selection-sharer.min.js'></script>
<script>
$('.intro').click(function() {
  $('body').toggleClass('no-bar');
	$('body').css('overflow', 'hidden');
});
	
$('.cl').click(function() {
$('body').css('overflow-y', 'scroll');
});
	
		
$(document).scroll(function() {
if($(window).scrollTop() > 50){
 		$(".menu-toggle").css("display","hidden");
}
});
</script>