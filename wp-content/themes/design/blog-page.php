<?php
/**
 * Template Name: Blog Page
 * The template for displaying pages
 *
 * This is the template that displays blog area.
 *
 * @package WordPress
 * @subpackage All Time Design
 * @since All Time Design 1.0
 */
 ?>
 
 <!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php wp_title(''); ?></title>
  <?php do_action( 'wpseo_head' );  ?>
  <link rel="shortcut icon" type="image/png" href="favicon.png"/>
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/navbar.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/default.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/additional.css" type="text/css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/popup.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/other-pages.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/base.css" data-rel-css="" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/example2.css" data-rel-css="" />
  <link href='<?php echo get_template_directory_uri(); ?>/assets/css/nprogress.css' rel='stylesheet' />
  <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script> 
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/other-pages.js"></script>
  <link rel='stylesheet prefetch' href='<?php echo get_template_directory_uri(); ?>/assets/css/select2.min.css'>
  <!--slide up-->
  
  <!---lightbox--->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/hes-gallery.css">
  
  <!---blog---->
  <link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/blog/style.css'/>
  <style>strong.mc_error_msg {color: red; font-weight:400 !important;} strong.mc_success_msg{color: green; font-weight:400 !important;}</style>

  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css" type="text/css" />
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js" type="text/javascript"></script>
  <script src="https://js.stripe.com/v3/"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/index.js" data-rel-js></script>
  <!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P2NJWB5');</script>
	<!-- End Google Tag Manager -->
</head>
<body class="blog">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P2NJWB5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<section class="menu design-menu">
    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
  <div class="container dis">
  
  <?php if ( is_user_logged_in() ) { 
  $current_user = wp_get_current_user();
	$customer_status = get_user_meta( $current_user->ID, 'status', true);
  
	  if ( $customer_status == 'active' ){
			echo '<div class="only-mobile">
		  <a class="btn btn-sm btn-primary display-4 btn-top" href="'.site_url('dashboard').'">Make Request</a>
		  </div>';
			
		}else{
			echo '<div class="only-mobile">
		  <a class="btn btn-sm btn-primary display-4 btn-top" href="'.site_url('pricing/').'" >Get Started</a>
		  </div>';
		}
	}else{
		echo '<div class="only-mobile">
      <a class="btn btn-sm btn-primary display-4 btn-top" href="'.site_url('pricing/').'" >Get Started</a>
      </div>';
	}
  
  ?> 
  
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-caption-wrap">
        <a class="navbar-caption text-white display-4" href="<?php echo get_home_url(); ?>">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/Logo.svg">
		</a></span>
            </div>
        </div>

 <?php if ( is_user_logged_in() ) { ?>   
<div class="collapse navbar-collapse mg2" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
      
      
	  <?php 
	  $current_user = wp_get_current_user();
	$customer_status = get_user_meta( $current_user->ID, 'status', true);
	if ( $customer_status == 'active' ){
		echo '<li class="nav-item">
      <a class="nav-link link text-black display-4" href="'.site_url('dashboard').'" aria-expanded="true">Dashboard</a>
      </li>';
		
	}else{
		echo '<li class="nav-item">
      <a class="nav-link link text-black display-4" href="'.site_url('pricing/').'" aria-expanded="true">Pricing</a>
      </li>';
	}
	  ?>
            <li class="nav-item">
      <a class="nav-link link text-black display-4" href="<?php echo site_url('works/'); ?>" aria-expanded="false">Works</a>
      </li>
      <li class="nav-item pr1">
      <a class="nav-link link text-black display-4" href="<?php echo site_url('blog/'); ?>" aria-expanded="false">Blog</a>
      </li>
      <li class="nav-item">
		<a  class="nav-link link text-black display-4 looutuser"  aria-expanded="false">Log out</a>
      </li>
      </ul>
      <?php 
		$style_prc = ( get_the_ID() == 58 ) ? 'style="visibility: hidden;"' : '';
		if ( $customer_status == 'active' ){
			echo '<div class="navbar-buttons design-section-btn" '.$style_prc.'><a class="btn btn-sm btn-primary display-4" href="'.site_url('dashboard').'">Make Request</a></div>';		
		}else{
			echo '<div class="navbar-buttons design-section-btn" '.$style_prc.' ><a class="btn btn-sm btn-primary display-4" href="'.site_url('pricing/').'" >Get Started</a></div>';
		}
	  ?>   
      
</div>
<?php } else { ?>
<div class="collapse navbar-collapse mg2" id="navbarSupportedContent">
    <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
        <li class="nav-item">
		  <a class="nav-link link text-black display-4" href="<?php echo site_url('pricing/'); ?>" aria-expanded="true">Pricing</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link link text-black display-4" href="<?php echo site_url('works/'); ?>" aria-expanded="false">Works</a>
		</li>
		<li class="nav-item pr1">
		  <a class="nav-link link text-black display-4" href="<?php echo site_url('blog/'); ?>" aria-expanded="false">Blog</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link link text-black display-4" href="tel:18774009977" aria-expanded="false">(877) 400-9977</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link link text-black display-4" href="<?php echo site_url('sign-in/'); ?>" aria-expanded="false">Sign in</a>
		</li>
	</ul>
	<div class="navbar-buttons design-section-btn" <?php if( get_the_ID() == 58 ){  ?> style="visibility: hidden;" <?php } ?>>
		<a class="btn btn-sm btn-primary display-4" href="<?php echo site_url('pricing/'); ?>">Get Started</a>
	</div>
</div>
<?php } ?>

</div>
</nav>
</section>

<section class="features1 design-cont mgt0 mgb1">
    <div class="container">
        <div class="row" align="center">
            <div class="col-md-12 main-info">
                Blog
            </div>
			
			<div id='search-box' class="blog-search">
			 <?php mailchimpSF_signup_form(); ?>
			</div>
			
        </div>
    </div>
</section>
 
  <section id="posts" class="container-l grid-3">
  
  <?php // Display blog posts on any page
		$temp = $wp_query; $wp_query= null;
		$wp_query = new WP_Query(); $wp_query->query('posts_per_page=50' . '&paged='.$paged);
		while ($wp_query->have_posts()) : $wp_query->the_post(); ?>	
  
  <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
      <a class="card-media" href="<?php the_permalink(); ?>" rel="noopener" style="background-image: url('<?php echo $thumb['0'];?>'); padding:30px;">
        <h3 class="width-xxs" style="color:#FFFFFF; font-weight:800; font-family: SF Pro Display;"><?php the_title(); ?></h3>
        <p>Read more ›</p>
      </a>
	  
	  <?php endwhile; ?>
	  <?php wp_reset_postdata(); ?>
  
  </section>
  <div class="mgb1">&nbsp;</div>



<section style="background-color:#fafafa;">
<div class="container"><div class="media-container-row pdlr1">

<div class="col-12 col-md-6 col-lg-6">
<div class="home-title ali1">Try Us 14 Days Risk-Free</div>
<div class="call-act mgt1a ali1">Services at All Time Design are top-notch. You don't have to blindly trust our words. Try us 100% risk-free for two whole weeks.</div>
</div>
<div class="col-12 col-md-6 col-lg-6">
<div class="navbar-buttons design-section-btn mgt1" align="center">
<a class="btn btn-sm btn-primary display-4 btn-hgt" href="<?php echo site_url('pricing/'); ?>">Get Started Now</a>
</div>
</div>

</div></div>
</section>
<?php get_footer(); ?>