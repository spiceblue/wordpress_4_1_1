<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Design Many">
  <title>All Time Design</title>
  <link rel="shortcut icon" type="image/png" href="favicon.png"/>
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css" />
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css" />
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-grid.min.css" />
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-reboot.min.css" />
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/navbar.css" />
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/default.css" />
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/additional.css" type="text/css" />
  <link href='<?php echo get_template_directory_uri(); ?>/assets/css/nprogress.css' rel='stylesheet' />
  <link rel='stylesheet prefetch' href='<?php echo get_template_directory_uri(); ?>/assets/css/select2.min.css'>
  <link href="https://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <?php 
  	$page_id = get_the_ID();
	if( $page_id != 9 && $page_id != 1106 && $page_id != 1108 && $page_id != 1110 && $page_id != 1112 ){
  ?>
  <link href="<?php echo get_template_directory_uri(); ?>/assets/css/other-pages.css" rel="stylesheet" />
  <?php } ?>
  <!---popup scr---->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/popup.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <!---popup scr end--->
  <style>
  body{background:#fff;overflow-y:scroll;} li{margin-bottom:15px;}.design-cont{background-color:#FFFFFF !important;} .form-group{text-align:left;}
  </style>
  <!-- Google Tag Manager 
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P2NJWB5');</script>
	<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) 
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P2NJWB5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->