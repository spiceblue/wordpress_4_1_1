<?php
/**
 * Template Name: stripe Page
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Design Many
 * @since Design Many 1.0
 */

//mail
$get_star_email = ( isset($_REQUEST['search']) ) ? $_REQUEST['search'] : '';
if ($get_star_email!='')
{
$to = "bala@spiceblue.co";
$subject = "[All Time Design]";
$txt = $get_star_email. "<br><br>This id tried to connect with All Time Design.";
$headers = "From:". $get_star_email;

wp_mail($to,$subject,$txt,$headers);

}
?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php wp_title(''); ?></title>
  <?php do_action( 'wpseo_head' );  ?>
  <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png"/>
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/SF-Pro-Display-Regular.otf" as="font" type="font/otf" crossorigin>
	  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/navbar.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/default.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/additional.css" type="text/css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/popup.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/other-pages.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/base.css" data-rel-css="" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/example2.css" data-rel-css="" />
  <link href='<?php echo get_template_directory_uri(); ?>/assets/css/nprogress.css' rel='stylesheet' />
  <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script> 
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/other-pages.js"></script>
  <link rel='stylesheet prefetch' href='<?php echo get_template_directory_uri(); ?>/assets/css/select2.min.css'>
  <!--slide up-->
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/balloon-css/0.5.0/balloon.min.css">
    
  <!---lightbox--->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/hes-gallery.css">

  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css" type="text/css" />
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js" type="text/javascript"></script>
  <script src="https://js.stripe.com/v3/"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/index.js" data-rel-js></script>
  <!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P2NJWB5');</script>
	<!-- End Google Tag Manager -->
<script type="text/javascript">                                                                                                                                                                                                                                                                                                                                                                                                                                               </script></head>
<body>

<div class="container">
<div class="row" style="margin-top:100px; margin-bottom:100px;">

<form action="" method="POST">
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_test_1Kr5jx4hEyF9XVw1M64ENPzA"
    data-image="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/11/09112414/stripe-logo.png"
    data-name="All Time Design"
    data-description="Subscription for $349"
    data-amount="34900"
    data-label="Sign Me Up!">
  </script>
</form>

<?php // Create a customer using a Stripe token

// If you're using Composer, use Composer's autoload:
//require_once('vendor/autoload.php');

// Be sure to replace this with your actual test API key
// (switch to the live key later)
\Stripe\Stripe::setApiKey("sk_test_P695BV1LjlIz5efUYUynjraE");

try
{
  $customer = \Stripe\Customer::create([
    'email' => $_POST['stripeEmail'],
    'source'  => $_POST['stripeToken'],
  ]);

  $subscription = \Stripe\Subscription::create([
    'customer' => $customer->id,
    'items' => [['plan' => 'weekly_box']],
  ]);

  header('Location: thankyou.html');
  exit;
}
catch(Exception $e)
{
  header('Location:oops.html');
  error_log("unable to sign up customer:" . $_POST['stripeEmail'].
    ", error:" . $e->getMessage());
}
?>

</div>
</div>




<?php get_footer(); ?>
