<?php
/**
 * Template Name: Tickets Page
 * The template for displaying pages
 *
 * This is the template that displays all Backend pages.
 *
 * @package WordPress
 * @subpackage All Time Design
 * @since All Time Design 1.0
 */
 include_once( 'top.php' );
if ( is_user_logged_in() ) {
/* Current User Data */

$current_user = wp_get_current_user();
$customer_status = get_user_meta( $current_user->ID, 'status', true);
$customer_new_user = get_user_meta( $current_user->ID, 'newuser', true);
if ( $customer_status == 'active' ){
  
?>
<style>
::-webkit-input-placeholder {
  color: rgba(0,0,0,.2);
  font-size:16px;
  font-weight:400;
}
::-moz-placeholder {
  color: rgba(0,0,0,.2);
  font-size:16px;
  font-weight:400;
}
:-ms-input-placeholder {
  color: rgba(0,0,0,.2);
  font-size:16px;
  font-weight:400;
}
:-moz-placeholder {
  color: rgba(0,0,0,.2);
  font-size:16px;
  font-weight:400;
}

body{background-color:#f9f9f9;}
.request-btn{margin-right: 0px;}
.dropbtn{margin-left: 0px;}
.back{color: #787E8C; font-family: "SF Pro Display"; font-size: 18px; line-height: 21px;}
.chat-title{color: #222222;	font-family: "SF Pro Display";	font-size: 24px;	font-weight: 500; line-height: 24px;}
.chat-txt1{color: #b3b4b9;	font-family: "SF Pro Display";	font-size: 13px;	font-weight: 500; line-height: 24px; padding-top:5px;}
.status-open{margin-left: 13px; vertical-align: middle;}
.status-delivered{margin-left: 13px; vertical-align: middle;}
.status-closed{margin-left: 13px; vertical-align: middle;}
.padd1{padding:25px; padding-top:15px; padding-bottom:10px;}
.req-title{color: #222222;	font-family: "SF Pro Display";	font-size: 16px; font-weight: bold;	line-height: 19px;}
.req-title span{opacity: 0.5; color: #9B9B9B;	font-family: "SF Pro Display"; font-weight: 500; font-size: 12px; line-height: 14px; padding-left:5px;}
.req-txt1{color: #222222; font-family: "SF Pro Display"; font-size: 14px; line-height: 22px; padding-top:10px;}
.req-att{color: #9B9B9B; font-family: "SF Pro Display";	font-size: 14px; font-weight: 500; line-height: 17px; padding-top:20px;}
.embed-submit-field textarea{width: 100%; border:0px; box-shadow:none; margin-bottom: 15px; padding-bottom: 70px;}
.embed-submit-field{width: 100%; margin-top:12px; margin-bottom: -8px; background-color:#F9F9F9;
    font-size: 18px;
    border: 1px solid #EDEDED;
    border-radius: 4px;
}
.att{position: static; margin-left: 20px; margin-bottom: 20px; float: left;}
.embed-submit-field button{position: static; float: right; margin-right: 20px; border-radius: 2px;	background-color: #96A0AB; color: #FFFFFF;	font-family: "SF Pro Display";	font-size: 14px;	font-weight: 600; padding-top: 8px; padding-bottom: 8px;}
textarea {resize: none}
.bdr1{width: 42px;	border: 1px solid #EDEDED;}
.or span{color: #666666;	font-family: "SF Pro Display"; font-size: 15px; line-height: 18px; vertical-align: -webkit-baseline-middle;}
.or{color:#EDEDED;}
.request-btn{padding: 10px 20px 10px 20px; font-size: 15px;}
.icon-bg{height: 80px; width: 90px; border: 1px solid #EBEDF0; background-color: #FFFFFF; text-align:center; padding: 22px; margin-top:10px; margin-right:12px;}
.accordion{max-width: 100%;}
.accordion .link{border:0px !important;}
.accordion .link:hover{border:0px !important;  -webkit-box-shadow: 0px 0px 11px -1px rgba(0,0,0,0.10);
    -moz-box-shadow: 0px 0px 11px -1px rgba(0,0,0,0.10);
    box-shadow: 0px 0px 11px -1px rgba(0,0,0,0.10);}
li{margin-bottom: 0px;}
.card{margin: -4px;}
.submenu{display: none !important;}
.accordion li.open i.fa-chevron-right{transform: rotate(0deg);}
</style>

<section class="menu design-menu">
    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">

        <div class="container">
                    <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-caption-wrap">
        <a class="navbar-caption text-white display-4" href="<?php echo get_home_url(); ?>">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/Logo.svg">
		</a></span>
            </div>
        </div>

            <div id="navbarSupportedContent">
                <div class="navbar-buttons design-section-btn">

                    <div class="setting-dropdown" style="float:right;">
                      <?php 
						$user_img = get_user_meta($current_user->ID,'profile_image', true);
						if( $user_img['url'] != '' ){
							echo '<a href="#"><img class="ad_setting_img" src="'.$user_img['url'].'" ></a>';
						}
						elseif($current_user->user_firstname!='')
						{
						echo '<a href="#" class="dashboard-icon">'.strtoupper(mb_substr($current_user->user_firstname, 0, 1).mb_substr($current_user->user_lastname, 0, 1)).'</a>';
						}
						else
						{
						echo '<a href="#" class="dashboard-icon">AD</a>';
						}
						 ?>
                        <div class="setting-dropdown-content">
							<a data-modal="exampleBilling" data-effect="pushup" data-icons="is_right" class="sm_open"><i class="fa fa-cog"></i> <span>Setting</span></a> 
							<a data-modal="exampleAccounting" data-effect="pushup" data-icons="is_right" class="sm_open"><i class="fa fa-list-alt"></i> <span>Billing</span></a>
							<hr>
							<a class="looutuser"><i class="fa fa-sign-out"></i> <span>Log out</span></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </nav>
</section>
<section class="features1">
    <div class="container" style="margin-top:100px;">
        <div class="media-container-row">
            <div class="card p-3 col-6 col-md-6 col-lg-6">
                <div class="dropdown back" style="margin-top:10px;">
                    <div style="color:#222222">All tickets</div>
                </div>
            </div>
            <div class="card p-3 col-6 col-md-6 col-lg-6" align="right">
                <div class="dropdown">
                    <button data-modal="exampleAdvanced" data-effect="pushup" data-icons="is_right" class="request-btn sm_open"><i class="fa fa-plus"></i>&nbsp;New&nbsp;Request</button>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="features1">
    <div class="container list_tickets">
		<?php	
		
			$user_ticket_convs = get_users_tickets($current_user->user_email);
			if(!empty($user_ticket_convs)){
			foreach( $user_ticket_convs as $user_ticket_conv){			
				$ticket_date_time = preg_split( "/(T|Z)/", $user_ticket_conv['created_at'] );
				$ticket_date = $ticket_date_time[0];
				$ticket_time = $ticket_date_time[1];				
				$ticket_status = ( $user_ticket_conv['status'] == 2 ) ? '<span class="status-open">Open</span>' : ( ( $user_ticket_conv['status'] == 4 ) ? '<span class="status-delivered">Delivered</span>' : ( ( $user_ticket_conv['status'] == 5 ) ? '<span class="status-closed">Closed</span>' : '' ) );
				$ticket_subject = $user_ticket_conv['subject'];
				$tiket_description = $user_ticket_conv['description'];
		?>
        <div class="media-container-row">
            <div class="card p-3 col-12 col-md-2 col-lg-2">
                <span class="period"><?php echo date_format($ticket_date,'M d, Y'); ?></span>
            </div>
            <div class="card p-3 col-12 col-md-10 col-lg-10">
                <div class="card-box">
                    <ul id="accordion" class="accordion slide_accordian">
                        <a href="<?php echo site_url('chat?ticket='.$user_ticket_conv['id']); ?>"><li class="acc" style="background:#fff;">
                            <div class="link link_acc">
                                <img class="icon" src="https://s3-ap-south-1.amazonaws.com/spiceblue-design/wp-content/uploads/2018/07/13131150/logo_design_dash.png"/> <span class="title"><?php echo $ticket_subject; ?></span>
                                <?php echo $ticket_status; ?>
                                <span class="date-time"><?php echo timeAgo($ticket_date.' '.$ticket_time); ?></span>
                                <i class="fa fa-chevron-right"></i>
                            </div>
                            <div class="submenu submenu_acc" style="background:#fff;">
                                <div id="chat-area">
									<?php $ticket_conversation = get_user_conversation($user_ticket_conv['id']); ?>
									<div class="customer_conv test1">
										<div class="ad_customer_conv">
											<?php 
											$user_img = get_user_meta($current_user->ID,'profile_image', true);
											if( $user_img['url'] != '' ){
												echo '<div class="ad_image"><img class="ad_setting_img" src="'.$user_img['url'].'" ></div>';
											}else{
												echo '<div class="ad_image_text">'.strtoupper(mb_substr($current_user->user_firstname, 0, 1).mb_substr($current_user->user_lastname, 0, 1)).'</div>';
											}
											echo $ticket_conversation['description']; 
											foreach( $ticket_conversation['attachments'] as $attachment ){  
											?>											
											<div class="ad_image_panel">
												<a href="<?php echo $attachment['attachment_url'] ?>" download target="_blank"><span class="att_name"><?php echo $attachment['name'] ?></span></a>
											</div>	
											<?php }	?>									
										</div>	
									<!--<div class="ad_customer_conv_time"> 51 m</div>-->									
									</div>
									<div class="clearfix"></div>
									<?php 
									if(!empty($ticket_conversation['conversations'])){
										foreach( $ticket_conversation['conversations'] as $conversation ){
											$from_email = str_replace(" <support@alltimedesign.freshdesk.com>","",$conversation['from_email']);	
											if( $from_email != '"All Time Design"' ){																				
									?>
										<div class="replay_conv">
											<div class="ad_replay_conv">
												<div class="ad_re_image"><img src="https://s3-ap-south-1.amazonaws.com/spiceblue-design/wp-content/uploads/2018/07/19092549/Icon-1.png" alt="att"></div>
												<?php 
												echo $conversation['body_text']; 
												foreach( $conversation['attachments'] as $attachment ){ ?>											 
													<div class="ad_image_panel">
														<a href="<?php echo $attachment['attachment_url'] ?>" download target='_blank'><span class="att_name"><?php echo $attachment['name'] ?></span></a>
													</div>	
												<?php } ?>										
											</div>
											<!--<div class="ad_replay_conv_time"> 51 m</div>-->
										</div>
										<div class="clearfix"></div>
									<?php }else{ ?>
										<div class="customer_conv">
											<div class="ad_customer_conv">										
												<?php 
													$user_img = get_user_meta($current_user->ID,'profile_image', true);
													if( $user_img['url'] != '' ){
														echo '<div class="ad_image"><img class="ad_setting_img" src="'.$user_img['url'].'" ></div>';
													}else{
														echo '<div class="ad_image_text">'.strtoupper(mb_substr($current_user->user_firstname, 0, 1).mb_substr($current_user->user_lastname, 0, 1)).'</div>';
													}
												?>
												<?php 	
													echo $conversation['body_text']; 
													foreach( $conversation['attachments'] as $attachment ){
												?>
													<div class="ad_image_panel">
														<a href="<?php echo $attachment['attachment_url'] ?>" download target="_blank"><span class="att_name"><?php echo $attachment['name'] ?></span></a>
													</div>	
												<?php } ?>	
											</div>
											<!--<div class="ad_customer_conv_time"> 51 m</div>-->
										</div>
									<?php } } } ?>
								</div>
								<div class="text_box">
									<form method="post" class="reply_form" enctype="multipart/form-data">
										<div class="embed-submit-field" id="txt-box" style="">
											<textarea class="mes"  name="reply_message" placeholder="Enter your reply..."></textarea>
											<div class="att">
											<input type="file" class="filebtn" name="repl_image" accept="image/*,.pdf"  />
											</div>
											<input type="hidden" name="ticket_id" value="<?php echo $user_ticket_conv['id']; ?>" />
											<input type="hidden" name="userid" value="<?php echo $current_user->ID ; ?>" />
											<button type="submit" class="btn_reply" >Reply</button>
										</div>
									</form>
								</div>		
								<div id="initial-button" class="initial_button">Reply
									<div>
									</div>
								</div>
                            </div>
                        </li></a>
                    </ul>
                </div>
            </div>			
        </div>
		<?php } } ?>		
    </div>
</section>
<div class="loader_img"></div>
<!-----popup section----->
<div class="slim_modal" id="exampleAdvanced">
    <div class="sm_content">
        <div class="sm_icon_menu">
            <ul>
                <li class="sm_close"><i class="fa fa-times fa-fw "></i></li>
            </ul>
        </div>
        <div class="sm_content_inner_wrap">
			<form action="" method="post" class="make_request" enctype="multipart/form-data">
            <div class="sm_area_bottom">
                <div class="frm-header">Making a request is easy</div>
				
				 <div class="media-container-row">
            <div class="card p-3a col-12 col-md-6 col-lg-6">
			<div class="form-group mg1">
                    <label for="sel1" class="frm-title">Request Type</label>
					<select class="js-select2 form-control" name="subject" required>
					  <option value="Social Media Design">Social Media Design</option>
					  <option value="Social Media Ads">Social Media Ads</option>
					  <option value="Banner Ads">Banner Ads</option>
					  <option value="Display Ads">Display Ads</option>
					  <option value="Brochures">Brochures</option>
					  <option value="Direct Mails">Direct Mails</option>
					  <option value="Flyers">Flyers</option>
					  <option value="Web Banners">Web Banners</option>
					  <option value="Posters">Posters</option>
					  <option value="Infographics">Infographics</option>
					  <option value="Stationery Design">Stationery Design</option>
					  <option value="Album Covers">Album Covers</option>
					  <option value="Basic Photo Edits">Basic Photo Edits</option>
					  <option value="Billboards">Billboards</option>
					  <option value="Book Covers">Book Covers</option>
					  <option value="Magazine Covers">Magazine Covers</option>
					  <option value="Car/Truck Wraps">Car/Truck Wraps</option>
					  <option value="Cards or Invites">Cards or Invites</option>
					  <option value="Web Ads">Web Ads</option>
					  <option value="Product Labels">Product Labels</option>
					  <option value="Trade Show Materials">Trade Show Materials</option>
					  <option value="Reports">Reports</option>
					  <option value="Proposals">Proposals</option>
					  <option value="Presentation Design">Presentation Design</option>
					  <option value="Packaging Design">Packaging Design</option>
					  <option value="Photoshop Editing">Photoshop Editing</option>
					  <option value="Invoices">Invoices</option>
					  <option value="Letterheads">Letterheads</option>
					  <option value="Letters">Letters</option>
					  <option value="Media Kits">Media Kits</option>
					  <option value="Newsletters">Newsletters</option>
					  <option value="Planners">Planners</option>
					  <option value="Business Cards">Business Cards</option>
					  <option value="Announcements">Announcements</option>
					  <option value="Magazine layouts">Magazine layouts</option>
					  <option value="Leaflets">Leaflets</option>
					  <option value="Invitations">Invitations</option>
					  <option value="Tags">Tags</option>
					  <option value="Presentation Graphics">Presentation Graphics</option>
					  <option value="Stickers">Stickers</option>
					  <option value="Email Header Graphics">Email Header Graphics</option>
					  <option value="Book Layouts">Book Layouts</option>
					  <option value="Booklets">Booklets</option>
					  <option value="Programs">Programs</option>
					  <option value="Report Cards">Report Cards</option>
					  <option value="Email Newsletters">Email Newsletters</option>
					  <option value="Bookmarks">Bookmarks</option>
					  <option value="Cards">Cards</option>
					  <option value="CD Covers">CD Covers</option>
					  <option value="Certificates">Certificates</option>
					  <option value="Class Schedules">Class Schedules</option>
					  <option value="Coupons">Coupons</option>
					  <option value="Calendar">Calendar</option>
					  <option value="Desktop Wallpapers">Desktop Wallpapers</option>
					  <option value="Gift Certificates">Gift Certificates</option>
					  <option value="T-Shirts">T-Shirts</option>
					  <option value="Merchandise">Merchandise</option>
					  <option value="Lesson Plans">Lesson Plans</option>
					  <option value="ID Cards">ID Cards</option>
					  <option value="Photo Collages">Photo Collages</option>
					  <option value="Menus">Menus</option>
					  <option value="Vector Tracing">Vector Tracing</option>
					  <option value="Postcards">Postcards</option>
					  <option value="Resumes">Resumes</option>
					  <option value="T-shirt Graphics">T-shirt Graphics</option>
					  <option value="Ebook Cover">Ebook Cover</option>
					  <option value="Rack Cards">Rack Cards</option>
					  <option value="Recipe Cards">Recipe Cards</option>
					  <option value="Others">Others</option>
					</select>
                </div>
			</div>
			
			<div class="card p-3a col-12 col-md-6 col-lg-6">
			<div class="form-group mg1">
                    <label for="sel1" class="frm-title">Inspirations (Optional)</label>
					<input type="text" class="form-control" placeholder="Type url" name="inspiration" />
			</div>
			</div>
			
			</div>
				
                
                <div class="form-group mg1">
                    <label for="comment" class="frm-title">Project Details</label>
                    <textarea class="form-control" rows="5" id="project" name="message" placeholder="Short brief about your project" required></textarea>
                </div>
				
				
				 <div class="media-container-row">
            <div class="card p-3a col-12 col-md-6 col-lg-6">
			<div class="form-group mg1">
                    <label for="att" class="frm-title">Attach Reference Files</label><br />
                        <input type="file" class="filebtn" name="image" accept="image/*,.pdf">
                </div>
			</div>
			
			<div class="card p-3a col-12 col-md-6 col-lg-6">
			<div class="form-group mg1">
			<label for="att" class="frm-title">&nbsp;</label><br />
                    <input type="hidden" name="email" value="<?php echo $current_user->user_email; ?>" />
                     <input type="hidden" name="userid" value="<?php echo $current_user->ID; ?>" />
				<input type="submit" class="frm-btn" name="create" value="Make Request">
			</div>
			</div>
			
			</div>
                
				
				
            </div>
           
			</form>
            <div align="right" class="frm-bottom"><a class="sm_close">Close <i class="fa fa-times"></i></a></div>
        </div>
    </div>
</div>
<!--popup section-->
<div class="slim_modal" id="exampleBilling">
    <div class="sm_content">
        <div class="sm_icon_menu">
            <ul>
                <li class="sm_close"><i class="fa fa-times fa-fw "></i></li>
            </ul>
        </div>
        <div class="sm_content_inner_wrap">
			 <div class="sm_area_bottom pd-pop">
				<div class="frm-header">Account Setting</div>
				<div class="row">
					<div class="col-md-4 fx3">
						<div class="user_img item">
						<?php 
						$user_img = get_user_meta($current_user->ID,'profile_image', true);
						?>
							<img class="transition img-responsive" id="profile-image" src="<?php echo ($user_img['url']) ? $user_img['url'] :  'https://s3-ap-south-1.amazonaws.com/spiceblue-design/wp-content/uploads/2018/07/11135440/test.jpg'; ?>" alt="user_img" />
								<input id="profile-image-upload" class="hidden" type="file" accept="image/*">
								<p><i class="fa fa-camera"></i></p>
						</div>
					</div>
					<div class="col-md-8">
						<div class="response_msge_up"></div>
						<form method="post" class="update_details">
							<div class="row">
								<div class="col-md-6 dm_contact_name">
									<label for="ev_name" class="frm-title">First Name</label>
									<input class="form-control" name="fname" placeholder="First Name" type="text" required="true" value="<?php echo $current_user->user_firstname; ?>" />
								</div>
								<div class="col-md-6 dNew Password">
									<label for="ev_name" class="frm-title">Last Name</label>
									<input class="form-control new_pass" name="lname" placeholder="Last Name" type="text" required="true" value="<?php echo $current_user->user_lastname; ?>" />

								</div>
							</div>
							<p class="dm_current_pass">

							</p>
							<div class="row dm_new_pass">
								<div class="col-md-6 dm_confirm_password">
									<label for="" class="frm-title">Email Id</label>
									<input class="form-control Confirm_pass" name="email" placeholder="Email" type="email" required value="<?php echo $current_user->user_email; ?>" readonly />
								</div>
								<div class="col-md-6 dm-submit1">
									<label for="" class="frm-title"></label>
									<input name="submit" type="submit" class="form-control btn btn-primary change_assword" value="Update" />
								</div>
							</div>
							<p class="dm_confirm_pass">

							</p>
						</form>
					</div>
				</div>
				<br>
				<hr>
				<br>
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12">
						<div class="response_msge"></div>
						<form method="post" class="change_pass">
							<div class="row">
								<div class="col-md-4 dm_contact_name">
									<label for="ev_name" class="frm-title">Current Password</label>
									<input class="form-control" name="old_pass" placeholder="Enter Current Password" type="Password" required="true" />
								</div>
								<div class="col-md-4 dNew Password">
									<label for="ev_name" class="frm-title">New Password</label>
									<input class="form-control new_pass" name="new_password" placeholder="Enter New Password" type="Password" required="true" />
								</div>
								<div class="col-md-4 dm_confirm_password">
									<label for="" class="frm-title">Confirm Password</label>
									<input class="form-control Confirm_pass" name="confirm_password" placeholder="Re-enter Password" type="Password" />
								</div>
							</div>
							<div class="row">
								<div class="col-md-8 dm_confirm_password">
									
								</div>
								<div class="col-md-4 dm-submit1">
									<label for="" class="dm_name"></label>
									<input name="submit" type="submit" class="form-control btn btn-primary change_assword" value="Change Password" />
								</div>
							</div>
						</form>
					</div>
				</div>
			 </div>
            <div align="right" class="frm-bottom"><a class="sm_close">Close <i class="fa fa-times"></i></a></div>
        </div>
    </div>
</div>
<!--popup section-->
<div class="slim_modal" id="exampleAccounting">
    <div class="sm_content">
        <div class="sm_icon_menu">
            <ul>
                <li class="sm_close"><i class="fa fa-times fa-fw "></i></li>
            </ul>
        </div>
        <div class="sm_content_inner_wrap">
			 <div class="sm_area_bottom pd-pop">
				<div class="frm-header">Billing Details</div>
				<div class="form-group">
					<div class="row">
						<?php 
							$user_plan = get_user_meta( $current_user->ID, 'plan', true );
							$user_amount = get_user_meta( $current_user->ID, 'amount', true );
							$customer_id = get_user_meta( $current_user->ID, 'customer_id', true );
							$subscribtion_start = get_user_meta( $current_user->ID, 'subscribtion_start', true );
							$subscribtion_start_date = date_create($subscribtion_start);
							$get_customer_data = customer_retrive($customer_id);
							
							$plan_amt = ($user_amount/100);  
							if( $user_plan == 'Yearly' ){
								$mnth_amt = ($plan_amt/12);
						?>
						<div class="col-12 col-md-6 col-lg-6">
							<p class="frm-title1 pl1 pdb4">Your Plan</p>
							<p class="frm-title2 pdb4"><span class="dm_doller">$<?php echo number_format($mnth_amt,0) ?></span> Per month billed annually</p>
							<p class="frm-title1 pl1 pdb4">Unlimited designs yearly</p>
						</div>
						<div class="col-12 col-md-6 col-lg-6">

							<img src="<?php echo site_url(); ?>/wp-content/themes/design/assets/images/Cards.png" class="icon-container">
						</div>
						<?php }else if( $user_plan == 'Monthly' ){ ?>
						<div class="col-12 col-md-6 col-lg-6">
							<p class="frm-title1 pl1 pdb4">Your Plan</p>
							<p class="frm-title2 pdb4"><span class="dm_doller">$<?php echo number_format($plan_amt,0) ?></span> Per month</p>
							<p class="frm-title1 pl1 pdb4">Unlimited designs monthly</p>
						</div>
						<div class="col-12 col-md-6 col-lg-6 yearmonth">
							<input type="submit" value="Update" class="btn update_user_plan" />
							<p class="yeartomonthtext">Upgrade and save 600 a year</p>
						</div>
						<?php } ?>
					</div>

				</div>
				<hr class="">
				<div class="form-group">
					<div class="row">
						<div class="col-12 col-md-12 col-lg-12">
							<p class="frm-title1 pl1 pdb4 mgt2">Payment Method<span class="dm_edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
			Change</span><span class="dm_cancel_bil"><i class="fa fa-times" aria-hidden="true"></i>
      Cancel</span></p>
						</div>
					</div>
				</div>
				<div class="form-group ad_payment_change1">
					<form method="post" class="update_card_from">
						<div class="row">
							<div class="col-12 col-md-5 col-lg-5 cardnumber pl1">
							
								<input type="text" class="form-control" id="ccnum" name="cardnumber" placeholder="xxxx xxxx xxxx 4521" value="<?php echo 'xxxx xxxx xxxx '.$get_customer_data['last']; ?>" required>
							</div>
							<div class="col-12 col-md-2 col-lg-2 expmonth">
								<input type="text" class="form-control" id="expmonth" name="expmonth" placeholder="MM / YY" value="<?php echo ( $get_customer_data['exp_month'] < 10 ) ? '0'.$get_customer_data['exp_month'].'/'.$get_customer_data['exp_year'] : $get_customer_data['exp_month'].'/'.$get_customer_data['exp_year']; ?>" required>
							</div>
							<div class="col-12 col-md-2 col-lg-2 cvv">

								<input type="text" class="form-control" id="cvv" name="cvv" placeholder="CVV" required>
							</div>
							<div class="col-12 col-md-3 col-lg-3 update">
								<input type="hidden" value="update_card_details_submit" class="btn" name="action" />
								<input type="submit" value="Update" class="form-control btn btn-primary change_assword" id="update">
							</div>
						</div>
					</form>
				</div>
        <div class="ad_card_pin pl1">XXXX XXXX XXXX <strong><?php echo $get_customer_data['last']; ?></strong></div>
				<hr class="">
				<div class="form-group">
					<div class="row">
						<div class="col-12 col-md-12 col-lg-12">
							<p class="dm_invoice_history pl1">Invoice History</p>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-12 col-md-12 col-lg-12">
						<?php 

						$customer_inid = get_user_meta( $current_user->ID, 'customer_id', true );
						$customer_invc = get_customer_invoice($customer_inid);
						$in_inr = 0;						
						foreach( $customer_invc->data as $invoice_data ){
							$invoice_amt = ($invoice_data['amount_paid']/100); 
							if( $in_inr == 0 ){
								echo '<p class="dm_date pl1">'. date('M d, Y',$invoice_data['date']) .' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="dm_date_price"> $'. number_format($invoice_amt,2) .' </span> <span class="dm_pdf"><a href="'.$invoice_data['invoice_pdf'].'" target="_blank">PDF</a></span></p>';
							}else{
								echo '<p class="dm_date pl1 load_extra">'. date('M d, Y',$invoice_data['date']) .' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="dm_date_price"> $'. number_format($invoice_amt,2) .' </span> <span class="dm_pdf"><a href="'.$invoice_data['invoice_pdf'].'" target="_blank">PDF</a></span></p>';
							}
							$in_inr++;
						}
						?>
							
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-12 col-md-12 col-lg-12">
							<p class="dm_all_invoices pl1">Show all invoices</p>
						</div>
					</div>
				</div>
			</div>
            <div align="right" class="frm-bottom"><a class="sm_close">Close <i class="fa fa-times"></i></a></div>
        </div>
    </div>
</div>
<script type='text/javascript'>
jQuery(document).ready(function($){
	$(document).on('change', ".filebtn", function () {
        var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
			$(this).val('');
            alert("Only formats are allowed : "+fileExtension.join(', '));
        }
    });
	var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
	$(document).on('click', '.btn_reply', function(e){
		e.preventDefault();
		var $_this = $(this).closest('.reply_form');
		var btn_this = $(this);
		var file = $($_this).find('input[type="file"]');
		var fd = new FormData();		
		var msge = $($_this).find('textarea[name=reply_message]');
		var tic_id = $($_this).find('input[name=ticket_id]');
		var individual_file = file[0].files[0];
		fd.append("file", individual_file);
		var reply_message = msge.val().replace(/\n|\r\n|\r/g, "<br>\r\n");
		var ticket_id = tic_id.val();

		var userid = $($_this).find('input[name=userid]').val();
		fd.append("reply_message", reply_message);  
		fd.append("ticket_id", ticket_id); 
		fd.append("userid", userid);  
		fd.append('action', 'reply_message_ticket'); 
        if(reply_message != " ") {
    		jQuery.ajax({
    			type: 'POST',
    			url: ajaxurl,
    			data: fd,
    			contentType: false,
    			processData: false,
    			beforeSend: function() {				
    				var cus_response = '<div class="customer_conv reply_query">'+reply_message+'<div>Sending...</div></div>'
    				$($_this).parent().parent().find('#chat-area').append(cus_response);
    				$($_this).find('.btn_reply').attr("disabled", true);
    			},
    			success: function(response){
				//console.log(response);
	   				$('.reply_query').remove();
    				$($_this).parent().parent().find('#chat-area').append(response);
					$('.btn_reply').closest('.reply_form').find('.mes').val('');
    				$($_this).find('.btn_reply').attr("disabled", false);
    			}
    		});  
        }else{
			alert("please type some message...");
        }
	})
	
	$(document).on('submit', '.make_request', function(e){
		e.preventDefault();
		var fd = new FormData();
		var file = $(this).find('input[type="file"]');
		var msge = $(this).find('textarea[name=message]');
		var subject = $(this).find('select[name=subject]');
		var email = $(this).find('input[name=email]');
		var inspiration_fld = $(this).find('input[name=inspiration]');
		var individual_file = file[0].files[0];
		fd.append("file", individual_file);
		var message = msge.val().replace(/\n|\r\n|\r/g, "<br>\r\n");
		var subject = subject.val();
		var email = email.val();
		var inspiration = inspiration_fld.val();
		var userid = $(this).find('input[name="userid"]').val();
		fd.append("message", message);  
		fd.append("subject", subject);  
		fd.append("email", email);  
		fd.append("inspiration", inspiration);  
		fd.append("userid", userid); 
		fd.append('action', 'make_request_ticket');
		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: fd,
			contentType: false,
			processData: false,
			beforeSend: function() {
				$('.sm_close').trigger('click'); 
				$('section').css({"opacity": ".5", "z-index": "-99"});
				/* $('.loader_img').show(); */
				NProgress.start();
			},
			success: function(response){
				console.log(response);
				$('.list_tickets').prepend(response);
				window.location.reload();
			},
			error: function(response) { 
				alert("Error occured.please try again");
			},
			complete: function() {
				$('.make_request').find('textarea[name=message]').val('');
				$('.make_request').find('input[type="file"]').val('');
				$('section').css({"opacity": "", "z-index": ""});
				/* $('.loader_img').hide(); */
				NProgress.done();
				$('.card-box ul').removeClass('slide_accordian');
				$('.link').removeClass('link_acc');
				$('.link').addClass('link_acc_ad');
				$('.submenu').removeClass('submenu_acc');
				$('.submenu').addClass('submenu_acc_add');
				$('.card-box ul').addClass('accordion_script');
				/* jQuery(function($) {
					var Accordion = function(el, multiple) {
						this.el = el || {};
						this.multiple = multiple || false;
						var links = this.el.find('.link_acc_ad');
						links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
					}
					Accordion.prototype.dropdown = function(e) {
						var $el = e.data.el;
							$this = $(this),
							$next = $this.next();
						$next.slideToggle();
						$this.parent().toggleClass('open');
						if (!e.data.multiple) {
							$el.find('.submenu_acc_add').not($next).slideUp().parent().removeClass('open');
						};
					}
					var accordion = new Accordion($('.accordion_script'), false);
				}); */
                //location.reload();
			}
		});
	})
	var i =0;
	$(document).on('click', '.item', function(e){
		i++;
		if( i == 1){		
		   $('#profile-image-upload').trigger('click');
		}	 
	})
   
	
	$('#profile-image-upload').on('change',function(){
		var input = this;
		var url = $(this).val();
		var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
		if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")){
			var reader = new FileReader();
			reader.onload = function (e) {
			   $('#profile-image').attr('src', e.target.result);
			   $('.ad_setting_img').attr('src', e.target.result);
				var individual_file = input.files[0];
				var fd = new FormData();
				fd.append('image', individual_file);
				fd.append('action', 'user_profile_change');
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data:fd,
					contentType: false,
					processData: false,
					beforeSend: function() {				
						NProgress.start();
					},
					success: function(response){
						if( response == 'success' ){
							i =0;
						}
						NProgress.done();
					}
				}); 
			}
		   reader.readAsDataURL(input.files[0]);
		}
		else{
		  $('#profile-image').attr('src', 'http://s3-ap-south-1.amazonaws.com/spiceblue-design/wp-content/uploads/2018/07/11080217/avatarF.jpg" alt="user_img');
		}	
	});
	
	$('.update_details').on('submit', function(e){
		e.preventDefault();
		var fd = new FormData();
		var fname = $(this).find('input[name=fname]').val();
		var lname = $(this).find('input[name=lname]').val();
		var email = $(this).find('input[name=email]').val();
		fd.append("fname", fname);  
		fd.append("lname", lname);  
		/* fd.append("email", email);   */
		fd.append('action', 'update_details_usr'); 
		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: fd,
			contentType: false,
			processData: false,
			beforeSend: function() {
				
			},
			success: function(response){
				console.log(response);
				if( response == 1 ){
					$('.response_msge_up').html('<div class="alert alert-warning"> <strong>Warning!</strong> Email Already Exit.</div>');
				}else if( response == 2 ){
					$('.response_msge_up').html('<div class="alert alert-success"> <strong>Success!</strong> Your details updated successfully.</div>');
				}else{
					$('.response_msge_up').html('');
				}						
			},
			error: function(response) { 
				alert("Error occured.please try again");
			},
			complete: function() {
				$('.change_pass').find('input[name=fname]').val(fname);
				$('.change_pass').find('input[name=lname]').val(lname);
				$('.change_pass').find('input[name=email]').val(email);
			}
		});
	})
	
	$('.change_pass').on('submit', function(e){
		var fd = new FormData();
		var old_pass = $(this).find('input[name=old_pass]').val();
		var new_oass = $(this).find('input[name=new_password]').val();
		var cnf_pass = $(this).find('input[name=confirm_password]').val();
		console.log(old_pass,new_oass,cnf_pass);
		fd.append("old_pass", old_pass);  
		fd.append("new_oass", new_oass);  
		fd.append("cnf_pass", cnf_pass);  
		fd.append('action', 'change_password');
		e.preventDefault();
		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: fd,
			contentType: false,
			processData: false,
			beforeSend: function() {
				
			},
			success: function(response){
				if( response == 1 ){
					$('.response_msge').html('<div class="alert alert-warning"> <strong>Warning!</strong> Old Password is wrong.</div>');
				}else if( response == 2 ){
					$('.response_msge').html('<div class="alert alert-danger"> <strong>Danger!</strong> New Password and confirm password is missmatch.</div>');
				}else if( response == 3 ){
					$('.response_msge').html('<div class="alert alert-success"> <strong>Success!</strong> Your password changed successfully.</div>');
				}
				
			},
			error: function(response) { 
				alert("Error occured.please try again");
			},
			complete: function() {
				$('.change_pass').find('input[name=old_pass]').val('');
				$('.change_pass').find('input[name=new_password]').val('');
				$('.change_pass').find('input[name=confirm_password]').val('');
			}
		});
	})
	$(document).on('submit', '.update_card_from', function(e){
		e.preventDefault();
		var fd = new FormData();
		var cardnumber = $(this).find('input[name=cardnumber]').val();
		var expmonth = $(this).find('input[name=expmonth]').val();
		var cvv = $(this).find('input[name=cvv]').val();
		var action = $(this).find('input[name=action]').val();
		fd.append("cardnumber", cardnumber);  
		fd.append("expmonth", expmonth);  
		fd.append("cvv", cvv);  
		fd.append('action', action);
		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data:fd,
			contentType: false,
			processData: false,
			beforeSend: function() {				
				NProgress.start();
			},
			success: function(response){
				if( response = 'success' ){
					$('.dm_cancel_bil').trigger('click');
					var lastFive = cardnumber.substr(cardnumber.length - 4);
					var card_nm = 'XXXX XXXX XXXX '+lastFive;
					$('.ad_card_pin').html(card_nm);
					$('.update_card_from').find('input[name=cvv]').val('');
					NProgress.done();
				}
				
			}
		});
	})
	$(document).on('click', '.update_user_plan', function(){
		$('.yearmonth').html('');
		$('.yearmonth').html('<p class="yeartomonthtext">Total Billed $3588</p><button type="submit" class="btn update_plan_user has-spinner">Upgrade My Plan</button>');
	})
	$(document).on('click', '.update_plan_user', function(e){
		e.preventDefault();
		var fd = new FormData();
		fd.append('action', 'upgrade_user_plan');
		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data:fd,
			contentType: false,
			processData: false,
			beforeSend: function() {				
				NProgress.start();
				var btn = $('.has-spinner');
				$(btn).buttonLoader('start');
			},
			success: function(response){
				/* location.reload(); */
				console.log(response);
				$('.yearmonth').append('<p class="yeartomonthtext">Thanks for upgrading. Please wait for a moment to refresh the page.</p>');setTimeout(function(){
					location.reload();
				},1200);				
			},
			complete: function() {
				var btn = $('.has-spinner');
				$(btn).buttonLoader('stop');
			}
		});
	})
	$(document).on('click', '.dm_all_invoices', function(){
		$('.dm_date.load_extra').show();
		$(this).removeClass('dm_all_invoices');
		$(this).addClass('hide_all_invoices');
		$(this).html('Hide Invoice');
	})
	$(document).on('click', '.hide_all_invoices', function(){
		$('.dm_date.load_extra').hide();
		$(this).removeClass('hide_all_invoices');
		$(this).addClass('dm_all_invoices');
		$(this).html('Show all Invoice');
	})
})
 

</script>
<!-----popup end-------->
<script type="text/javascript">
  $(document).ready(function(){
     $(".ad_payment_change1").hide();
     $(".dm_cancel_bil").hide();
    $(".dm_edit").click(function(){
        $(".ad_payment_change1").show();
        $(".ad_card_pin").hide();
        $(".dm_edit").hide();
         $(".dm_cancel_bil").show();
    });
     $(".dm_cancel_bil").click(function(){
        $(".ad_payment_change1").hide();
        $(".dm_cancel_bil").hide();
        $(".ad_card_pin").show();
        $(".dm_edit").show();
    });
});
</script>
 <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.buttonLoader.js"></script> 
<?php
	}else{
		wp_redirect( site_url( 'pricing' ) );
	}
}else{
	wp_redirect( site_url( 'pricing' ) );
} 
include_once( 'bottom.php' ); ?> 