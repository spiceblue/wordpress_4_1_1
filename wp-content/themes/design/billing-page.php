<?php
/**
 * Template Name: Billing page
 * The template for displaying pages
 *
 * This is the template that displays all Backend pages.
 *
 * @package WordPress
 * @subpackage Design Many
 * @since Design Many 1.0
 */
 get_header();
 if ( is_user_logged_in() ) {
	 $current_user = wp_get_current_user();
	 
	$current_plan = get_user_meta($current_user->ID, 'plan', true);
	$current_plan_end = get_user_meta($current_user->ID, 'subscribtion_end', true);
	$current_plan_end = get_user_meta($current_user->ID, 'subscribtion_end', true);
	$amount = get_user_meta($current_user->ID, 'amount', true);
	$currency = get_user_meta($current_user->ID, 'currency', true);
	$usd_amt = ($amount / 100);
	$amount_show = ( $currency == 'usd' ) ? '$'.number_format($usd_amt,2) : number_format($usd_amt,2);	
	$date = date_create($current_plan_end);
	
?>
<div class="container" style="padding-top:100px;">
    <h1 class="dm_billing_main_head">Billing Settings</h1>
    <div class="row">
	<?php 
		if( $current_plan == 'Monthly' ){
	?>	
        <div class="col-sm-6">
            <div class="dm_billing_panel_header">
                <h1 class="dm_panel_inner_header">Current Subscription</h1></div>
            <div class="dm_billing_panel_body">
                <div class="dm_billing_border">
                    <span>You are currently on Monthly Plan</span><span class="dm_billing_para1">Next Renewal <?php echo date_format($date, 'dS F , Y'); ?></span><span class="dm_billing_price"><?php echo $amount_show; ?></span>
                </div>
                <div class="dm_billing_button_right">
                    <button type="button" class="btn btn-default dm_billing_change_plan">Change Plan</button>
                </div>
            </div>
        </div>
	<?php } ?>	
        <div class="col-sm-6">
            <div class="dm_billing_panel_header">
                <h1 class="dm_panel_inner_header">Billing Details</h1></div>
            <div class="dm_billing_panel_body">
                <div class="dm_billing_border">
                    <span>Your credit card no on file is xxxx xxxx xxxx 3546 </span>
                </div>
                <div class="dm_billing_button_right">

                    <button type="button" class="btn btn-default dm_billing_edit">Edit Billing</button>
                </div>
            </div>
        </div>
    </div>
</div> 
<?php 
 }else{
	 wp_redirect( site_url( 'sign-in' ) );
 }
?>	