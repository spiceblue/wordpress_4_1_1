jQuery(document).ready(function($){
	var baseurl = window.location.origin;
	var ajaxurl = footer_ajaxurl.ajax_url;
	$('.pay_form_frm').on('submit', function(e){
		e.preventDefault();
		var fd = new FormData();
		var user_id = $(this).find('input[name=user_id]');
		var user_plan = $(this).find('input[name=user_plan]');
		var stripe_token = $(this).find('input[name=stripe_token]');
		var user_id1 = user_id.val();
		var user_plan1 = user_plan.val();
		var stripe_token1 = stripe_token.val();
		fd.append("user_id", user_id1);  
		fd.append("user_plan", user_plan1);  
		fd.append("stripe_token", stripe_token1);  
		fd.append('action', 'user_payment_option');
		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: fd,
			contentType: false,
			processData: false,
			beforeSend: function() {
				$('.response_msge_gts').html('');
				$('.example2 .token').html('Fetching Payment......');
			},
			success: function(response){
				var obj = jQuery.parseJSON(response);
				if(obj.status){
					$('.stripe_check_pay').hide();
					$('.response_msge_gts').html('<div class="alert alert-success"> <strong>Thank you, Payment Completed Successfully!</strong></div>');
					$('.final_response').show();
					$('.final_response .make_request #user_email_req').val(obj.msge);
				}else{
					$('.response_msge_gts').html('<div class="alert alert-danger"> <strong>'+obj.msge+'</strong></div>');
				}
				$('.example2').removeClass('submitting');
			},
			error: function(response) { 
				alert("Error occured.please try again");
			},
			complete: function() {
				
			}
		});
	})	

	$('.make_request').on('submit', function(e){
		e.preventDefault();
		var fd = new FormData();
		var file = $(this).find('input[type="file"]');
		var msge = $(this).find('textarea[name=message]');
		var subject = $(this).find('select[name=subject]');
		var email = $(this).find('input[name=email]');
		var inspiration_fld = $(this).find('input[name=inspiration]');
		var individual_file = file[0].files[0];
		fd.append("file", individual_file);
		var message = msge.val();
		var subject = subject.val();
		var email = email.val();
		var inspiration = inspiration_fld.val();
		fd.append("message", message);  
		fd.append("subject", subject);  
		fd.append("email", email); 
		fd.append("inspiration", inspiration);  
		fd.append('action', 'make_request_ticket');
		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: fd,
			contentType: false,
			processData: false,
			beforeSend: function() {
				$('.sm_close').trigger('click');
				NProgress.start();
			},
			success: function(response){
				console.log(response);
			},
			error: function(response) { 
				alert("Error occured.please try again");
			},
			complete: function() {
				NProgress.done();
				window.location.replace(baseurl+"/dashboard/");
			}
		});
	})
	
	$('.looutuser').on('click', function(){		
		$.ajax({
			type:'post',
			url:ajaxurl,
			data:{
				action:'logout_user'
			},
			success:function(data){
				window.location.replace(baseurl+"/sign-in/");
			}
		})
	});
}) 