$(document).ready(function() {
	$('.accordion').find('.accordion-toggle').click(function() {
		$(this).next().slideToggle('600');
		$(".accordion-content").not($(this).next()).slideUp('600');
	});
	$('.accordion-toggle').on('click', function() {
		$(this).toggleClass('active').siblings().removeClass('active');
	});
// 	if($(".dm_checked").val() == ""){
// 		$('.dm_prizing1').hide();
// 	}
// 	else{
// $('.dm_prizing2').hide();
$('.dm_prizing1').hide();  
// 	}
	$('.dm_checked').click(function() {   
  if($("input[name='dm_prizing_radio']:checked").val()){ 
    $('.dm_prizing1').css('display', 'none');
    $('.dm_prizing2').css('display', 'block');
  }
  else{ 
     $('.dm_prizing2').css('display', 'none');
    $('.dm_prizing1').css('display', 'block');
  }
});
	
	var Accordion = function(el, multiple) {
		this.el = el || {};
		this.multiple = multiple || false;

		// Variables privadas
		var links = this.el.find('.link');
		// Evento
		links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
	}

	Accordion.prototype.dropdown = function(e) {
		var $el = e.data.el;
			$this = $(this),
			$next = $this.next();

		$next.slideToggle();
		$this.parent().toggleClass('open');

		if (!e.data.multiple) {
			$el.find('.submenu').not($next).slideUp().parent().removeClass('open');
		};
	}	

	var accordion = new Accordion($('#accordion'), false);
	
});