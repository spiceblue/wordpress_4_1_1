<?php
/**
 * Template Name: Landing Page 2
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage All Time Design
 * @since All Time Design 2.0
 */

 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php wp_title(''); ?></title>
  <?php do_action( 'wpseo_head' );  ?>
  <link rel="shortcut icon" type="image/png" href="favicon.png"/>
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/landing2.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/navbar.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/default.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/additional.css" type="text/css">
  <link href="https://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/other-pages.css">
  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script> 
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/other-pages.js"></script>
  
  <style>
  body{background:url(<?php echo get_template_directory_uri(); ?>/assets/images/landing2-banner.png) no-repeat top; background-size:100% auto;}
  .design-cont{background:none !important;}
  </style>
  
  <!--slide up-->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css" type="text/css" />
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js" type="text/javascript"></script>
</head>
<body>


<section>
    <div class="container l2-mgt1">
        <div class="row" align="center">
            <div class="col-md-12 l2-banner-title">
			<div class="l2-mg1">
			<a href="<?php echo site_url(); ?>"> <img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/10/17132817/Logo_header.png" alt="logo"></a><br /><br />
                Get unlimited graphic designs for a flat monthly fee
            </div>
			</div>
			
			<div id='search-box'>
			  <input id='search-text' name='search' placeholder='Enter email address' type='text'/>
			  <button id='search-button' type='submit'>                     
			  <span class="btn display-4 fnt1">Get&nbsp;Started</span>
			  </button>
			</div>
			
        </div>
    </div>
</section>


<div class="container">
<div class="row">
<div class="col-12 col-md-12 col-lg-1">
&nbsp;
</div>

<div class="col-12 col-md-12 col-lg-10">
<section class="features1 design-cont">
    <div class="container work-img" style="padding:0px; -webkit-box-shadow: 0px 26px 32px 8px rgba(0,0,0,0.07);
-moz-box-shadow: 0px 26px 32px 8px rgba(0,0,0,0.07);
box-shadow: 0px 26px 32px 8px rgba(0,0,0,0.07);">
        
		<div class="media-container-row bs1" style="padding:10px; padding-bottom:0px; background-color:#FFFFFF;">
            
			<div class="card p-3a col-12 col-md-4 col-lg-4">
                <div class="item"><img class="wow animated" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133356/design1.png" />
                    <div class="item-overlay top"></div>
                </div>
            </div>
			
			<div class="card p-3a col-12 col-md-4 col-lg-4">
                <div class="item"><img class="wow animated" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133348/design2.png" />
                    <div class="item-overlay top"></div>
                </div>
            </div>
			
			<div class="card p-3a col-12 col-md-4 col-lg-4">
                <div class="item"><img class="wow animated" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133349/design3.png" />
                    <div class="item-overlay top"></div>
                </div>
            </div>
		
        </div>
		
		<div class="media-container-row" style="padding:10px; padding-top:0px; background-color:#FFFFFF;">
            
			<div class="card p-3a col-12 col-md-4 col-lg-4">
                <div class="item"><img class="wow animated" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133350/design4.png" />
                    <div class="item-overlay top"></div>
                </div>
            </div>
			
			<div class="card p-3a col-12 col-md-4 col-lg-4">
                <div class="item"><img class="wow animated" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133352/design5.png" />
                    <div class="item-overlay top"></div>
                </div>
            </div>
			
			<div class="card p-3a col-12 col-md-4 col-lg-4">
                <div class="item"><img class="wow animated" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133354/design6.png" />
                    <div class="item-overlay top"></div>
                </div>
            </div>
		
        </div>

    </div>
    </div>
</section>
</div>

<div class="col-12 col-md-12 col-lg-1">
&nbsp;
</div>

</div>
</div>



<section class="features1 l2-cont">
    <div class="container" style="margin-top:80px;">
        <div class="media-container-row">

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-hearth" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/10/17132712/l2-icon1.png" height="64"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">
                        Unlimited Designs</h4>
                    <p class="many-text many-fonts-style display-7">
                        Unlimited Pixel-Perfect designs that are in line with the given<br />context.
                    </p>
                </div>
            </div>

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-rocket" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/10/17132713/l2-icon2.png" height="64"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">Quick Turnarounds</h4>
                    <p class="many-text many-fonts-style display-7">
                        We are a fast but realistic team, working constantly towards quick project delivery.
                    </p>
                </div>
            </div>

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-globe" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/10/17132714/l2-icon3.png" height="64"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">Dedicated Team</h4>
                    <p class="many-text many-fonts-style display-7">Get assigned to a reliable design expert account manager for personalized assistance.</p>
                </div>
            </div>


        </div>
    </div>

<div class="container">
        <div class="media-container-row pdb3 mgb2">

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-hearth" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/10/17132715/l2-icon4.png" height="64"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">
                        Unlimited Revisions</h4>
                    <p class="many-text many-fonts-style display-7">
                        Our approachable team is all ready to work on your revisions, to make your vision come alive.
                    </p>
                </div>
            </div>

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-rocket" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/10/17132717/l2-icon5.png" height="64"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">Constant Updates</h4>
                    <p class="many-text many-fonts-style display-7">
                        Stay updated on a day to day basis about the status of your<br />request.
                    </p>
                </div>
            </div>

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-globe" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/10/17132909/l2-icon6.png" height="64"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">Fixed Affordable Price</h4>
                    <p class="many-text many-fonts-style display-7">Pay a flat price for our services and it’s nothing compared to what design agencies charge.</p>
                </div>
            </div>

        </div>
		
    </div>
</section>


<div class="container-fluid">
<div style="border-top:1px solid #D8D8D8; margin-left:11%; margin-right:11%; margin-bottom:30px;">&nbsp;</div>
<div class="media-container-row">
<div class="col-6 col-md-6 col-lg-2" align="center">
<img class="wdt1" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/10/18064552/Google.png">
</div>
<div class="col-6 col-md-6 col-lg-2" align="center">
<img class="wdt1" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/10/18064555/Youtube.png">
</div>
<div class="col-6 col-md-6 col-lg-2" align="center">
<img class="wdt1" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/10/18064553/Instagram.png">
</div>
<div class="col-6 col-md-6 col-lg-2" align="center">
<img class="wdt1" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/10/18064551/Fastcompany.png">
</div>
<div class="col-6 col-md-6 col-lg-2" align="center">
<img class="wdt1" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/10/18064554/Invision.png">
</div>
</div>
<div style="border-bottom:1px solid #D8D8D8; margin-left:11%; margin-right:11%; margin-top:30px;">&nbsp;</div>
</div>


<section>
<div class="container l2-mgt2">
<div class="media-container-row" align="center">
		<div class="card p-3a col-12 col-md-12 col-lg-12">
		<div class="testim">
		All time design has made it quick and easy for our team to create a variety of eye-catching marketing materials. Being able to communicate directly with the designer about our needs, which helps to ensure that we get the best possible final product, every time
		</div>
		
		<div class="pdt3" align="center" style="width:350px;">
		
		<div style="float: left; width: 45%; text-align: right; padding-right: 20px;"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/10/17144357/Testomonials.png"></div>
		<div style="float:right; width:55%; text-align: left;">
		<div class="testi-txt" style="text-align:left !important;">KELLY LAURA</div><div class="testi-txt1" style="text-align:left !important;">Manager</div>
		</div>
		
		</div>
		


</div><hr /></div>
</section>

<div align="center" class="get-but">
<a class="btn btn-sm btn-primary display-4" href="<?php echo site_url('pricing/'); ?>" style="padding: 30px 25px;">Get Started Now</a>
</div>


<section>
<div class="container l2-mg2">
<div class="media-container-row">
<div class="col-12 col-md-12 col-lg-12">
<div class="ft"><a href="#">Terms & Conditions</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#">Privacy Policy</a></div>
</div>
</div>
</div>
</section>

<script type="text/javascript">
            new WOW().init();
        </script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/smooth-scroll.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/script.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jarallax.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/script.js"></script>
  <script  src="<?php echo get_template_directory_uri(); ?>/assets/js/popup.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/l10n.js" data-rel-js></script>
  <?php if( get_the_ID() == 58 ){ ?>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/example2.js" data-rel-js></script>
  <?php } ?>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/custom_spiceblue.js"></script>  
  <script src='<?php echo get_template_directory_uri(); ?>/assets/js/nprogress.js'></script>
  

<script src='<?php echo get_template_directory_uri(); ?>/assets/js/select2.full.js'></script>
<script  src="<?php echo get_template_directory_uri(); ?>/assets/js/select2.js"></script>

<!---lightbox--->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/hes-gallery.js"></script>
  
</body>
</html>  