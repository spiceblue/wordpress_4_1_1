<?php
/**
 * Template Name: Main Page
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage All Time Design
 * @since All Time Design 1.0
 */

get_header(); ?>

<?php
while ( have_posts() ) : the_post();
the_content();			
endwhile;
?>


<section style="background-color:#fafafa;">
<div class="container"><div class="media-container-row pdlr1">

<div class="col-12 col-md-6 col-lg-6">
<div class="home-title ali1">Try Us 14 Days Risk-Free</div>
<div class="call-act mgt1a ali1">Services at All Time Design are top-notch. You don't have to blindly trust our words. Try us 100% risk-free for two whole weeks.</div>
</div>
<div class="col-12 col-md-6 col-lg-6">
<div class="navbar-buttons design-section-btn mgt1" align="center">
<a class="btn btn-sm btn-primary display-4 btn-hgt" href="<?php echo site_url('pricing/'); ?>">Get Started Now</a>
</div>
</div>

</div></div>
</section>
<?php get_footer(); ?>