<?php
/**
 * Template Name: Home Page
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage All Time Design
 * @since All Time Design 1.0
 */

get_header(); ?>
<?php /*?><section class="header1 design-parallax many-parallax-background">
    <div class="container">
        <div class="row">
            <div class="many-white col-md-10">
                <h1 class="many-section-title align-center many-bold pb-3 many-fonts-style display-1">
<!--<span class="banner-txt">We are All Time Design</span><br>-->
Get unlimited graphic designs for a flat monthly fee</h1>
                
<div class="navbar-buttons design-section-btn mgb1">
<a class="btn btn-sm btn-primary display-4" href="<?php echo site_url(); ?>/pricing/">Get Started</a>
</div>
               </div>
        </div>
    </div>
</section><?php */?>
<style>
#search-box input[type="email"]
    {
    width: 100%;
    padding: 14px 0 14px 1em;
    color: #333;
    outline: none;
	border: 1px solid #979797 !important;	border-radius: 3px 3px 4px 3px !important;	background-color: #FFFFFF;
    }
</style>
<style>
input[type=text]:focus, input[type=password]:focus, textarea:focus {
  -moz-box-shadow: none;
  -webkit-box-shadow:none;
  box-shadow:none;
  border: 1px solid #0d83dd;
}
.dm_prizing1{display:none;}

/*new code*/
.pri-title{	color: #2C2F33;	font-family: "SF Pro Display";	font-size: 20px; font-weight: 800 !important; line-height: 24px; text-align: center; margin-bottom:30px;}
.chk-txt{color: #778095 !important; font-family: "SF Pro Display";	font-size: 16px !important; line-height: 19px;	text-align: center;}
.ded{color: #939DB8; font-family: "SF Pro Display"; font-size: 14px; line-height: 16px; padding-left:10px;}
.ded-txt{color: #939DB8; font-family: "SF Pro Display";	font-size: 17px; line-height: 25px;	text-align: center; padding:50px; padding-top:5px; letter-spacing:.5px;}
.ded-txt span{color: #2C2F33;}
.curr{left: 26% !important; font-weight: 600 !important; color:#939DB8 !important; font-size:18px !important;}
.dm_prizing_para4{color:#939DB8 !important;}
.pricing-sub{border: 1.5px solid #cacfd5; !important;}
::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  color: #D9DFE8; font-size: 16px;
}
::-moz-placeholder { /* Firefox 19+ */
  color: #D9DFE8; font-size: 16px;
}
:-ms-input-placeholder { /* IE 10+ */
  color: #D9DFE8; font-size: 16px;
}
:-moz-placeholder { /* Firefox 18- */
  color: #D9DFE8; font-size: 16px;
}
.table-title{color: #2C2F33; font-family: "SF Pro Display"; border: 1.5px solid #EDF2FA; border-right:0px; border-bottom:0px; font-size: 18px; font-weight: 600; background-color:#fafbfc; padding:12px; padding-left:30px;}
.table-cont{color: #2C2F33; font-family: "SF Pro Display"; font-size: 18px; font-weight: 400; padding:12px; padding-left:30px; border: 1.5px solid #EDF2FA; border-right:0px;}
.table-cont1{color: #2C2F33; font-family: "SF Pro Display"; font-size: 18px; font-weight: 400; padding:12px; padding-left:30px; border: 1.5px solid #EDF2FA; border-right:0px; border-top:0px;}
.table-cont2{padding:12px; padding-left:30px; border: 1.5px solid #EDF2FA; border-top:0px; text-align:center;}
.table-cont3{padding:12px; padding-left:30px; border: 1.5px solid #EDF2FA; border-left:0px; border-top:0px; text-align:center;}
.curve-arrow{float:right; margin-top:-18px; margin-right:55px;}

/* Acordeon styles */
.tab {
  position: relative;
  margin-bottom: 1px;
  width: 100%;
  color: #fff;
  overflow: hidden;
}
.tab input {
  position: absolute;
  opacity: 0;
  z-index: -1;
}
.tab label {
  position: relative;
  display: block;
  padding: 0 0 0 1em;
  background: #dce9f3;
  color:#0068e5;
  font-weight: bold;
  line-height: 3;
  cursor: pointer;
}
.blue label {
  background: #2980b9;
}
.tab-content {
  max-height: 0;
  overflow: hidden;
  background: #EDF2FA;
  transition: max-height .35s;
  color:#939DB8;
}
.blue .tab-content {
  background: #3498db;
}
.tab-content p {
  margin: 1em;
}
/* :checked */
.tab input:checked ~ .tab-content {
  max-height: 100vh;
}
/* Icon */
.tab label::after {
  position: absolute;
  right: 0;
  top: 0;
  display: block;
  width: 3em;
  height: 3em;
  line-height: 3;
  text-align: center;
  transition: all .35s;
}
.tab input[type=checkbox] + label::after {
  content: "+";
}
.tab input[type=radio] + label::after {
  content: "\25BC";
}
.tab input[type=checkbox]:checked + label::after {
  -webkit-transform: rotate(315deg);
          transform: rotate(315deg);
}
.tab input[type=radio]:checked + label::after {
  -webkit-transform: rotateX(180deg);
          transform: rotateX(180deg);
}
label{margin-bottom: 0rem;}
.pri-but{margin-top:10px; width: 282px;}
.pri-des{color: #939DB8; font-family: "SF Pro Display";	font-size: 14px; line-height: 22px;	text-align: center; padding:50px; padding-top:20px; padding-bottom:0px;}
.pricing-sub{margin-top:0px;}
.switch{top: 10px;}
.dm_prizing_number{font-size: 80px;}
.des-list{color: #939DB8 !important; font-family: "SF Pro Display"; font-size: 16px !important; line-height: 16px; padding:10px !important;}

select {
  border: 0 !important;
  font: 16px SF Pro Display !important;
}

[data-balloon][data-balloon-pos='right']:hover:before, [data-balloon][data-balloon-pos='right'][data-balloon-visible]:before{display:none;}
.ttip{font-weight:500 !important; color:#e1e9f0 !important; float:right;}
.pdt60{padding-top:60px;}
.dm_prizing_switch {
    margin-right: 5px;
    margin-left: 5px;
}
.offer{	color: #31B860;	font-family: "SF Pro Display";	font-size: 16px;	letter-spacing: -0.04px;	line-height: 19px;}
.dm_prizing_number{font-weight: 700 !important;}
#ftrs ul li{color: #2C2F33; font-family: "SF Pro Display";	font-size: 18px; font-weight: 500;	letter-spacing: 0.3px;	line-height: 36px;	text-align: center;}
div#mc_message{color:#FFFFFF;}
.newsletter{width: 50% !important; margin-left: 20%; margin-top:40px;}
.mb101{margin-top:60px; margin-bottom:150px;}
@media screen and (max-width:1024px){
.curr{left: 20% !important;}
.pri-des{padding: 40px;}
select {
  font: 14px SF Pro Display !important;
}
.table-cont1, .table-cont{font-size: 15px;}
.table-cont2, .table-cont3{padding: 10px;}
}

@media screen and (max-width:768px){
.curr{left: 16% !important;}
.chk-txt{font-size: 12px !important;}
.ded-txt{padding: 0px; padding-top:80px;}
.ded{font-size: 12px;}
.dm_prizing_number{font-size: 65px;}
.pri-des{padding: 0px;}
.mmmgt1{margin-top:40px !important;}
.pri-title{margin-bottom: 40px;}
.table-cont1, .table-cont{font-size: 11px;}
.table-cont2, .table-cont3{padding: 7px;}
select{font: 10px SF Pro Display !important;}
.ded-txt{padding-top: 20px; padding-bottom: 30px;}
.pricing-sub, .pri-but{width: 206px !important;}
#ftrs ul li{font-size:16px;}
.newsletter{width: 70% !important;}
}

@media screen and (max-width:767px){
.curr{left: 29% !important;}
.table-cont1, .table-cont{font-size: 16px;}
.table-cont, .table-cont1{padding-left: 18px;}
.pdt60{padding-top:0px;}
.chk-txt{font-size: 16px !important;}
select{font: 18px SF Pro Display !important;}
.dm_prizing_number{font-size: 80px;}	
.pri-des{font-size: 16px; margin: 20px; margin-left: 50px; margin-right: 50px;}
.pricing-sub, .pri-but{width: 282px !important;}
.newsletter{width: 100% !important; margin-left: 0px !important; margin-top:40px;}
.mb101{margin-top:60px; margin-bottom:0px;}
}

@media screen and (max-width:380px){
.home-title{font-size: 24px !important;}
.curr{left: 27% !important;}
}

@media screen and (max-width:360px){
.curr{left: 23% !important;}
}

@media screen and (max-width:320px){
.curr{left: 20% !important;}
select{font: 15px SF Pro Display !important;}
.pri-des{margin-left: 0px; margin-right: 0px; }	
.home-title{font-size: 19px !important;}
}
</style>
<script>
function sync()
{
  var n1 = document.getElementById('n1');
  val1 = document.getElementById('n1').value;
  if (val1=='')
  {
  document.getElementById('n1').value=1;
  }
  else if(val1==0)
  {
  document.getElementById('n1').value=1;
  }
  
  if(val1>=10)
  {
  document.getElementById('n1').value=10;
  }
  var n2 = document.getElementById("n2").innerHTML = n1.value*349;
  var n3 = document.getElementById("n3").innerHTML = n1.value*299;
  var n4 = document.getElementById("n4").innerHTML = n1.value*999;
  var n5 = document.getElementById("n5").innerHTML = n1.value*899;
}
</script>

<section class="clients">
    <div class="container">
        <div class="row" align="center">
            <div class="col-md-12 main-info">
                Get unlimited graphic designs for a flat monthly fee
            </div>
			
			<div id='search-box'>
			<form method="post" action="<?php echo site_url('pricing/'); ?>" >
			  <input id='search-text' name='search' placeholder='Enter email address' type='email' required />
			  <button id='search-button' type='submit'>                     
			  <span class="btn display-4 fnt1">Get&nbsp;Started</span>
			  </button>
			  </form>
			</div>
			
        </div>
    </div>
</section>


<section class="features1 design-cont">
    <div class="container work-img pdt4">
        
		<div class="media-container-row bs1" style="padding-top:10px;">
            
			<div class="card p-3a col-12 col-md-4 col-lg-4">
                <div class="item"><img class="wow animated" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133356/design1.png" />
                    <div class="item-overlay top"></div>
                </div>
            </div>
			
			<div class="card p-3a col-12 col-md-4 col-lg-4">
                <div class="item"><img class="wow animated" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133348/design2.png" />
                    <div class="item-overlay top"></div>
                </div>
            </div>
			
			<div class="card p-3a col-12 col-md-4 col-lg-4">
                <div class="item"><img class="wow animated" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133349/design3.png" />
                    <div class="item-overlay top"></div>
                </div>
            </div>
		
        </div>
		
		<div class="media-container-row">
            
			<div class="card p-3a col-12 col-md-4 col-lg-4">
                <div class="item"><img class="wow animated" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133350/design4.png" />
                    <div class="item-overlay top"></div>
                </div>
            </div>
			
			<div class="card p-3a col-12 col-md-4 col-lg-4">
                <div class="item"><img class="wow animated" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133352/design5.png" />
                    <div class="item-overlay top"></div>
                </div>
            </div>
			
			<div class="card p-3a col-12 col-md-4 col-lg-4">
                <div class="item"><img class="wow animated" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133354/design6.png" />
                    <div class="item-overlay top"></div>
                </div>
            </div>
		
        </div>

<div class="navbar-buttons design-section-btn mgb1 mg2 mgt3" align="center">
<a class="btn btn-sm btn-primary display-4" href="<?php echo site_url('pricing/'); ?>">Get Started Now</a>
<a class="btn btn-sm btn-primary1 display-4" href="<?php echo site_url('works/'); ?>">See All Works</a>
</div>

    </div>
    </div>
</section>


<section>
<div class="container"><div class="media-container-row" align="center">
		<div class="card p-3a col-12 col-md-12 col-lg-12 mgb4">
		<div class="testim">
		All time design has made it quick and easy for our team to create a variety of eye-catching marketing materials. Being able to communicate directly with the designer about our needs, which helps to ensure that we get the best possible final product, every time
		</div>
		
		<div class="pdt3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/13131840/Testimonials-avatar.png">&nbsp;&nbsp;<span class="testi-txt">Caroline Hung,</span> <span class="testi-txt1">Senior Medical Manager, Sanofi</span></div>
		
		</div>
		


</div><hr /></div>
</section>



<section class="features1 design-cont mgt1">
<div align="center" class="home-title">Our Features</div>
    <div class="container">
        <div class="media-container-row">

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-hearth" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133853/Unlimited-design.png"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">
                        Unlimited Designs</h4>
                    <p class="many-text many-fonts-style display-7">
                        Unlimited Pixel-Perfect designs that are in line with the given context.
                    </p>
                </div>
            </div>

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-rocket" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133852/Quick-Turnarounds.png"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">Quick Turnarounds</h4>
                    <p class="many-text many-fonts-style display-7">
                        We are a fast but realistic team, working constantly towards quick project delivery.
                    </p>
                </div>
            </div>

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-globe" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133857/Dedicated-Team.png"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">Dedicated Team</h4>
                    <p class="many-text many-fonts-style display-7">Get assigned to a reliable design expert account manager for personalized assistance.</p>
                </div>
            </div>


        </div>
    </div>

<div class="container">
        <div class="media-container-row pdb3 mgb2">

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-hearth" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133854/Unlimited-Revisions.png"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">
                        Unlimited Revisions</h4>
                    <p class="many-text many-fonts-style display-7">
                        Our approachable team is all ready to work on your revisions, to make your vision come alive.
                    </p>
                </div>
            </div>

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-rocket" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133855/Constant-Updates.png"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">Constant Updates</h4>
                    <p class="many-text many-fonts-style display-7">
                        Stay updated on a day to day basis about the status of your request.
                    </p>
                </div>
            </div>

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-globe" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/09/05133851/Fixed-Affordable-Price.png"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">Fixed Affordable Price</h4>
                    <p class="many-text many-fonts-style display-7">Pay a flat price for our services and it’s nothing compared to what design agencies charge.</p>
                </div>
            </div>

        </div>
		<hr />
    </div>
</section>





<section class="features1 design-cont">
<div class="container">
<div align="center" class="main-info pdb2">All Your Designs At a Flat Rate</div>

<div align="center" style="margin-top: 20px; margin-bottom:30px;">
<span class="dm_prizing_discount chk-txt">Monthly Plan</span>
<label class="switch dm_prizing_switch">
<input type="checkbox" name="dm_prizing_radio" class="dm_checked" checked>
<span class="slider round"></span>
</label>
<span class="dm_prizing_discount chk-txt">Yearly Plan<!--&nbsp;&nbsp;&nbsp;<span class="offer">20% Off</span>--></span>
</div>

</div>

<select id="n1" class="form-control" onClick="sync()" onKeyUp="sync()" onKeyPress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" style="width:64.5%; background-color: #fffff !important; border: 1px solid #d4d7dd !important; border-radius: 5px; padding-left:12px; width:10px; display:none;">
  <option value="1">01&nbsp;&nbsp;&nbsp;Dedicated Designer</option>
          <option value="2">02&nbsp;&nbsp;&nbsp;Dedicated Designer</option>
		  <option value="3">03&nbsp;&nbsp;&nbsp;Dedicated Designer</option>
		  <option value="4">04&nbsp;&nbsp;&nbsp;Dedicated Designer</option>
		  <option value="5">05&nbsp;&nbsp;&nbsp;Dedicated Designer</option>
		  <option value="6">06&nbsp;&nbsp;&nbsp;Dedicated Designer</option>
		  <option value="7">07&nbsp;&nbsp;&nbsp;Dedicated Designer</option>
		  <option value="8">08&nbsp;&nbsp;&nbsp;Dedicated Designer</option>
		  <option value="9">09&nbsp;&nbsp;&nbsp;Dedicated Designer</option>
		  <option value="10">10&nbsp;&nbsp;&nbsp;Dedicated Designer</option>
</select>
</section>


<div class="container mb101">
<div class="row">

<div class="col-sm-2">&nbsp;</div>

<div class="col-sm-4" style="border: 1.5px solid #EDF2FA; padding-top:60px;">
<div class="pri-title">STANDARD</div>
<div class="dm_prizing1">
<div class="cd-price">		
<span class="curr">$</span>		
<span class="dm_prizing_number" id="n2">349</span><span  class="dm_prizing_para4">/&nbsp;mo</span>				
</div>
<div class="pri-des">Saving $50 a month and $600 a year when billed annually</div>
</div>
<div class="dm_prizing2">
<div class="cd-price">		
<span class="curr">$</span>						
<span class="dm_prizing_number" id="n3">299</span><span  class="dm_prizing_para4">/&nbsp;mo</span>
</div>
<div class="pri-des">Saving $50 a month and $600 a year when billed annually</div>
</div>
<div class="navbar-buttons design-section-btn mgt3" align="center">
<input type="email" name="standard" id="email1" class="pricing-sub stripe_email" placeholder="Enter Your Email Address" value="<?php //echo $get_star_email; ?>" />
<br />
<a id="customButton" class="btn btn-sm btn-primary display-4 pri-but" href="#" style="margin-bottom:50px;">Buy Now</a>
</div>
</div>


<div class="col-sm-4" style="border: 1.5px solid #EDF2FA; padding-top:60px; border-left:0px; background-color:#FAFBFC;">
<div class="pri-title">DESIGN IN A DAY</div>
<div class="dm_prizing1">
<div class="cd-price">		
<span class="curr">$</span>		
<span class="dm_prizing_number" id="n4">999</span><span  class="dm_prizing_para4">/&nbsp;mo</span>				
</div>
<div class="pri-des">Saving $100 a month and $1200 a year when billed annually</div>
</div>
<div class="dm_prizing2">
<div class="cd-price">		
<span class="curr">$</span>						
<span class="dm_prizing_number" id="n5">899</span><span  class="dm_prizing_para4">/&nbsp;mo</span>
</div>
<div class="pri-des">Saving $100 a month and $1200 a year when billed annually</div>
</div>
<div class="navbar-buttons design-section-btn mgt3" align="center">
<input type="email" name="design" id="email2" class="pricing-sub stripe_email1" placeholder="Enter Your Email Address" value="<?php //echo $get_star_email; ?>" />
<br />
<a id="customButton1" class="btn btn-sm btn-primary display-4 pri-but" href="#" style="margin-bottom:50px;">Buy Now</a>
</div>
</div>


<div class="col-sm-2 desk-show">&nbsp;</div>

</div>
</div>


<section style="background:url('<?php echo get_template_directory_uri(); ?>/assets/images/Main_banner.png');">
<div class="container"><div class="media-container-row pdlr2">

<div class="col-12 col-md-12 col-lg-12">
<div class="home-title" style="color:#FFFFFF;">Join our email newsletter list</div>
<div class="call-act mgt1a mglr10" style="color:#FFFFFF; text-align:center; opacity:0.5; font-weight:400; font-size: 16px; line-height: 26px;">Services at All Time Design are top-notch. You don't have to blindly trust out words. Try us 100% risk-free for two whole weeks.</div>

<div id='search-box' class="blog-search newsletter">
<?php mailchimpSF_signup_form(); ?>
</div>

</div>

</div></div>
</section>

<?php get_footer(); ?>