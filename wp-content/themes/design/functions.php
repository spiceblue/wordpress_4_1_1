<?php 

//header

function add_stylesheet_to_head()
{
	echo '<link rel="stylesheet" href="'. get_template_directory_uri().'/style.css">';
    echo '<link rel="stylesheet" href="'. get_template_directory_uri().'/assets/css/bootstrap.min.css">';
	echo '<link rel="stylesheet" href="'. get_template_directory_uri().'/assets/css/bootstrap-grid.min.css">';
	echo '<link rel="stylesheet" href="'. get_template_directory_uri().'/assets/css/bootstrap-reboot.min.css">';
	echo '<link rel="stylesheet" href="'. get_template_directory_uri().'/assets/css/navbar.css">';
	echo '<link rel="stylesheet" href="'. get_template_directory_uri().'/assets/css/default.css">';
	echo '<link rel="stylesheet" href="'. get_template_directory_uri().'/assets/css/additional.css">';		
}

add_action('wp_head', 'add_stylesheet_to_head');

//footer

function scripts_with_jquery()

{
	// Register the script like this for a theme:

	wp_register_script( 'jquery-min', get_template_directory_uri() . '/assets/js/jquery.min.js', array( 'jquery' ) );
	wp_register_script( 'bootstrap-min', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ) );
	wp_register_script( 'smooth-scroll', get_template_directory_uri() . '/assets/js/smooth-scroll.js', array( 'jquery' ) );
	wp_register_script( 'script-min', get_template_directory_uri() . '/assets/js/script.min.js', array( 'jquery' ) );
	wp_register_script( 'jarallax-min', get_template_directory_uri() . '/assets/js/jarallax.min.js', array( 'jquery' ) );
	wp_register_script( 'gen-script', get_template_directory_uri() . '/assets/js/script.js', array( 'jquery' ) );

	// For either a plugin or a theme, you can then enqueue the script:

	wp_enqueue_script( 'jquery-min' );
	
	wp_enqueue_script( 'bootstrap-min' );
	
	wp_enqueue_script( 'smooth-scroll' );	
	
	wp_enqueue_script( 'script-min' );
	
	wp_enqueue_script( 'jarallax-min' );
	
	wp_enqueue_script( 'gen-script' );
	
}

add_action( 'wp_footer', 'scripts_with_jquery' );

/*---remove p and br -----*/
remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );

/* Create Customer Ticket */
function create_ticket($ticket_payload, $userid ){
	$api_key = "KdvhiisHUYoa8cnykg";
	$password = "X";
	$yourdomain = "alltimedesign";
	
	$url = "https://$yourdomain.freshdesk.com/api/v2/tickets";
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_USERPWD, "$api_key:$password");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $ticket_payload);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$server_output = curl_exec($ch);
	$info = curl_getinfo($ch);
	$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
	$headers = substr($server_output, 0, $header_size);
	$response = substr($server_output, $header_size);
	if($info['http_code'] == 201) {
		$ticket_info = json_decode($response, true);
		$ticket_date_time = preg_split( "/(T|Z)/", $ticket_info['created_at'] );
		$ticket_date = $ticket_date_time[0];
		$ticket_time = $ticket_date_time[1];				
		// $ticket_date = date_create($ticket_date);
		// $ticket_time = date_create($ticket_time);
		$ticket_status = ( $ticket_info['status'] == 2 ) ? '<span class="status-open">Open</span>' : ( ( $ticket_info['status'] == 4 ) ? '<span class="status-delivered">Delivered</span>' : ( ( $ticket_info['status'] == 5 ) ? '<span class="status-closed">Closed</span>' : '' ) );
		$ticket_subject = $ticket_info['subject'];
		$tiket_description = $ticket_info['description'];
		
		foreach( $ticket_info['attachments'] as $attachment ){
			$attch .='<div class="ad_image_panel"> <a href="'.$attachment['attachment_url'].'" download target="_blank"><span class="att_name">'.$attachment['name'].'</span>';
			$attch .='</a></div>';
		}
		
		$result = '<div class="media-container-row">';
		
		$result .='<div class="card p-3 col-12 col-md-2 col-lg-2"><span class="period">'.date_format($ticket_date,'M d, Y').'</span></div>';
		
		$result .='<div class="card p-3 col-12 col-md-10 col-lg-10"><div class="card-box"><ul id="accordion" class="accordion"><li class="acc" style="background:#fff;"> <div class="link"><img class="icon" src="https://s3-ap-south-1.amazonaws.com/spiceblue-design/wp-content/uploads/2018/07/13131150/logo_design_dash.png"> <span class="title">'.$ticket_subject.'</span>'.$ticket_status.'<span class="date-time">'.timeAgo(date(date_format($ticket_date, 'Y-m-d').' '.date_format($ticket_time, 'H:m:s'))).'</span><i class="fa fa-chevron-right"></i></div>';
		
		$result .= '<div class="submenu" style="background:#fff;"><div id="chat-area"><div class="customer_conv"><div class="ad_customer_conv">';		     
		$user_img = get_user_meta($userid ,'profile_image', true);
		$user_first = get_user_meta($userid ,'first_name', true);
		$user_last = get_user_meta($userid ,'last_name', true);
		if( $user_img['url'] != '' ){
			$usr_prf = '<div class="ad_image"><img src="'.$user_img['url'].'" alt="att"></div>';
		} else{
			$usr_prf = '<div class="ad_image_text">'.strtoupper(mb_substr($user_first, 0, 1).mb_substr($user_last, 0, 1)).'</div>';
		}
		$result .= $usr_prf.$tiket_description.''.$attch.'</div></div></div><div class="text_box"><form method="post" class="reply_form" enctype="multipart/form-data"><div class="embed-submit-field" id="txt-box" style=""><textarea class="mes" name="reply_message" placeholder="Enter your reply..."></textarea><div class="att"><input type="file" class="filebtn" name="repl_image" accept="image/*,.pdf" /></div> <input type="hidden" name="ticket_id" value="'.$ticket_info['id'].'" /> <button type="submit" class="btn_reply" id="reply">Reply</button></div></form></div><div id="initial-button" class="initial_button">Reply<div></div></div></div></div>';		
		$result .= '</li></ul></div>';		
		$result .='</div>';
	  return $result;
	} else {
	  echo 'Error Please try again....';
	}
	curl_close($ch);
}

/* update Customer Ticket */
function update_ticket($ticket_payload_update,$ticket_id){

$api_key = "KdvhiisHUYoa8cnykg";
$password = "X";
$yourdomain = "alltimedesign";

$url = "https://$yourdomain.freshdesk.com/api/v2/tickets/$ticket_id";

$ch = curl_init($url);

curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_USERPWD, "$api_key:$password");
curl_setopt($ch, CURLOPT_POSTFIELDS, $ticket_payload_update);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$server_output = curl_exec($ch);
$info = curl_getinfo($ch);
$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
$headers = substr($server_output, 0, $header_size);
$response = substr($server_output, $header_size);

if($info['http_code'] == 200) {
  echo "Contact updated successfully, the response is given below \n";
  echo "Response Headers are \n";
  echo $headers."\n";
  echo "Response Body \n";
  echo "$response \n";
} else {
  if($info['http_code'] == 404) {
    echo "Error, Please check the end point \n";
  } else {
    echo "Error, Please check the end point \n";
  }
}
  curl_close($ch);

}



/* Get Ticket API calls */
function get_users_tickets($user_email){
	$api_key = "KdvhiisHUYoa8cnykg";
	$password = "X";
	$yourdomain = "alltimedesign";
	$url = "https://$yourdomain.freshdesk.com/api/v2/tickets?email=$user_email";

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_USERPWD, "$api_key:$password");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$server_output = curl_exec($ch);
	$info = curl_getinfo($ch);
	$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
	$headers = substr($server_output, 0, $header_size);
	$response = substr($server_output, $header_size);
	if($info['http_code'] == 200) {
		
	  $tickets = json_decode($response, true);
	  return $tickets;
	}
}

function get_user_conversation($ticket_id){
	$api_key = "KdvhiisHUYoa8cnykg";
	$password = "X";
	$yourdomain = "alltimedesign";
	$url = "https://$yourdomain.freshdesk.com/api/v2/tickets/".$ticket_id."?include=conversations";
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_USERPWD, "$api_key:$password");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$server_output = curl_exec($ch);
	$info = curl_getinfo($ch);
	$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
	$headers = substr($server_output, 0, $header_size);
	$response = substr($server_output, $header_size);
	return json_decode($response, true);
}

function timeAgo($timestamp){
    $datetime1=new DateTime("now");
    $datetime2=new DateTime($timestamp);
    $diff=date_diff($datetime1, $datetime2);
    $timemsg='';
    if($diff->y > 0){
        $timemsg = $diff->y .' year'. ($diff->y > 1?"'s":'');

    }
    else if($diff->m > 0){
     $timemsg = $diff->m . ' month'. ($diff->m > 1?"'s":'');
    }
    else if($diff->d > 0){
     $timemsg = $diff->d .' day'. ($diff->d > 1?"'s":'');
    }
    else if($diff->h > 0){
     $timemsg = $diff->h .' hour'.($diff->h > 1 ? "'s":'');
    }
    else if($diff->i > 0){
     $timemsg = $diff->i .' minute'. ($diff->i > 1?"'s":'');
    }
    else if($diff->s > 0){
     $timemsg = $diff->s .' second'. ($diff->s > 1?"'s":'');
    }

$timemsg = $timemsg.' ago';
return $timemsg;
}


function reply_message_ticket(){	
	$api_key = "KdvhiisHUYoa8cnykg";
	$password = "X";
	$yourdomain = "alltimedesign";
	$ticket_id = $_POST['ticket_id'];
	$userid =  $_POST['userid'];
	$user_email= $_POST['user_email'];
	if($_FILES){
	    $reply_payload= array(
		  'body' => $_POST['reply_message'],
		  'attachments[]' => curl_file_create($_FILES['file']['tmp_name'], $_FILES['file']['type'], $_FILES['file']['name'])
		);
	}else{
		$reply_payload = array(
		  'body' => $_POST['reply_message']
		);
	}
	$url = "https://$yourdomain.freshdesk.com/api/v2/tickets/$ticket_id/reply";
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_USERPWD, "$api_key:$password");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $reply_payload);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$server_output = curl_exec($ch);
	$info = curl_getinfo($ch);
	$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
	$headers = substr($server_output, 0, $header_size);
	$response = substr($server_output, $header_size);
	if($info['http_code'] == 201) {
		$reply_msge = json_decode($response, true);
		$user_img = get_user_meta($userid ,'profile_image', true);
		$user_first = get_user_meta($userid ,'first_name', true);
		$user_last = get_user_meta($userid ,'last_name', true);
		$users_full_name = $user_first." ".$user_last;
        $chat_date_time = preg_split( "/(T|Z)/", $reply_msge['created_at'] );
	 	$chat_date = $chat_date_time[0];
		$chat_time = $chat_date_time[1];
		
		if( $user_img['url'] != '' ){
			$usr_prf = '<img src="'.$user_img['url'].'" alt="att" style="width:55px; height:55px; border-radius:50%;">';
		} else{
			$usr_prf = '<div class="ad_image_text" style="width:45px; height:45px; border-radius:50%;">'.strtoupper(mb_substr($user_first, 0, 1).mb_substr($user_last, 0, 1)).'</div>';
		}
		$result = '
		<div class="media-container-row padd1" style="background-color: #FCFCFC;">
		<div class="card p-3 col-12 col-md-1 col-lg-1">
		'.$usr_prf.'</div><div class="card p-3 col-12 col-md-11 col-lg-11">';
		if($users_full_name != ""){
		 $result .= '<div class="req-title">'.$users_full_name.'<span>'.timeAgo($chat_date.' '.$chat_time).'</span><span>('.$chat_date .')</span></div>';
		}
		 else{
        $result .= '<div class="req-title">'.$user_email.'<span>'.timeAgo($chat_date.' '.$chat_time).'</span><span>('.$chat_date .')</span></div>';
		 }
		 

												$result .= '<div class="req-txt1"><pre>'.$reply_msge['body_text'].'</pre></div>';
		foreach( $reply_msge['attachments'] as $attachment ){
			$attch .='<div style="margin-top:30px; margin-bottom:10px;"> <a href="'.$attachment['attachment_url'].'" download target="_blank"><span>';
			
			if( end(explode('.',$attachment['name'])) == "pdf")
														{
															$attch .= '<span class="icon-bg"><img src="'.get_template_directory_uri().'/assets/images/pdf.png"></span>';

														}
														elseif( end(explode('.',$attachment['name'])) == "psd"){
                                                           $attch .='<span class="icon-bg"><img src="'.get_template_directory_uri().'/assets/images/ps.png"></span>';
														}
                										else{
                											 $attch .='<span class="icon-bg"><img src="'.get_template_directory_uri().'/assets/images/jpg.png"></span>';
                										}
			
			$attch .='</span></a></div>';
		}
		$attch .='</div></div>';
		$result .= $attch;
		$result .='';
		$result .='	</div>';	
		$result .= '</div>';
		echo $result;
	} 
	curl_close($ch);	
    die;
}

add_action('wp_ajax_reply_message_ticket', 'reply_message_ticket');
add_action('wp_ajax_nopriv_reply_message_ticket', 'reply_message_ticket');


function make_request_ticket(){	
	$api_key = "KdvhiisHUYoa8cnykg";
	$password = "X";
	$yourdomain = "alltimedesign";
	$userid =  $_POST['userid'];
	if( $_POST['inspiration'] != '' ){
		$description =  $_POST['message'].'<br><b>Inspiration URL:</b>'.$_POST['inspiration'];
	}else{
		$description =  $_POST['message']; 
	}
	
	if($_FILES['file']['name']){
		$ticket_payload = array(
		  'email' => $_POST['email'],
		  'subject' => $_POST['subject'],
		  'description' => $description,
		  'priority' => 2,
		  'status' => 2,
		  'attachments[]' =>  curl_file_create($_FILES['file']['tmp_name'], $_FILES['file']['type'], $_FILES['file']['name'])
		);
	}else{
		$ticket_payload = array(
		  'email' => $_POST['email'],
		  'subject' => $_POST['subject'],
		  'description' => $description,
		  'priority' => 2,
		  'status' => 2
		);
	}
	$ticket_created = create_ticket($ticket_payload, $userid );
	print_r($ticket_created); 
    die;
}

add_action('wp_ajax_make_request_ticket', 'make_request_ticket');
add_action('wp_ajax_nopriv_make_request_ticket', 'make_request_ticket');

//update ticket

function mark_message_as_update(){
	$api_key = "KdvhiisHUYoa8cnykg";
	$password = "X";
	$yourdomain = "alltimedesign";
	$ticket_id = $_POST['update_ticket_status'];
	$ticket_payload_update = array(
		'status' => 5
	);

	$ticket_update = update_ticket($ticket_payload_update,$ticket_id);
	print_r($ticket_update);
}

add_action('wp_ajax_mark_message_as_update', 'mark_message_as_update');
add_action('wp_ajax_nopriv_mark_message_as_update', 'mark_message_as_update');




function my_auto_login( $user_id ) {
    wp_set_auth_cookie( $user_id, false, is_ssl() );
}

function change_password(){
	$user = wp_get_current_user();
	if ( $user && wp_check_password( $_POST['old_pass'], $user->data->user_pass, $user->ID) ){
		if( $_POST['new_oass'] == $_POST['cnf_pass'] ){
		   wp_set_password( $_POST['new_oass'], $user->ID );
		   echo 3;
	    }else{
			echo 2;
		}
	}else{
	   echo 1;
	}
	die;
}
add_action('wp_ajax_change_password', 'change_password');
add_action('wp_ajax_nopriv_change_password', 'change_password');

function update_details_usr(){
	$user = wp_get_current_user();
	if( ($user->first_name != $_POST['fname']) || ($user->last_name != $_POST['lname']) ){
		echo 2;
	}
	update_user_meta( $user->ID , 'first_name', $_POST['fname'] );
	update_user_meta( $user->ID , 'last_name', $_POST['lname'] );
	if( $_POST['email'] != $user->user_email ){
		if( email_exists($_POST['email']) == false ){
			/* wp_update_user( array(
				'ID' => $user->ID,
				'user_email' => $_POST[ 'user_email' ]
			) );  */
			echo 2;
		}else{
			echo 1;
		}
	}
	die;
}
add_action('wp_ajax_update_details_usr', 'update_details_usr');
add_action('wp_ajax_nopriv_update_details_usr', 'update_details_usr');

function get_start_user(){
	$user_first_name 	= $_POST['fname'];
	$user_last_name 	= $_POST['lname'];
	$user_email 		= $_POST['email'];
	$user_password 		= $_POST['password'];
	$user_plan 			= $_POST['plan_user'];
	if( $user_email != '' ){
		$user_id = username_exists( $user_email );
		if ( !$user_id and email_exists($user_email) == false ) {
			$user_id = wp_create_user( $user_email, $user_password, $user_email );
			update_user_meta( $user_id, 'first_name', $user_first_name);
			update_user_meta( $user_id, 'last_name', $user_last_name);
			update_user_meta( $user_id, 'user_plan', $user_plan);
			/* $subject = 'Welcome to All Time Design!';
			$message = 'Hi, <br> <p>Thanks for signing up. We are happy to see you get started.</p><p>You are almost there. Go ahead and make your payment to enjoy our unparalleled designs.</p><p>Make Payment</p><p>Thanks,</p></br><p>Team All Time Design</p>';
			$headers[] = 'From: All Time Design <hellow@alltimedesign.com>';
			wp_mail($user_email, $subject, $message, $headers); */
			my_auto_login($user_id);
			echo json_encode(array('success' => true, 'result' => $user_id));
		}else{
			echo json_encode(array('success' => false, 'result' => ''));
		}
	}
	die;
}
add_action('wp_ajax_get_start_user', 'get_start_user');
add_action('wp_ajax_nopriv_get_start_user', 'get_start_user');

function user_payment_option(){
	require_once("Stripe/config.php"); 
	require_once("Stripe/create-subscription.php"); 
	if( $return['status'] ){
		$user_email = $return['msge']['useremail'];
		if( $user_email != '' ){
			$user_id = username_exists( $user_email );
			if ( !$user_id and email_exists($user_email) == false ) {
				$user_password = wp_generate_password( 8, false );
				$user_id = wp_create_user( $user_email, $user_password, $user_email );
            	update_user_meta( $user_id, 'created', $return['msge']['created']);
            	update_user_meta( $user_id, 'subscribtion_start', $return['msge']['subscribtion_start']);
            	update_user_meta( $user_id, 'subscribtion_end', $return['msge']['subscribtion_end']);
            	update_user_meta( $user_id, 'plan', $return['msge']['plan']);
            	update_user_meta( $user_id, 'amount', $return['msge']['amount']);
            	update_user_meta( $user_id, 'currency', $return['msge']['currency']);
            	update_user_meta( $user_id, 'status', $return['msge']['status']);
                update_user_meta( $user_id, 'customer_id', $return['msge']['customer_id']);
            	update_user_meta( $user_id, 'subscribtion_data', $return['msge']['subscribtion_data']);
            	update_user_meta( $user_id, 'newuser', 1); 
				my_auto_login($user_id);
				echo json_encode(array('success' => true, 'result' => $user_id, 'user_email' => $user_email));
			}else{
				echo json_encode(array('success' => false, 'result' => $_POST['stripe_email']));
			}
		}		
	}else{
		echo json_encode(array('success' => false, 'result' => $_POST['stripe_email']));
	}
	die;
}
add_action('wp_ajax_user_payment_option', 'user_payment_option');
add_action('wp_ajax_nopriv_user_payment_option', 'user_payment_option');

function user_profile_change(){
	$current_user = wp_get_current_user();
	if( $_FILES['image']['error'] === UPLOAD_ERR_OK ) {
		$upload_overrides = array( 'test_form' => false );
		$r = wp_handle_upload( $_FILES['image'], $upload_overrides );
		update_user_meta( $current_user->ID, 'profile_image', $r );
		echo 'success';
	} 
	die;
}
add_action('wp_ajax_user_profile_change', 'user_profile_change');
add_action('wp_ajax_nopriv_user_profile_change', 'user_profile_change');

function customer_retrive($customer_id){
	require_once("Stripe/config.php"); 
	require_once("Stripe/customer_data.php"); 
	return $cus_crd_data;
}
function update_card_details_submit(){
	$card_num = $_POST['cardnumber'];
	$expmonth = $_POST['expmonth'];
	$cvv = $_POST['cvv'];
	require_once("Stripe/config.php"); 
	require_once("Stripe/customer_data.php");
	die;
}
add_action('wp_ajax_update_card_details_submit', 'update_card_details_submit');
add_action('wp_ajax_nopriv_update_card_details_submit', 'update_card_details_submit');

function upgrade_user_plan(){
	$current_user = wp_get_current_user();
	$customer_id = get_user_meta( $current_user->ID, 'customer_id', true );
	$subscribtion_data = get_user_meta( $current_user->ID, 'subscribtion_data', true );
	$subcrb_id = $subscribtion_data['id'];
	require_once("Stripe/config.php"); 
	require_once("Stripe/customer_data.php");
	print_r($user_arr);
	die;
}
add_action('wp_ajax_upgrade_user_plan', 'upgrade_user_plan');
add_action('wp_ajax_nopriv_upgrade_user_plan', 'upgrade_user_plan');

function go_home(){
	wp_logout();
	wp_redirect( home_url() );
	ob_clean();
	exit();
}
add_action( 'wp_ajax_logout_user', 'go_home' );
add_action( 'wp_ajax_nopriv_logout_user', 'go_home' ); 

function wpdocs_set_html_mail_content_type() {
    return 'text/html';
}
add_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' );

add_filter( 'wp_mail_from_name', 'custom_wp_mail_from_name' );
function custom_wp_mail_from_name( $original_email_from ) {
	return 'All Time Design';
}

function codecanal_reset_password_message( $message, $key ) {
    if ( strpos($_POST['user_login'], '@') ) {
        $user_data = get_user_by('email', trim($_POST['user_login']));
    } else {
        $login = trim($_POST['user_login']);
        $user_data = get_user_by('login', $login);
    }

    $user_login = $user_data->user_login;

    $msg = __('<p>We have received a request to reset your password. </p>'). "\r\n\r\n";
    $msg .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
    $msg .= __('<p>Click on the link below to change your current password.</p>') . "\r\n\r\n";
    $msg .= '<a href="'. site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') .'">Click Here</a>'."\r\n";
    $msg .= '<p>Thanks,</p></br><p>Team All Time Design</p>';

    return $msg;
}
add_filter('retrieve_password_message', 'codecanal_reset_password_message', null, 2);

add_filter( 'jpeg_quality', create_function('', 'return 50;' ) ); 

function get_customer_invoice( $customer_invoice ) {
	require_once("Stripe/customer_invoice.php"); 
	return $cu;
}

/* $headers[] = 'From: Me Myself <hello@alltimedesign.com>';
$headers[] = 'Cc: John Q Codex <joy@spiceblue.co>';
$headers[] = 'Cc: joy@spiceblue.co';
wp_mail('vigneshwaran@spiceblue.co', 'We had a problem while billing your account', 'test', $headers); */

function ajaxforgotpassword(){	
	global $wpdb;	
	$account = $_POST['user_login'];	
	if( empty( $account ) ) {
		$error = 'Enter an username or e-mail address.';
	} else {
		if(is_email( $account )) {
			if( email_exists($account) ) 
				$get_by = 'email';
			else	
				$error = 'There is no user registered with that email address.';			
		}
		else if (validate_username( $account )) {
			if( username_exists($account) ) 
				$get_by = 'login';
			else	
				$error = 'There is no user registered with that username.';				
		}
		else
			$error = 'Invalid username or e-mail address.';		
	}	
	
	if(empty ($error)) {
		$random_password = wp_generate_password();
		$user = get_user_by( $get_by, $account );
			
		$update_user = wp_update_user( array ( 'ID' => $user->ID, 'user_pass' => $random_password ) );
		if( $update_user ) {			
			$from = 'hello@alltimedesign.com';			
			if(!(isset($from) && is_email($from))) {		
				$sitename = strtolower( $_SERVER['SERVER_NAME'] );
				if ( substr( $sitename, 0, 4 ) == 'www.' ) {
					$sitename = substr( $sitename, 4 );					
				}
				$from = 'admin@'.$sitename; 
			}
			
			$to = $user->user_email;
			$subject = 'Reset your password';
			$sender = 'From: '.get_option('name').' <'.$from.'>' . "\r\n";
			
			$msg = __('<p>We have received a request to reset your password. </p>'). "\r\n\r\n";
			$msg .= sprintf(__('<p>Username: %s</p>'), $account) . "\r\n\r\n";
			$msg .= 'Your new password is: '.$random_password;
			$msg .= '<p>Thanks,</p></br><p>Team All Time Design</p>';
				
			$headers[] = 'MIME-Version: 1.0' . "\r\n";
			$headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers[] = "X-Mailer: PHP \r\n";
			$headers[] = $sender;
				
			$mail = wp_mail( $to, $subject, $msg, $headers );
			if( $mail ) 
				$success = 'Check your email address for you new password.';
			else
				$error = 'System is unable to send you mail containg your new password.';						
		} else {
			$error = 'Oops! Something went wrong while updaing your account.';
		}
	}	
	if( ! empty( $error ) )
		echo json_encode(array('loggedin'=>false, 'message'=>__($error)));			
	if( ! empty( $success ) )
		echo json_encode(array('loggedin'=>true, 'message'=>__($success)));				
	die();	
}
add_action( 'wp_ajax_ajaxforgotpassword', 'ajaxforgotpassword' );
add_action( 'wp_ajax_nopriv_ajaxforgotpassword', 'ajaxforgotpassword' ); 



//image in post page
add_theme_support( 'post-thumbnails' );

//pagination
function design_pagination() {
    if( is_singular() )
        return;
    global $wp_query;
    /* Stop the code if there is only a single page page */
    if( $wp_query->max_num_pages <= 1 )
        return;
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
    /*Add current page into the array */
    if ( $paged >= 1 )
        $links[] = $paged;
    /*Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
    echo '<div class="navigation"><ul>' . "\n";
    /*Display Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );
    /*Display Link to first page*/
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }
    /* Link to current page */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }
    /* Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";
        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }
    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link() );
    echo '</ul></div>' . "\n";
}

function password_update(){
	if( $_POST['new_pass']  == $_POST['confrm_pass'] ) {
		$password = sanitize_text_field($_POST['new_pass']);
		$userdata = array(
            'ID'        =>  $_POST['user_id'],
            'user_pass' =>  $password 
        );
		$user_id = wp_update_user($userdata);
		echo json_encode(array('success' => true, 'result' => 'Passsword set successfully..!'));
	}else{
		echo json_encode(array('success' => false, 'result' => 'New password and Conform Password are not match.....'));
	}
	die;
}
add_action('wp_ajax_password_update', 'password_update');
add_action('wp_ajax_nopriv_password_update', 'password_update');


//captcha
add_filter( 'wpcf7_use_really_simple_captcha', '__return_true' );
?>

