<?php
/**
 * Template Name: Works Page
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage All Time Design
 * @since All Time Design 1.0
 */

get_header(); ?>
<section class="features1 design-cont mgt0">
<div class="container">
    <div align="center" class="main-info pdb2">Our Latest Client Projects</div>
    <div align="center" class="scope-txt"><div class="mglr9">Our works are always designed to tell a story. A story that will satisfy your purpose.</div>
    </div>
    <div class="container work-img hes-gallery" style="padding-top: 100px; padding-bottom: 150px;">
        <div class="media-container-row">
            <div class="card p-3a col-12 col-md-6 col-lg-6">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144101/Image1.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
            <div class="card p-3a col-12 col-md-6 col-lg-6">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144104/Image2.png" />

                    <div class="item-overlay top"></div>
                </div>
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144106/Image3.png" />

                    <div class="item-overlay top"></div>
                </div>
            </div>
        </div>
        <div class="media-container-row">
            <div class="card p-3a col-12 col-md-3 col-lg-3">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144108/Image4.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
            <div class="card p-3a col-12 col-md-6 col-lg-6">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144110/Image5.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
            <div class="card p-3a col-12 col-md-3 col-lg-3">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144114/Image6.png" />

                    <div class="item-overlay top"></div>
                </div>
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144116/Image7.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
        </div>
        <div class="media-container-row">

            <div class="card p-3a col-12 col-md-3 col-lg-3">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144123/Image11.png" />

                    <div class="item-overlay top"></div>
                </div>
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144125/Image12.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
            <div class="card p-3a col-12 col-md-3 col-lg-3">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144121/Image10.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>

            <div class="card p-3a col-12 col-md-6 col-lg-6">

                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144118/Image9.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>

        </div>
        <div class="media-container-row">
            <div class="card p-3a col-12 col-md-6 col-lg-6">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144127/Image13.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
            <div class="card p-3a col-12 col-md-6 col-lg-6">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/08/22065522/Image14.png" />

                    <div class="item-overlay top"></div>
                </div>
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144133/Image15.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
        </div>
        <div class="media-container-row">
            <div class="card p-3a col-12 col-md-12 col-lg-12">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144136/Image16.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
        </div>
        <div class="media-container-row">
            <div class="card p-3a col-12 col-md-6 col-lg-6">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144221/Image36.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
            <div class="card p-3a col-12 col-md-3 col-lg-3">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144142/Image18.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
            <div class="card p-3a col-12 col-md-3 col-lg-3">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144144/Image19.png" />

                    <div class="item-overlay top"></div>
                </div>
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144146/Image20.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
        </div>
        <div class="media-container-row">
            <div class="card p-3a col-12 col-md-6 col-lg-6">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144140/Image17.png" />

                    <div class="item-overlay top"></div>
                </div>
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144148/Image21.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
            <div class="card p-3a col-12 col-md-6 col-lg-6">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144150/Image22.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
        </div>
        <div class="media-container-row">
            <div class="card p-3a col-12 col-md-12 col-lg-12">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144154/Image23.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
        </div>
        <div class="media-container-row">
            <div class="card p-3a col-12 col-md-6 col-lg-6">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144158/Image24.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
            <div class="card p-3a col-12 col-md-6 col-lg-6">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144201/Image25.png" />

                    <div class="item-overlay top"></div>
                </div>
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144203/Image26.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
        </div>
        <div class="media-container-row">
            <div class="card p-3a col-12 col-md-6 col-lg-6">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144206/Image27.png" />

                    <div class="item-overlay top"></div>
                </div>
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144208/Image28.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
            <div class="card p-3a col-12 col-md-6 col-lg-6">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144210/Image30.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
        </div>
        <div class="media-container-row">

            <div class="card p-3a col-12 col-md-6 col-lg-6">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144213/Image31.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
            <div class="card p-3a col-12 col-md-3 col-lg-3">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144217/Image34.png" />

                    <div class="item-overlay top"></div>
                </div>
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/08/22065516/Image35.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
            <div class="card p-3a col-12 col-md-3 col-lg-3">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144215/Image33.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
        </div>

        <div class="media-container-row">
            <div class="card p-3a col-12 col-md-6 col-lg-6">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144224/Image37.png" />

                    <div class="item-overlay top"></div>
                </div>
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144226/Image38.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
            <div class="card p-3a col-12 col-md-6 col-lg-6">
                <div class="item"><img class="wow animated slideInUp" src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/11144228/Image39.png" />

                    <div class="item-overlay top"></div>
                </div>

            </div>
        </div>

    </div>
    </div>
</section>


<section style="background-color:#fafafa;">
<div class="container"><div class="media-container-row pdlr1">

<div class="col-12 col-md-6 col-lg-6">
<div class="home-title ali1">Try Us 14 Days Risk-Free</div>
<div class="call-act mgt1a ali1">Services at All Time Design are top-notch. You don't have to blindly trust our words. Try us 100% risk-free for two whole weeks.</div>
</div>
<div class="col-12 col-md-6 col-lg-6">
<div class="navbar-buttons design-section-btn mgt1" align="center">
<a class="btn btn-sm btn-primary display-4 btn-hgt" href="<?php echo site_url('pricing/'); ?>">Get Started Now</a>
</div>
</div>

</div></div>
</section>

<?php get_footer(); ?>