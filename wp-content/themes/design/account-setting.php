<?php
/**
 * Template Name: Account Setting page
 * The template for displaying pages
 *
 * This is the template that displays all Backend pages.
 *
 * @package WordPress
 * @subpackage Design Many
 * @since Design Many 1.0
 */
 get_header();
 if ( is_user_logged_in() ) {
	 $current_user = wp_get_current_user();
 ?>
<div class="container-fluid dm_background" style="padding-top:150px;">
    <div class="container dm_background">
        <div class="row dm_acc_title">

            <div class="col-md-12">
                <p class="dm_head" align="left">Account Settings</p>
            </div>
        </div>

        <div class="row dm_name">
            <div class="col-md-12 dm_name_sec2">
                <p class="icons_personal"> <i style="font-size:50px" class="fa" id="fa-icon1">&#xf030;</i><span class="daily_cloud"><?php echo $current_user->user_firstname . ' ' .$current_user->user_lastname; ?> </p>

            </div>
        </div>
    </div>
    <div class="container dm_background1">
        <div class="row dm_acc_set1">
            <div class="col-md-6 dm_acc_set2">

                <div class="row dm_change_password">
                    <p class="dm_change_pass">Change Password</p>
                </div>
                <div class="test_currentset">
					<div class="response_msge">
						
					</div>
					<form method="post" class="change_pass">
						<div class="row">
							<div class="col-md-12 dm_contact_name">
								<label for="ev_name" class="dm_name">Current Password</label>
								<input class="form-control" name="old_pass" placeholder="Type Your Current Password" type="Password" required="true" />

							</div>
						</div>
						<p class="dm_current_pass">

						</p>
						<div class="row dm_new_pass">
							<div class="col-md-12 dNew Password">
								<label for="ev_name" class="dm_name">New Password</label>
								<input class="form-control new_pass" name="new_password" placeholder="New Password" type="Password" required="true" />

							</div>
						</div>
						<div class="row">
							<div class="col-md-12 dm_confirm_password">
								<label for="" class="dm_name">Confirm Password</label>
								<input class="form-control Confirm_pass" name="confirm_password" placeholder="Re-enter Password" type="Password" />

							</div>
						</div>
						<p class="dm_confirm_pass">

						</p>
						<div class="form-group dm-submit" id="dm-submit">
							<div class="col-md-12 dm-submit1">
								<input name="submit" type="submit" class="btn btn-primary change_assword" value="Change Password" />
							</div>
						</div>
					</form>
                </div>
            </div>
            <br>
            <div class="col-md-6 dm_acc_set3">
                <div class="row dm_general_set">
                    <p class="dm_general_ettings">General Settings</p>
                </div>
                <div class="test_setting">

                    <div class="response_msge_up">
						
					</div>
					<form method="post" class="update_details">
						<div class="row">
							<div class="col-md-12 dm_contact_name">
								<label for="ev_name" class="dm_name">First Name</label>
								<input class="form-control" name="fname" placeholder="First Name" type="text" required="true" value="<?php echo $current_user->user_firstname; ?>" />

							</div>
						</div>
						<p class="dm_current_pass">

						</p>
						<div class="row dm_new_pass">
							<div class="col-md-12 dNew Password">
								<label for="ev_name" class="dm_name">Last Name</label>
								<input class="form-control new_pass" name="lname" placeholder="Last Name" type="text" required="true" value="<?php echo $current_user->user_lastname; ?>" />

							</div>
						</div>
						<div class="row">
							<div class="col-md-12 dm_confirm_password">
								<label for="" class="dm_name">Email</label>
								<input class="form-control Confirm_pass" name="email" placeholder="Email" type="email" required value="<?php echo $current_user->user_email; ?>" />

							</div>
						</div>
						<p class="dm_confirm_pass">

						</p>
						<div class="form-group dm-submit" id="dm-submit">
							<div class="col-md-12 dm-submit1">
								<input name="submit" type="submit" class="btn btn-primary change_assword" value="Update" />
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>
    </div>
	<script>
		jQuery(document).ready(function($){
			var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
			$('.change_pass').on('submit', function(e){
				var fd = new FormData();
				var old_pass = $(this).find('input[name=old_pass]').val();
				var new_oass = $(this).find('input[name=new_password]').val();
				var cnf_pass = $(this).find('input[name=confirm_password]').val();
				console.log(old_pass,new_oass,cnf_pass);
				fd.append("old_pass", old_pass);  
				fd.append("new_oass", new_oass);  
				fd.append("cnf_pass", cnf_pass);  
				fd.append('action', 'change_password');
				e.preventDefault();
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: fd,
					contentType: false,
					processData: false,
					beforeSend: function() {
						
					},
					success: function(response){
						if( response == 1 ){
							$('.response_msge').html('<div class="alert alert-warning"> <strong>Warning!</strong> Old Password is wrong.</div>');
						}else if( response == 2 ){
							$('.response_msge').html('<div class="alert alert-danger"> <strong>Danger!</strong> New Password and confirm password is missmatch.</div>');
						}else if( response == 3 ){
							$('.response_msge').html('<div class="alert alert-success"> <strong>Success!</strong> Your password changed successfully.</div>');
						}
						
					},
					error: function(response) { 
						alert("Error occured.please try again");
					},
					complete: function() {
						$('.change_pass').find('input[name=old_pass]').val('');
						$('.change_pass').find('input[name=new_password]').val('');
						$('.change_pass').find('input[name=confirm_password]').val('');
					}
				});
			})
			$('.update_details').on('submit', function(e){
				e.preventDefault();
				var fd = new FormData();
				var fname = $(this).find('input[name=fname]').val();
				var lname = $(this).find('input[name=lname]').val();
				var email = $(this).find('input[name=email]').val();
				fd.append("fname", fname);  
				fd.append("lname", lname);  
				fd.append("email", email);  
				fd.append('action', 'update_details_usr');
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: fd,
					contentType: false,
					processData: false,
					beforeSend: function() {
						
					},
					success: function(response){
						if( response == 1 ){
							$('.response_msge_up').html('<div class="alert alert-warning"> <strong>Warning!</strong> Email Already Exit.</div>');
						}else if( response == 2 ){
							$('.response_msge_up').html('<div class="alert alert-success"> <strong>Success!</strong> Your details updated successfully.</div>');
						}else{
							$('.response_msge_up').html('');
						}						
					},
					error: function(response) { 
						alert("Error occured.please try again");
					},
					complete: function() {
						$('.change_pass').find('input[name=fname]').val(fname);
						$('.change_pass').find('input[name=lname]').val(lname);
						$('.change_pass').find('input[name=email]').val(email);
					}
				});
			})
		})
	</script>
<?php 
 }else{
	 wp_redirect( site_url( 'sign-in' ) );
 }
?>	