<?php
/**
 * Template Name: Back Page
 * The template for displaying pages
 *
 * This is the template that displays all Backend pages.
 *
 * @package WordPress
 * @subpackage Design Many
 * @since Design Many 1.0
 */

 ?>

<?php include_once( 'top.php' ); ?>

<?php
while ( have_posts() ) : the_post();
the_content();			
endwhile;
?>

<?php include_once( 'bottom.php' ); ?>