<?php
/**
 * Template Name: Scope Page
 * The template for displaying scope pages
 *
 * @package WordPress
 * @subpackage All Time Design
 * @since All Time Design 2.0
 */

get_header();  ?>

<section class="features1 design-cont mgt0">
<div class="container">
<div align="center" class="main-info pdb2">Scope of Service</div>
<div align="center" class="scope-txt"><div class="mglr9">Before you decide to opt for our service take a look at the services we provide and those we don't.</div></div>
</div>
</section>

<section class="features1 design-cont mgt1">
<div class="container">
<div align="center" class="scope-title pdb2">Here's what we DO</div>

 <div class="media-container-row mgb2 mgt1">

            <div class="card p-3a col-12 col-md-4 col-lg-4">
               <ul class="scope-list">
			   <li><i class="fa fa-check"></i> Social Media Design</li>
 	<li><i class="fa fa-check"></i> Social Media Ads</li>
 	<li><i class="fa fa-check"></i> Banner Ads</li>
 	<li><i class="fa fa-check"></i> Display Ads</li>
 	<li><i class="fa fa-check"></i> Brochures</li>
 	<li><i class="fa fa-check"></i> Direct Mails</li>
 	<li><i class="fa fa-check"></i> Flyers</li>
 	<li><i class="fa fa-check"></i> Web Banners</li>
 	<li><i class="fa fa-check"></i> Posters</li>
 	<li><i class="fa fa-check"></i> Infographics</li>
 	<li><i class="fa fa-check"></i> Stationery Design</li>
 	<li><i class="fa fa-check"></i> Album Covers</li>
 	<li><i class="fa fa-check"></i> Basic Photo Edits</li>
 	<li><i class="fa fa-check"></i> Billboards</li>
 	<li><i class="fa fa-check"></i> Book Covers</li>
 	<li><i class="fa fa-check"></i> Magazine Covers</li>
 	<li><i class="fa fa-check"></i> Car/Truck Wraps</li>
 	<li><i class="fa fa-check"></i> Cards or Invites</li>
 	<li><i class="fa fa-check"></i> Web Ads</li>
 	<li><i class="fa fa-check"></i> Product Labels</li>
 	<li><i class="fa fa-check"></i> Trade Show Materials</li>
 	<li><i class="fa fa-check"></i> Reports</li>
 	<li><i class="fa fa-check"></i> Proposals</li>
			   </ul>
            </div>
			
			 <div class="card p-3a col-12 col-md-4 col-lg-4">
               <ul class="scope-list">
			   <li><i class="fa fa-check"></i> Presentation Design</li>
 	<li><i class="fa fa-check"></i> Packaging Design</li>
 	<li><i class="fa fa-check"></i> Photoshop Editing</li>
 	<li><i class="fa fa-check"></i> Invoices</li>
 	<li><i class="fa fa-check"></i> Letterheads</li>
 	<li><i class="fa fa-check"></i> Letters</li>
 	<li><i class="fa fa-check"></i> Media Kits</li>
 	<li><i class="fa fa-check"></i> Newsletters</li>
 	<li><i class="fa fa-check"></i> Planners</li>
 	<li><i class="fa fa-check"></i> Business Cards</li>
 	<li><i class="fa fa-check"></i> Announcements</li>
 	<li><i class="fa fa-check"></i> Magazine layouts</li>
 	<li><i class="fa fa-check"></i> Leaflets</li>
 	<li><i class="fa fa-check"></i> Invitations</li>
 	<li><i class="fa fa-check"></i> Tags</li>
 	<li><i class="fa fa-check"></i> Presentation Graphics</li>
 	<li><i class="fa fa-check"></i> Stickers</li>
 	<li><i class="fa fa-check"></i> Email Header Graphics</li>
 	<li><i class="fa fa-check"></i> Book Layouts</li>
 	<li><i class="fa fa-check"></i> Booklets</li>
 	<li><i class="fa fa-check"></i> Programs</li>
 	<li><i class="fa fa-check"></i> Report Cards</li>
 	<li><i class="fa fa-check"></i> Email Newsletters</li>
			   </ul>
            </div>
			
			 <div class="card p-3a col-12 col-md-4 col-lg-4">
               <ul class="scope-list">
			   <li><i class="fa fa-check"></i> Bookmarks</li>
 	<li><i class="fa fa-check"></i> Cards</li>
 	<li><i class="fa fa-check"></i> CD Covers</li>
 	<li><i class="fa fa-check"></i> Certificates</li>
 	<li><i class="fa fa-check"></i> Class Schedules</li>
 	<li><i class="fa fa-check"></i> Coupons</li>
 	<li><i class="fa fa-check"></i> Calendar</li>
 	<li><i class="fa fa-check"></i> Desktop Wallpapers</li>
 	<li><i class="fa fa-check"></i> Gift Certificates</li>
 	<li><i class="fa fa-check"></i> T-Shirts</li>
        <li><i class="fa fa-check"></i> Merchandise</li>
 	<li><i class="fa fa-check"></i> Lesson Plans</li>
 	<li><i class="fa fa-check"></i> ID Cards</li>
 	<li><i class="fa fa-check"></i> Photo Collages</li>
 	<li><i class="fa fa-check"></i> Menus</li>
 	<li><i class="fa fa-check"></i> Vector Tracing</li>
 	<li><i class="fa fa-check"></i> Postcards</li>
 	<li><i class="fa fa-check"></i> Resumes</li>
 	<li><i class="fa fa-check"></i> T-shirt Graphics</li>
 	<li><i class="fa fa-check"></i> Ebook Cover</li>
 	<li><i class="fa fa-check"></i> Rack Cards</li>
 	<li><i class="fa fa-check"></i> Recipe Cards</li>
 	<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;and more...</li>
			   </ul>
            </div>

 </div>

<div align="center" class="scope-txt1 mgb4 mobmg1">To check out some of our latest designs visit <a href="<?php echo site_url('works'); ?>"><strong>Our Works</strong></a></div>
</div>
</section>

<hr />


<section class="mgt4">
<div class="container">
<div align="center" class="scope-title pdb2">Here's what we DON'T DO
</div>

 <div class="media-container-row mgb1 mgt1">

            <div class="card p-3a col-12 col-md-6 col-lg-6">
               <ul class="scope-list">
			   <li><i class="fa fa-times"></i> Website illustrations</li>
 	<li><i class="fa fa-times"></i> Book illustrations</li>
 	<li><i class="fa fa-times"></i> Mascots</li>
 	<li><i class="fa fa-times"></i> Programming</li>
 	<li><i class="fa fa-times"></i> Complex photo manipulations</li>
			   </ul>
            </div>
			
			 <div class="card p-3a col-12 col-md-6 col-lg-3">
               &nbsp;
            </div>
			
			 <div class="card p-3a col-12 col-md-6 col-lg-3">
               &nbsp;
            </div>

 </div>

</div>
</section>


<section style="background-color:#fafafa;">
<div class="container"><div class="media-container-row pdlr1">

<div class="col-12 col-md-6 col-lg-6">
<div class="home-title ali1">Try Us 14 Days Risk-Free</div>
<div class="call-act mgt1a ali1">Services at All Time Design are top-notch. You don't have to blindly trust our words. Try us 100% risk-free for two whole weeks.</div>
</div>
<div class="col-12 col-md-6 col-lg-6">
<div class="navbar-buttons design-section-btn mgt1" align="center">
<a class="btn btn-sm btn-primary display-4 btn-hgt" href="<?php echo site_url('pricing/'); ?>">Get Started Now</a>
</div>
</div>

</div></div>
</section>
<?php get_footer(); ?>