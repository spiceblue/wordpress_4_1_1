<?php
/**
 * Template Name: Create Password
 * The template for displaying pages
 *
 * This is the template that displays login.
 *
 * @package WordPress
 * @subpackage All Time Design
 * @since All Time Design 1.0
 */
global $wpdb;
$err = '';
$success = '';
?>
<?php
if(empty($_REQUEST['usrid'])):
wp_redirect( home_url() ); 
exit;
endif;
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php wp_title(''); ?></title>
  <?php do_action( 'wpseo_head' );  ?>
  <link rel="shortcut icon" type="image/png" href="favicon.png"/>
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/navbar.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/default.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/additional.css" type="text/css">
  <link href="https://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/other-pages.css">
  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script> 
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/other-pages.js"></script>
  
  <style>
  body{height:100vh; background-color:#0051b5;}
  @media screen and (max-width:1440px){body{overflow-y:auto !important; overflow-x:hidden !important;}}
  @media screen and (max-width:767px){body{background-color:#fff !important;}}
  </style>
  
  <!--slide up-->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css" type="text/css" />
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js" type="text/javascript"></script>
   <!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P2NJWB5');</script>
	<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P2NJWB5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
 <div class="container pdt1">
    <div class="row">
        <div class="col-md-3 mar-auto"></div>
        <div class="col-md-6 dm-signin">
            <div class="contact-page mobmg">
                <div class="log-logo desk-show" align="center"><a href="<?php echo site_url(); ?>"> <img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/10183023/Logo.png" alt="logo"></a></div> 
				<div class="log-logo mob-show" align="center"><a href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo1.png"></a></div>
				
                <h2 class="sign-txt desk-show" align="center">Create your password</h2>

                <div class="form-style wt-box">
				<div class="error_ms">
				<?php 
					echo ( $err != '' ) ? strip_tags($err, '<strong>') : '';
				?>
				</div>
					<form method="post" class="sign_form">
						<fieldset>					
							<div class="form-group">
								<div class="col-md-12">
									<input class="form-control" name="new_pass" placeholder="New Password" type="password" value="" required="true" />
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input class="form-control" name="confrm_pass" placeholder="Confirm Password" type="password" value="" required="true" />
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 dm-signin">
									<input type="hidden" name="user_id" id="user_id" value="<?php echo $_REQUEST['usrid']; ?> " />
									<input type="hidden" name="action" value="password_update" />
									<input name="submit" type="submit" class="btn btn-primary dm_submit has-spinner"  value="Create" />
									
								</div>
							</div>
							<p class="response_msge"></p>
						</fieldset>
						
					</form>
                </div>
            </div>
        </div>
        <div class="col-md-3 mar-auto"></div>
    </div>
</div>

  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/smooth-scroll.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/script.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jarallax.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/script.js"></script>
  <script src="https://staging.alltimedesign.com/wp-content/themes/design/assets/js/jquery.buttonLoader.js"></script> 
<script>
var ajax_ur = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
var baseurl = window.location.origin;
jQuery(document).on('submit', '.sign_form', function(e){
	e.preventDefault();
	jQuery.ajax({
		type: 'POST',
		url: ajax_ur,
		data: jQuery( this ).serializeArray(),
		beforeSend: function() {
			var btn = $('.has-spinner');
			$(btn).buttonLoader('start');
		},
		success: function(response){
			var obj = jQuery.parseJSON(response);
			if(obj.success){
				$('.response_msge').html('<span style="color:#0068E5">'+obj.result+'</span>');
				window.location.replace(baseurl+"/welcome");
			}else{
				$('.response_msge').html('<span style="color:#0068E5">'+obj.result+'</span>');
			}
		},
		error: function(response) { 
			alert("Error occured.please try again");
		},
		complete: function() {
			var btn = $('.has-spinner');
			$(btn).buttonLoader('stop');
		}
	});
})
  </script>
</body>
</html>