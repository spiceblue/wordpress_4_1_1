<?php
/**
 * Template Name: Monthly Pricing
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Design Many
 * @since Design Many 1.0
 */

get_header(); 
if ( is_user_logged_in() ) {
	$current_user = wp_get_current_user();
	$customer_status = get_user_meta( $current_user->ID, 'status', true);
	if ( $customer_status == 'active' ){
		echo '<script>window.location.replace("'.site_url('dashboard').'");</script>';
	}else{
	}
}
$month_plan = '';
$year_plan = '';
$payment_mode = get_option( 'payment_mode' );
if( $payment_mode == 'test' ){
	$year_plan = 'DQbqF7DXaWow0Z';
}
if( $payment_mode == 'live' ){
	$year_plan = 'DQfWJ5vwFBGpHM';
} 


?>
<!---popup scr---->
<style>
.dm_prizing1{display:none;}
</style>

<!---popup scr end--->
<div class="container">
	<?php echo $error; ?>
		<div style="text-align: center; padding: 25px;"><a class="navbar-caption text-white display-4" href="<?php echo get_home_url(); ?>">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/Logo.svg">
		</a></div>
		<h1 class="home-title">All Your Designs At A Flat Rate</h1>
		<!--<p class="dm_prizing_para">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested.</p>-->
	</div>
	
	<div class="container dm_prizing_container">
			<div class="dm_shadow dm_prizing1">
			</div>
			<div class="row dm_prizing2">
			<div class="col-sm-6 dm_prizing_background2">
				<h1 class="dm_prizing_header3">DESIGN IN A DAY PLAN</h1>
				<p class="dm_prizing_para3">Customized designs for your <br />daily needs</p>
				<div class="cd-price">		
					<span class="curr">$</span>						
					<span class="dm_prizing_number">999</span>
				</div>
				<p class="dm_prizing_para4">Per Month</p>
				<div class="dm_prizing_button_div mg2">
					<button type="button" data-modal="exampleAdvanced" data-effect="pushup" data-icons="is_right" class="btn dm_prizing_button sm_open">Get Started</button>
				</div>	
			</div>
			
			<div class="col-sm-6 dm_prizing_background1">
				<ul>
					<li class="dm_prizing_img1"><i class="fa fa-check pdr1"></i> Delivery of designs in a day</li>
					<li class="dm_prizing_img2"><i class="fa fa-check pdr1"></i> Irrespective of time zones</li>
					<li class="dm_prizing_img3"><i class="fa fa-check pdr1"></i> Professional designers</li>
					<li class="dm_prizing_img4"><i class="fa fa-check pdr1"></i> Instant response</li>
					<li class="dm_prizing_img4"><i class="fa fa-check pdr1"></i> Non-ambiguous communication</li>
					<li class="dm_prizing_img5"><i class="fa fa-check pdr1"></i> Clarify all doubts</li>
					<li class="dm_prizing_img6"><i class="fa fa-check pdr1"></i> Expert live chat support</li>
					<li class="dm_prizing_img7"><i class="fa fa-check pdr1"></i> Dedicated design expert</li>
					<li class="dm_prizing_img8"><i class="fa fa-check pdr1"></i> Excellent designs</li>
				</ul>
			</div>
			</div>
			<div class="dm_shadow dm_prizing2">
			</div>
			</div>
			
			
			<div class="container faq-accordion pdt2">
			<section class="faq-container no-side-pad-tablet">
				<h1 class="home-title" style="padding-bottom:50px;">Frequently asked questions</h1>
 				<div class="faq-item pdt">
					<h3 class="faq-question">What type of projects do you deal with?</h3>
					<div class="faq-answer"><p style="display: none;">All Time Design takes care of your graphic design needs like posters, brochures, infographics, social media ads and many other services. But services like complete website designing or logo making are out of <span class="our_work"><a href="<?php echo site_url(); ?>/scope/">our scope</a></span>. To know in detail view Scope of Service.</p></div>
				</div>
 				<div class="faq-item">
					<h3 class="faq-question">What makes you different from the others in the industry?</h3>
					<div class="faq-answer"><p>The obvious answer is the quality of our work. Our designs are not just visually attractive but our team makes sure that they understand our client's requirement as well as their target's expectation to make a strong impact.</p></div>
				</div>
 				<div class="faq-item">
					<h3 class="faq-question">What do you mean by unlimited revisions?</h3>
					<div class="faq-answer"><p>You can send in as many changes as you want, until you think that your project has reached your expectation. Our team will make sure that these revisions are incorporated immediately.</p></div>
				</div>				
				<div class="faq-item">
					<h3 class="faq-question">How many requests can I send in a month?</h3>
					<div class="faq-answer"><p>We don't restrict you with any number of design requests. Once we are done with your current request we are ready to move on to your next.</p></div>
				</div>
			</section>
</div>
			


		</div>
		
<!-----popup section----->
<div class="slim_modal" id="exampleAdvanced">
    <div class="sm_content">
        <div class="sm_icon_menu">
            <ul>
                <li class="sm_close pricing_close"><i class="fa fa-times fa-fw "></i></li>
            </ul>
        </div>
        <div class="sm_content_inner_wrap get_start_form">
			<div class="response_msge_gts"></div>
			<div class="response_card_msg"></div>			
			<form method="post" class="p_info" <?php echo ( is_user_logged_in() ) ? "style='display:none;'" : ''; ?>>
            <div class="sm_area_bottom">
                <div class="frm-header">Create account before payment</div>
                <div class="container">
						<div class="row">
							<div class="card p-3a col-12 col-md-6 col-lg-6">
								<div class="form-group mg1">
									<label for="sel1" class="frm-title">First Name</label>
									<input type="text" class="form-control" id="fname" name="fname" placeholder="Enter your first name" required>
								</div>
							</div>
							<div class="card p-3a col-12 col-md-6 col-lg-6">
								<div class="form-group mg1">
									<label for="sel1" class="frm-title">Last Name</label>
									<input type="text" class="form-control" id="lname" name="lname" placeholder="Enter your last name" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="card p-3a col-12 col-md-6 col-lg-6">
								<div class="form-group mg1">
									<label for="sel1" class="frm-title">Email Id</label>
									<input type="email" class="form-control" id="email" name="email" placeholder="yourmail@gmail.com" required>
								</div>
							</div>
							<div class="card p-3a col-12 col-md-6 col-lg-6">
								<div class="form-group mg1">
									<label for="sel1" class="frm-title">Password</label>
									<input type="password" class="form-control" id="password" name="password" placeholder="Enter password" required>
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="card p-3a col-12 col-md-12 col-lg-12">
								<div class="form-group mg1">
									<label for="sel1" class="frm-title our_work1">Already have an account? <a href="<?php echo site_url('sign-in/'); ?>">Sign in</a></label>
								</div>
							</div>
						</div>
						
						<hr class="mgtb1" />
						
						<div class="row">
							<div class="card p-3a col-12 col-md-6 col-lg-6">
								<div class="form-group mg1">
									<label for="sel1" class="frm-title our_work1">By clicking "Create Account" you agree to Alltime Design's <a href="#">Terms of Service</a> and <a href="<?php echo site_url('privacy/'); ?>">Privacy Policy</a>.</label>
								</div>
							</div>
							<div class="card p-3a col-12 col-md-6 col-lg-6">
								<div class="form-group mg1">
									<input type="hidden" name="plan_user" class="plan_user" value="<?php echo $year_plan; ?>" />
									<input type="hidden" name="action" value="get_start_user" />
									<div class="frm-bt_priceing" align="center"><button type="submit" class="next_btn has-spinner">Next</button></div>
								</div>
							</div>
						</div>
						
					
                </div>
            </div>
			</form>	
			<div class="stripe_check_pay <?php echo ( is_user_logged_in() ) ? "user_login" : ''; ?>">
				<div class="frm-header">Your payment method</div>
				<main>
					<section class="container-lg"> 	
					  <div class="cell example example2">
						<form method="post" class="card_submit_form">
						  
						  <div class="row">
							<div class="field half-width bd1">
							<div class="frm-title" style="display:block !important; width:100% !important; text-align:left; margin-top:-30px;">Card Number</div>
							  <div id="example2-card-number" class="input empty"></div>
							  <label for="example2-card-number" data-tid="elements_examples.form.card_number_label">xxxx xxxx xxxx xxxx</label>
							  <!--<div class="baseline"></div>-->
							</div>
							
							<div class="field half-width frm-img">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/Cards.png" />
							</div> 
						  </div>
						  
						  <div class="row mgt10">
							
							<div class="field half-width bd1 expiration">
							<div class="frm-title" style="display:block !important; width:100% !important; text-align:left; margin-top:-30px;">Expiration</div>
							  <div id="example2-card-expiry" class="input empty"></div>
							  <label for="example2-card-expiry" data-tid="elements_examples.form.card_expiry_label">MM / YY</label>
							  <!--<div class="baseline"></div>-->
							</div>
							
							<div class="field half-width bd1 cvv_number">
							<div class="frm-title" style="display:block !important; width:100% !important; text-align:left; margin-top:-30px;">CVV</div>
							  <div id="example2-card-cvc" class="input empty"></div>
							  <label for="example2-card-cvc" data-tid="elements_examples.form.card_cvc_label">_ _ _</label>
							  <!--<div class="baseline"></div>-->
							</div>
						  </div>
						  
						  <div class="row mgt10">
							
							<div class="field half-width" align="left">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/Stripe-Security.png" />
							<div class="frm-title" style="display:block !important; width:100% !important;"></div>
							</div>
							
							<div class="field half-width pricing_details" align="right">
							<div class="frm-title" style="display:block !important; width:100% !important;"><p class="price_per_mn">$999</p></div>
							<div class="frm-title" style="display:block !important; width:100% !important;">Price &nbsp;&nbsp;<span class="amount">$999</span></div>
							<!--<div class="price-txt">FREE TO TRY FOR 14 DAYS</div>-->
							</div>
							
						  </div>
						  
						  <div class="row mgt10">							
							<div class="field half-width" align="left">
								<div class="frm-title our_work1">By availing our services you agree to Alltime Design's <a href="#">Terms of Service</a> and <a href="<?php echo site_url('privacy/'); ?>">Privacy Policy</a>.</div>
							</div>
							
							<div class="field half-width" align="center">
								<input type="hidden" name="plan_user" class="plan_user" value="<?php echo $year_plan; ?>" />
								<input type="hidden" name="user_id" id="user_id" value="<?php echo $current_user->ID; ?>" />
								<button type="submit" data-tid="elements_examples.form.pay_button">Pay</button>
							</div>
							
						  </div>
						  
						  <div class="error" role="alert" style="display:none;"><span class="message"></span></div>
						</form>
						<div class="success">
						  <div class="icon">
								<img src="https://s3-ap-south-1.amazonaws.com/spiceblue-design/wp-content/uploads/2018/07/20111910/load.gif" alt="loader_img" style="width: 150px;"/>
						  </div>
						  <h3 class="title" data-tid="elements_examples.success.title">Processing Payment....</h3>
						  <p class="message"><span data-tid="elements_examples.success.message"></span><span class="token"></span></p>
						</div>
					  </div>
					 </section>
				</main>	
			</div>
			<div class="final_response">			
			<div class="sm_content_inner_wrap">
				<form action="" method="post" class="make_request" enctype="multipart/form-data">
					<div class="sm_area_bottom">
						<div class="frm-header">Making a request is easy</div>

						<div class="media-container-row">
							<div class="card p-3a col-12 col-md-6 col-lg-6">
								<div class="form-group mg1">
									<label for="sel1" class="frm-title">Request Type</label><br />
									<select class="js-select2 form-control" name="subject" required>
					  <option value="Social Media Design">Social Media Design</option>
					  <option value="Social Media Ads">Social Media Ads</option>
					  <option value="Banner Ads">Banner Ads</option>
					  <option value="Display Ads">Display Ads</option>
					  <option value="Brochures">Brochures</option>
					  <option value="Direct Mails">Direct Mails</option>
					  <option value="Flyers">Flyers</option>
					  <option value="Web Banners">Web Banners</option>
					  <option value="Posters">Posters</option>
					  <option value="Infographics">Infographics</option>
					  <option value="Stationery Design">Stationery Design</option>
					  <option value="Album Covers">Album Covers</option>
					  <option value="Basic Photo Edits">Basic Photo Edits</option>
					  <option value="Billboards">Billboards</option>
					  <option value="Book Covers">Book Covers</option>
					  <option value="Magazine Covers">Magazine Covers</option>
					  <option value="Car/Truck Wraps">Car/Truck Wraps</option>
					  <option value="Cards or Invites">Cards or Invites</option>
					  <option value="Web Ads">Web Ads</option>
					  <option value="Product Labels">Product Labels</option>
					  <option value="Trade Show Materials">Trade Show Materials</option>
					  <option value="Reports">Reports</option>
					  <option value="Proposals">Proposals</option>
					  <option value="Presentation Design">Presentation Design</option>
					  <option value="Packaging Design">Packaging Design</option>
					  <option value="Photoshop Editing">Photoshop Editing</option>
					  <option value="Invoices">Invoices</option>
					  <option value="Letterheads">Letterheads</option>
					  <option value="Letters">Letters</option>
					  <option value="Media Kits">Media Kits</option>
					  <option value="Newsletters">Newsletters</option>
					  <option value="Planners">Planners</option>
					  <option value="Business Cards">Business Cards</option>
					  <option value="Announcements">Announcements</option>
					  <option value="Magazine layouts">Magazine layouts</option>
					  <option value="Leaflets">Leaflets</option>
					  <option value="Invitations">Invitations</option>
					  <option value="Tags">Tags</option>
					  <option value="Presentation Graphics">Presentation Graphics</option>
					  <option value="Stickers">Stickers</option>
					  <option value="Email Header Graphics">Email Header Graphics</option>
					  <option value="Book Layouts">Book Layouts</option>
					  <option value="Booklets">Booklets</option>
					  <option value="Programs">Programs</option>
					  <option value="Report Cards">Report Cards</option>
					  <option value="Email Newsletters">Email Newsletters</option>
					  <option value="Bookmarks">Bookmarks</option>
					  <option value="Cards">Cards</option>
					  <option value="CD Covers">CD Covers</option>
					  <option value="Certificates">Certificates</option>
					  <option value="Class Schedules">Class Schedules</option>
					  <option value="Coupons">Coupons</option>
					  <option value="Calendar">Calendar</option>
					  <option value="Desktop Wallpapers">Desktop Wallpapers</option>
					  <option value="Gift Certificates">Gift Certificates</option>
					  <option value="T-Shirts">T-Shirts</option>
					  <option value="Merchandise">Merchandise</option>
					  <option value="Lesson Plans">Lesson Plans</option>
					  <option value="ID Cards">ID Cards</option>
					  <option value="Photo Collages">Photo Collages</option>
					  <option value="Menus">Menus</option>
					  <option value="Vector Tracing">Vector Tracing</option>
					  <option value="Postcards">Postcards</option>
					  <option value="Resumes">Resumes</option>
					  <option value="T-shirt Graphics">T-shirt Graphics</option>
					  <option value="Ebook Cover">Ebook Cover</option>
					  <option value="Rack Cards">Rack Cards</option>
					  <option value="Recipe Cards">Recipe Cards</option>
					  <option value="Others">Others</option>
					</select>
									
									
								</div>
							</div>

							<div class="card p-3a col-12 col-md-6 col-lg-6">
								<div class="form-group mg1">
									<label for="sel1" class="frm-title">Inspirations (Optional)</label>
									<input type="text" class="form-control" placeholder="Type url" name="inspiration" />
								</div>
							</div>

						</div>

						<div class="form-group mg1">
							<label for="comment" class="frm-title">Project Details</label>
							<textarea class="form-control" rows="5" id="project" name="message" placeholder="Short brief about your project"></textarea>
						</div>

						<div class="media-container-row">
							<div class="card p-3a col-12 col-md-6 col-lg-6">
								<div class="form-group mg1">
									<label for="att" class="frm-title">Attach Reference Files</label>
									<br />
										<input type="file" class="filebtn" name="image" accept="image/*,.pdf">
								</div>
							</div>

							<div class="card p-3a col-12 col-md-6 col-lg-6">
								<div class="form-group mg1">
									<label for="att" class="frm-title">&nbsp;</label>
									<br />
									<input type="hidden" name="email" id="user_email_req" value="<?php echo $current_user->user_email; ?>" />
									<input type="submit" class="frm-btn" name="create" value="Make Request">
								</div>
							</div>

						</div>

					</div>

				</form>
			</div>		
			</div>
			<div class="pay_form">
				<form method="post" class="pay_form_frm">
					<input type="hidden" name="user_id" />
					<input type="hidden" name="user_plan" />
					<input type="hidden" name="coupoun_code" />
					<input type="hidden" name="stripe_token" />
				</form>
			</div>
            <div align="right" class="frm-bottom"><a class="sm_close">Close <i class="fa fa-times"></i></a></div>
        </div>
    </div>
</div>
<script>
jQuery(document).ready(function($){
	var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
	$('.p_info').on('submit', function(e){
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: ajaxurl,
			data: $(this).serializeArray(),
			dataType: "json",
			beforeSend: function() {
				$('.response_msge_gts').html('');
				var btn = $('.has-spinner');
				$(btn).buttonLoader('start');
			},
			success: function(response){
				if(response.success){
					console.log(response.result);
					$('.p_info').hide();
					$('.stripe_check_pay').show();
					$('.stripe_check_pay form #user_id').val(response.result);
					$('.sm_close').addClass('pricing_close');
				}else{
					$('.response_msge_gts').html('<div class="alert alert-danger"> <strong>Danger!</strong> Email address already exit.</div>');
					console.log(response.result);
				}				
			},
			error: function(response) { 
				alert("Error occured.please try again");
			},
			complete: function() {
				var btn = $('.has-spinner');
				$(btn).buttonLoader('stop');
			}
		});
	})
	$(document).on('click', '.sm_close.pricing_close', function(){
		location.reload();
	})
})
</script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.buttonLoader.js"></script> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/faq.js"></script> 
<!-----popup end-------->
<?php 
get_footer(); ?>		