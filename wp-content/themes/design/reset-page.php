<?php
/**
 * Template Name: Reset Page
 * The template for displaying pages
 *
 * This is the template that displays reset.
 *
 * @package WordPress
 * @subpackage All Time Design
 * @since All Time Design 1.0
 */
global $wpdb;

$err = '';
$success = '';

if(isset($_POST['submit']) && $_POST['submit'] == 'Log In' )
{
	//We shall SQL escape all inputs to avoid sql injection.
	$username = $wpdb->escape($_POST['email']);
	$password = $wpdb->escape($_POST['password']);
	if( $username == "" || $password == "" ) {
		$err = 'Please don\'t leave the required field.';
	} else {
		$user_data = array();
		$user_data['user_login'] = $username;
		$user_data['user_password'] = $password;
		$user_data['remember'] = $remember;
		$user = wp_signon( $user_data, false );

		if ( is_wp_error($user) ) {
			$err = $user->get_error_message();
		} else {
			wp_set_current_user( $user->ID, $username );
			do_action('set_current_user');
			wp_redirect( site_url( 'dashboard' ) );
			exit();
		}
	}
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php wp_title(''); ?></title>
  <?php do_action( 'wpseo_head' );  ?>
  <link rel="shortcut icon" type="image/png" href="favicon.png"/>
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/navbar.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/default.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/additional.css" type="text/css">
  <link href="https://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/other-pages.css">
  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script> 
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/other-pages.js"></script>
  
  <style>
  body{height:100vh; background-color:#0051b5;}
  @media screen and (max-width:1440px){body{overflow-y:auto !important; overflow-x:hidden !important;}}
  @media screen and (max-width:767px){body{background-color:#fff !important;}}
  </style>
  
  <!--slide up-->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css" type="text/css" />
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js" type="text/javascript"></script>
</head>
<body>


 <div class="container pdt1">
    <div class="row">
        <div class="col-md-3 mar-auto"></div>
        <div class="col-md-6 dm-signin">
            <div class="contact-page mobmg">
                <div class="log-logo desk-show" align="center"><a href="<?php echo site_url(); ?>"> <img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/07/10183023/Logo.png" alt="logo"></a></div> 
				<div class="log-logo mob-show" align="center"><a href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo1.png"></a></div>
				
                <h2 class="sign-txt desk-show" align="center">Reset Password</h2>
				<h2 class="sign-txt mob-show" align="center" style="color:#000000;">Reset Password</h2>

                <div class="form-style wt-box">
				<div class="error_ms">
				<?php 
					echo ( $err != '' ) ? strip_tags($err, '<strong>') : '';
				?>
				</div>
					<form method="post" class="sign_form">
						<fieldset>					
							<div class="form-group">
								<div class="col-md-12">
									<input class="form-control" name="password1" placeholder="Type New Password" type="password" value="" required="true" />
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input class="form-control" name="password2" placeholder="Re-enter Password" type="password" value="" required="true" />
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 dm-signin">
									<input name="submit" type="submit" class="btn btn-primary dm_submit" value="Reset Password" />
									
								</div>
							</div>
						</fieldset>
					</form>
                </div>
            </div>
        </div>
        <div class="col-md-3 mar-auto"></div>
    </div>
</div>


  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/smooth-scroll.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/script.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jarallax.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/script.js"></script>
  
</body>
<script>
jQuery(document).ready(function($){
	var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
	$('.forgot_pass').on('submit', function(e){
		e.preventDefault();
		var user_login = $('.user_login').val();
		if(user_login){
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: ajaxurl,
				data: { 
					'action': 'ajaxforgotpassword', 
					'user_login': user_login, 
				},
				success: function(data){	
					if( data.loggedin ){	
						$('.error_msge').html('');
						$('.success_msge').html( data.message );
						
					}else{
						$('.success_msge').html('');
						$('.error_msge').html( data.message );
					}
				}
			});
		}else{
			
		}
	})	
})
</script>
</html>