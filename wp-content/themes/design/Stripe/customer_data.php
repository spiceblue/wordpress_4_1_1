<?php
if($customer_id){
	$customer_data = \Stripe\Customer::retrieve($customer_id);
	$cus_data = $customer_data->sources->data;
	$cus_crd_data['last'] = $cus_data[0]->last4;
	$cus_crd_data['exp_month'] = $cus_data[0]->exp_month;
	$cus_crd_data['exp_year'] = $cus_data[0]->exp_year;
}
if( $card_num != '' && $expmonth != '' && $cvv != '' ){
	$explode_mnth_yr = explode('/',$expmonth);
	$explode_mnth = ltrim($explode_mnth_yr[0], '0');

	$card_token = \Stripe\Token::create(array(
	  "card" => array(
		"number" => $card_num,
		"exp_month" => $explode_mnth,
		"exp_year" => $explode_mnth_yr[1],
		"cvc" => $cvv
	  )
	));

	$current_user = wp_get_current_user();
	$customer_id = get_user_meta( $current_user->ID, 'customer_id', true );
	$cu = \Stripe\Customer::retrieve($customer_id);
	$cu->description = "Updated Card Details";
	$cu->source = $card_token->id; 
	$cu->save();
	echo 'success';
}

if( $customer_id != '' && $subcrb_id != '' ){
	$subscription = \Stripe\Subscription::retrieve($subcrb_id);
	$upgrade_plan = \Stripe\Subscription::update($subcrb_id, [
	  'cancel_at_period_end' => false,
	  'items' => [
			[
				'id' => $subscription->items->data[0]->id,
				'plan' => 'plan_DGYAWcKzoRJuZa',
			],
		],
	]);
	$sub_arr = $upgrade_plan->__toArray(true);
	$user_arr['created'] = date('Y-m-d', $sub_arr['created']);
	$user_arr['subscribtion_start'] = date('Y-m-d', $sub_arr['current_period_start']);
	$user_arr['subscribtion_end'] = date('Y-m-d', $sub_arr['current_period_end']);
	$user_arr['plan'] = $sub_arr['plan']['nickname'];
	$user_arr['amount'] = $sub_arr['plan']['amount'];
	$user_arr['currency'] = $sub_arr['plan']['currency'];
	$user_arr['status'] = $sub_arr['status'];
	$user_id=$current_user->ID;
	update_user_meta( $user_id, 'created', $user_arr['created']);
	update_user_meta( $user_id, 'subscribtion_start', $user_arr['subscribtion_start']);
	update_user_meta( $user_id, 'subscribtion_end', $user_arr['subscribtion_end']);
	update_user_meta( $user_id, 'plan', $user_arr['plan']);
	update_user_meta( $user_id, 'amount', $user_arr['amount']);
	update_user_meta( $user_id, 'currency', $user_arr['currency']);
	update_user_meta( $user_id, 'status', $user_arr['status']);
	update_user_meta( $user_id, 'subscribtion_data', $sub_arr);
}
?>