<?php
/**
 * Template Name: Pricing Page
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Design Many
 * @since Design Many 1.0
 */

//mail
$get_star_email = ( isset($_REQUEST['search']) ) ? $_REQUEST['search'] : '';
if ($get_star_email!='')
{
$to = "prem@spiceblue.com";
$subject = "[All Time Design]";
$txt = $get_star_email. "<br><br>This id tried to connect with All Time Design.";
$headers = "From:". $get_star_email;

wp_mail($to,$subject,$txt,$headers);

}
?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php wp_title(''); ?></title>
  <?php do_action( 'wpseo_head' );  ?>
  <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png"/>
  <link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/SF-Pro-Display-Regular.otf" as="font" type="font/otf" crossorigin>
	  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/navbar.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/default.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/additional.css" type="text/css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/popup.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/other-pages.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/base.css" data-rel-css="" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/example2.css" data-rel-css="" />
  <link href='<?php echo get_template_directory_uri(); ?>/assets/css/nprogress.css' rel='stylesheet' />
  <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script> 
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/other-pages.js"></script>
  <link rel='stylesheet prefetch' href='<?php echo get_template_directory_uri(); ?>/assets/css/select2.min.css'>
  <!--slide up-->
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/balloon-css/0.5.0/balloon.min.css">
    
  <!---lightbox--->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/hes-gallery.css">

  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css" type="text/css" />
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js" type="text/javascript"></script>
  <script src="https://js.stripe.com/v3/"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/index.js" data-rel-js></script>
  <!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P2NJWB5');</script>
	<!-- End Google Tag Manager -->
<script type="text/javascript">                                                                                                                                                                                                                                                                                                                                                                                                                                               </script></head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P2NJWB5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<style>
#search-box input[type="email"]
    {
    width: 100%;
    padding: 14px 0 14px 1em;
    color: #333;
    outline: none;
	border: 1px solid #979797 !important;	border-radius: 3px 3px 4px 3px !important;	background-color: #FFFFFF;
    }
</style>
<style>
input[type=text]:focus, input[type=password]:focus, textarea:focus {
  -moz-box-shadow: none;
  -webkit-box-shadow:none;
  box-shadow:none;
  border: 1px solid #0d83dd;
}
.dm_prizing1{display:none;}

/*new code*/
.pri-title{	color: #2C2F33;	font-family: "SF Pro Display";	font-size: 20px; font-weight: 800 !important; line-height: 24px; text-align: center; margin-bottom:30px;}
.chk-txt{color: #778095 !important; font-family: "SF Pro Display";	font-size: 16px !important; line-height: 19px;	text-align: center;}
.ded{color: #939DB8; font-family: "SF Pro Display"; font-size: 14px; line-height: 16px; padding-left:10px;}
.ded-txt{color: #939DB8; font-family: "SF Pro Display";	font-size: 17px; line-height: 25px;	text-align: center; padding:50px; padding-top:5px; letter-spacing:.5px;}
.ded-txt span{color: #2C2F33;}
.curr{left: 26% !important; font-weight: 600 !important; color:#939DB8 !important; font-size:18px !important;}
.dm_prizing_para4{color:#939DB8 !important;}
.pricing-sub{border: 1.5px solid #cacfd5; !important;}
::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  color: #D9DFE8; font-size: 16px;
}
::-moz-placeholder { /* Firefox 19+ */
  color: #D9DFE8; font-size: 16px;
}
:-ms-input-placeholder { /* IE 10+ */
  color: #D9DFE8; font-size: 16px;
}
:-moz-placeholder { /* Firefox 18- */
  color: #D9DFE8; font-size: 16px;
}
.table-title{color: #2C2F33; font-family: "SF Pro Display"; border: 1.5px solid #EDF2FA; border-right:0px; border-bottom:0px; font-size: 18px; font-weight: 600; background-color:#fafbfc; padding:12px; padding-left:30px;}
.table-cont{color: #2C2F33; font-family: "SF Pro Display"; font-size: 18px; font-weight: 400; padding:6px; padding-left:30px;}
.table-cont1{color: #2C2F33; font-family: "SF Pro Display"; font-size: 18px; font-weight: 400; padding:6px; padding-left:30px;}
.table-cont2{padding:12px; padding-left:30px; text-align:center;}
.table-cont3{padding:12px; padding-left:30px; text-align:center;}
.curve-arrow{float:right; margin-top:-18px; margin-right:55px;}

/* Acordeon styles */
.tab {
  position: relative;
  margin-bottom: 1px;
  width: 100%;
  color: #fff;
  overflow: hidden;
}
.tab input {
  position: absolute;
  opacity: 0;
  z-index: -1;
}
.tab label {
  position: relative;
  display: block;
  padding: 0 0 0 1em;
  background: #fff;
  border: 1.5px solid #EDF2FA;
  border-top:0px !important;
  color:#0068e5;
  font-weight: bold;
  line-height: 3;
  cursor: pointer;
}
.blue label {
  background: #2980b9;
}
.tab-content {
  max-height: 0;
  overflow: hidden;
  background: #fff;
  border: 1.5px solid #EDF2FA;
  border-top:0px !important;
  transition: max-height .35s;
  color:#939DB8;
}
.blue .tab-content {
  background: #3498db;
}
.tab-content p {
  margin: 1em;
}
/* :checked */
.tab input:checked ~ .tab-content {
  max-height: 100vh;
}
/* Icon */
.tab label::after {
  position: absolute;
  right: 0;
  top: 0;
  display: block;
  width: 3em;
  height: 3em;
  line-height: 3;
  text-align: center;
  transition: all .35s;
}
.tab input[type=checkbox] + label::after {
  content: "\f078";
  font-family: FontAwesome;
  font-weight:500 !important;
}
.tab input[type=radio] + label::after {
  content: "\25BC";
}
.tab input[type=checkbox]:checked + label::after {
  -webkit-transform: rotate(180deg);
          transform: rotate(180deg);
}
.tab input[type=radio]:checked + label::after {
  -webkit-transform: rotateX(180deg);
          transform: rotateX(180deg);
}
label{margin-bottom: 0rem;}
.pri-but{margin-top:10px; width: 282px;}
.pri-des{color: #000; font-family: "SF Pro Display";	font-size: 14px; line-height: 22px;	text-align: center; padding:50px; padding-top:20px; padding-bottom:0px;}
.pricing-sub{margin-top:0px;}
.switch{top: 10px;}
.dm_prizing_number{font-size: 80px;}
.des-list{color: #939DB8 !important; font-family: "SF Pro Display"; font-size: 16px !important; line-height: 16px; padding:10px !important;}

select {
  border: 0 !important;
  font: 16px SF Pro Display !important;
}

[data-balloon][data-balloon-pos='right']:hover:before, [data-balloon][data-balloon-pos='right'][data-balloon-visible]:before{display:none;}
.ttip{font-weight:500 !important; color:#e1e9f0 !important; float:right;}
.pdt60{padding-top:60px;}
.dm_prizing_switch {
    margin-right: 5px;
    margin-left: 5px;
}
.offer{color: #31B860;
    font-family: "SF Pro Display";
    font-size: 14px;
    font-weight: 700;
    border: 1.5px solid #31B860;
    border-radius: 3px;
    padding: 4px 8px;
    background: #f3fef8;
    letter-spacing: -0.04px;
    line-height: 19px;}
.pri-mg1{margin-top: 20px; margin-bottom:30px; margin-left: 100px;}
	
.dm_prizing_number{font-weight: 700 !important;}
#ftrs ul li{color: #2C2F33; font-family: "SF Pro Display";	font-size: 18px; font-weight: 500;	letter-spacing: 0.3px;	line-height: 36px;	text-align: center;}
div#mc_message{color:#FFFFFF;}
.newsletter{width: 50% !important; margin-left: 20%; margin-top:40px;}
.mb101{margin-top:60px; margin-bottom:150px;}
.brdr1{border-bottom: 1.5px solid #EDF2FA; margin:30px;}
.mg101{margin-top:60px;}
.p-box2{border: 1.5px solid #EDF2FA; padding-top:60px; border-left:0px; background-color:#FAFBFC;}
@media screen and (max-width:1024px){
.curr{left: 20% !important;}
.pri-des{padding: 40px;}
select {
  font: 14px SF Pro Display !important;
}
.table-cont1, .table-cont{font-size: 15px;}
.table-cont2, .table-cont3{padding: 10px;}
}

@media screen and (max-width:768px){
.curr{left: 16% !important;}
.chk-txt{font-size: 12px !important;}
.ded-txt{padding: 0px; padding-top:80px;}
.ded{font-size: 12px;}
.dm_prizing_number{font-size: 65px;}
.pri-des{padding: 0px;}
.mmmgt1{margin-top:40px !important;}
.pri-title{margin-bottom: 40px;}
.table-cont1, .table-cont{font-size: 11px;}
.table-cont2, .table-cont3{padding: 7px;}
select{font: 10px SF Pro Display !important;}
.ded-txt{padding-top: 20px; padding-bottom: 30px;}
.pricing-sub, .pri-but{width: 206px !important;}
#ftrs ul li{font-size:13px; line-height: 32px;}
.newsletter{width: 70% !important;}
}

@media screen and (max-width:767px){
.curr{left: 21% !important;}
.table-cont1, .table-cont{font-size: 16px;}
.table-cont, .table-cont1{padding-left: 18px;}
.pdt60{padding-top:0px;}
.chk-txt{font-size: 16px !important;}
select{font: 18px SF Pro Display !important;}
.dm_prizing_number{font-size: 80px;}	
.pri-des{font-size: 16px; margin: 20px; margin-left: 0px; margin-right: 0px;}
.pricing-sub, .pri-but{width: 282px !important;}
.newsletter{width: 100% !important; margin-left: 0px !important; margin-top:40px;}
.mb101{margin-top:60px; margin-bottom:0px;}
#ftrs ul li{font-size:16px;}
.pricing-box{margin-left:35px; margin-right:35px;}
.brdr1{border-bottom: 0px; margin:0px;}
.pricing-mg1{margin-top:40px !important;}
.pricing-mg2{margin-bottom:100px !important;}
.mg101{margin-top:0px;}
.p-box2{border-left:1.5px solid #EDF2FA;}
.pri-mg1{margin-left: 0px;}
.offer{display:block; margin-left: 175px; width:110px; border: 1px solid #31B860;}
}

@media screen and (max-width:380px){
.home-title{font-size: 24px !important;}
.curr{left: 17% !important;}
.pricing-sub, .pri-but{width: 243px !important;}
}

@media screen and (max-width:360px){
.curr{left: 14% !important;}
.pricing-sub, .pri-but{width: 227px !important;}
}

@media screen and (max-width:320px){
select{font: 15px SF Pro Display !important;}
.pri-des{margin-left: 0px; margin-right: 0px; }	
.home-title{font-size: 19px !important;}
.pricing-box{margin-left: 15px !important; margin-right: 15px !important;}
}
</style>
<script>
function sync()
{
  var n1 = document.getElementById('n1');
  val1 = document.getElementById('n1').value;
  if (val1=='')
  {
  document.getElementById('n1').value=1;
  }
  else if(val1==0)
  {
  document.getElementById('n1').value=1;
  }
  
  if(val1>=10)
  {
  document.getElementById('n1').value=10;
  }
  var n2 = document.getElementById("n2").innerHTML = n1.value*349;
  var n3 = document.getElementById("n3").innerHTML = n1.value*299;
  var n4 = document.getElementById("n4").innerHTML = n1.value*999;
  var n5 = document.getElementById("n5").innerHTML = n1.value*899;
}
</script>


<div align="center" class="mgt1 mmmgt1"><a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/Logo.svg" /></a></div>


<section class="features1 design-cont">
<div class="container">
<div align="center" class="main-info pdb2">All Your Designs At a Flat Rate</div>

<div align="center" class="pri-mg1">
<span class="dm_prizing_discount chk-txt">Monthly Plan</span>
<label class="switch dm_prizing_switch">
<input type="checkbox" name="dm_prizing_radio" class="dm_checked" checked>
<span class="slider round"></span>
</label>
<span class="dm_prizing_discount chk-txt">Yearly Plan&nbsp;&nbsp;&nbsp;<span class="offer">2 Months Free</span></span>
</div>

</div>

<select id="n1" class="form-control" onClick="sync()" onKeyUp="sync()" onKeyPress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" style="width:64.5%; background-color: #fffff !important; border: 1px solid #d4d7dd !important; border-radius: 5px; padding-left:12px; width:10px; display:none;">
  <option value="1">01&nbsp;&nbsp;&nbsp;Dedicated Designer</option>
          <option value="2">02&nbsp;&nbsp;&nbsp;Dedicated Designers</option>
		  <option value="3">03&nbsp;&nbsp;&nbsp;Dedicated Designers</option>
		  <option value="4">04&nbsp;&nbsp;&nbsp;Dedicated Designers</option>
		  <option value="5">05&nbsp;&nbsp;&nbsp;Dedicated Designers</option>
		  <option value="6">06&nbsp;&nbsp;&nbsp;Dedicated Designers</option>
		  <option value="7">07&nbsp;&nbsp;&nbsp;Dedicated Designers</option>
		  <option value="8">08&nbsp;&nbsp;&nbsp;Dedicated Designers</option>
		  <option value="9">09&nbsp;&nbsp;&nbsp;Dedicated Designers</option>
		  <option value="10">10&nbsp;&nbsp;&nbsp;Dedicated Designers</option>
</select>
</section>


<div class="container mg101">
<div class="row pricing-box">

<div class="col-sm-2">&nbsp;</div>

<div class="col-sm-4" style="border: 1.5px solid #EDF2FA; padding-top:60px;">
<div class="pri-title">STANDARD</div>
<div class="dm_prizing1">
<div class="cd-price">		
<span class="curr">$</span>		
<span class="dm_prizing_number" id="n2">349</span><span  class="dm_prizing_para4">/&nbsp;mo</span>				
</div>
<div class="pri-des">Saving $59 a month and $708 a year when billed annually</div>
</div>
<div class="dm_prizing2">
<div class="cd-price">		
<span class="curr">$</span>						
<span class="dm_prizing_number" id="n3">290</span><span  class="dm_prizing_para4">/&nbsp;mo</span>
</div>
<div class="pri-des">Saving $59 a month and $708 a year when billed annually</div>
</div>
<div class="navbar-buttons design-section-btn mgt3" align="center">
<input type="email" name="standard" id="email1" class="pricing-sub stripe_email" placeholder="Enter Your Email Address" value="<?php //echo $get_star_email; ?>" />
<br />
<a id="customButton" class="btn btn-sm btn-primary display-4 pri-but" href="#">Buy Now</a>
</div>

<div align="center" class="brdr1">&nbsp;</div>
<div align="center" id="ftrs" class="desk-show">
<ul style="margin-top:50px;">
<li>14 Days Risk-Free Trial</li>
<li>Unlimited Designs</li>
<li>Unlimited Revisions</li>
<li>Professional Designer</li>
<li>Customer Support</li>
<li>Dedicated Account Manager</li>
<li>Royalty Free Stock Images</li>
<li>Ownership for Your Files</li>
</ul>
</div>

</div>
<div class="tab mob-show">
<input id="tab-one" type="checkbox" name="tabs">
<label for="tab-one">Features</label>
<div class="tab-content">
<ul style="padding-top: 10px; padding-bottom: 10px;">
<li class="table-cont1">14 day Risk-Free Trial</li>
<li class="table-cont">Unlimited Designs</li>
<li class="table-cont1">Unlimited Revisions</li>
<li class="table-cont1">Professional Designer</li>
<li class="table-cont1">Customer Support</li>
<li class="table-cont1">Dedicated Account Manager</li>
<li class="table-cont1">Royalty Free Stock Images</li>
<li class="table-cont1">Ownership for Your Files</li>
</ul>
</div>
</div>

<div class="col-sm-4 pricing-mg1 p-box2">
<div class="pri-title">DESIGN IN A DAY</div>
<div class="dm_prizing1">
<div class="cd-price">		
<span class="curr">$</span>		
<span class="dm_prizing_number" id="n4">999</span><span  class="dm_prizing_para4">/&nbsp;mo</span>				
</div>
<div class="pri-des">Saving $167 a month and $2004 a year when billed annually</div>
</div>
<div class="dm_prizing2">
<div class="cd-price">		
<span class="curr">$</span>						
<span class="dm_prizing_number" id="n5">832</span><span  class="dm_prizing_para4">/&nbsp;mo</span>
</div>
<div class="pri-des">Saving $167 a month and $2004 a year when billed annually</div>
</div>
<div class="navbar-buttons design-section-btn mgt3" align="center">
<input type="email" name="design" id="email2" class="pricing-sub stripe_email1" placeholder="Enter Your Email Address" value="<?php //echo $get_star_email; ?>" />
<br />
<a id="customButton1" class="btn btn-sm btn-primary display-4 pri-but" href="#">Buy Now</a>
</div>

<div align="center" class="brdr1">&nbsp;</div>
<div align="center" id="ftrs" class="desk-show">
<ul style="margin-top:50px;">
<li style="font-weight:700;">All of Our Standard Features</li>
<li style="color:#0068e5; font-weight:700;">Plus</li>
<li>Same-Day Delivery</li>
<li>Dedicated Designer</li>
<li>Faster Turnaround Time</li>
<li>Premium Shutter Stock Photos</li>
<li>Real-Time Collaboration via Slack</li>
<li>Live Chat Support</li>
</ul>
</div>

</div>
<div class="tab mob-show">
<input id="tab-two" type="checkbox" name="tabs">
<label for="tab-two" style="background: #FAFBFC;">Features</label>
<div class="tab-content pricing-mg2">
<ul style="background: #FAFBFC; padding-top: 10px; padding-bottom: 10px;">
<li class="table-cont" style="font-weight:700;">All of Our Standard Features</li>
<li class="table-cont1" style="color:#0068e5; font-weight:700;">Plus</li>
<li class="table-cont1">Same-Day Delivery</li>
<li class="table-cont1">Dedicated Designer</li>
<li class="table-cont1">Faster Turnaround Time</li>
<li class="table-cont1">Premium Shutter Stock Photos</li>
<li class="table-cont1">Real-Time Collaboration via Slack</li>
<li class="table-cont1">Live Chat Support</li>
</ul>
</div>
</div>

<div class="col-sm-2 desk-show">&nbsp;</div>

</div>
</div>



<section class="features1 design-cont mgt1" style="background-color:#fafafa; margin-top:100px;">
<div align="center" class="home-title" style="padding-top:50px; padding-bottom:20px;">All Time Design Top Features</div>
    <div class="container">
        <div class="media-container-row">

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-hearth" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/12/28083008/Free_trial.png"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">
                        14 days risk free trial</h4>
                    <p class="many-text many-fonts-style display-7">
                        Risk free trial for 14 days with money refund policy to check the quality of our work.
                    </p>
                </div>
            </div>

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-rocket" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/12/28083023/Subscription_based.png"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">Subscription Based</h4>
                    <p class="many-text many-fonts-style display-7">
                        No more hourly rates. Pay a flat monthly fee and get unlimited designs and revisions.
                    </p>
                </div>
            </div>

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-globe" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/12/28083019/File_ownership.png"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">Own Your Files</h4>
                    <p class="many-text many-fonts-style display-7">You have a complete holding of all source files that we create for your project.</p>
                </div>
            </div>


        </div>
    </div>

<div class="container">
        <div class="media-container-row pdb1 mgb2">

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-hearth" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/12/28083020/Infinite_brands.png"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">
                        Use on Infinite Brands</h4>
                    <p class="many-text many-fonts-style display-7">
                        Absolute entitlement for the designs you pay and so you can use them on any brands.
                    </p>
                </div>
            </div>

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-rocket" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/12/28083021/Live_chat.png"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">Live Chat Support</h4>
                    <p class="many-text many-fonts-style display-7">
                        Connect with your designers instantly via slack to share in your requirements and reviews.
                    </p>
                </div>
            </div>

            <div class="card p-3a col-12 col-md-6 col-lg-4">
                <div class="card-img pb-3">
                    <span class="many-iconfont mbrib-globe" style="color: rgb(127, 25, 51);" media-simple="true"></span>
                </div>
                <div class="card-box six-box" align="center">
<div class="mgb3"><img src="https://s3-ap-south-1.amazonaws.com/alltimedesign/wp-content/uploads/2018/12/28083024/Support.png"></div>
                    <h4 class="card-title py-3 many-fonts-style display-5">Customer Support</h4>
                    <p class="many-text many-fonts-style display-7">We have a dedicated team at your beck and call to help resolve your issues.</p>
                </div>
            </div>

        </div>
    </div>
</section>



<section>
<div class="container mgt4">
<div align="center" class="main-info pdb2 mgb1">FAQs</div>

<div class="media-container-row">
<div class="col-12 col-md-6 col-lg-6">
<div class="faq-ques">What type of projects do you deal with?</div>
<div class="faq-ans mgt2">All Time Design takes care of your graphic design needs like posters, brochures, infographics, social media ads and many other services. But services like complete website designing or logo making are out of <a href="<?php echo site_url('scope'); ?>">our scope</a>. To know in detail view Scope of Service.</div>
</div>

<div class="col-12 col-md-6 col-lg-6">
<div class="faq-ques mggt3">What do you mean by 14 days money back guarantee?</div>
<div class="faq-ans mgt2">For a period of two whole weeks, you can try our services and check the quality of our work for yourself. If you feel that our service is not up to the mark then you can have a refund of your money.</div>
</div>
</div>

<div class="media-container-row pdt1">
<div class="col-12 col-md-6 col-lg-6">
<div class="faq-ques mggt3">What makes you different from the others in the industry?</div>
<div class="faq-ans mgt2">The obvious answer is the quality of our work. Our designs are not just visually attractive but our team makes sure that they understand our client's requirement as well as their target's expectation to make a strong impact.</div>
</div>

<div class="col-12 col-md-6 col-lg-6">
<div class="faq-ques mggt3">How long does it take me to get on board?</div>
<div class="faq-ans mgt2">You can make a request from our dashboard as soon as you sign up.</div>
</div>
</div>

<div class="media-container-row pdt1">
<div class="col-12 col-md-6 col-lg-6">
<div class="faq-ques mggt3">How long do you take to finish a request?</div>
<div class="faq-ans mgt2">We generally finish our projects in 2 to 3 business days but keep in mind that the time taken will purely depend on the complexity of the request. Though, if you require designs urgently you can opt for our Design in a day plan</div>
</div>

<div class="col-12 col-md-6 col-lg-6">
<div class="faq-ques mggt3">What do you mean by unlimited revisions?</div>
<div class="faq-ans mgt2">You can send in as many changes as you want, until you think that your project has reached your expectation. Our team will make sure that these revisions are incorporated immediately.</div>
</div>
</div>

<div class="media-container-row pdt1">
<div class="col-12 col-md-6 col-lg-6">
<div class="faq-ques mggt3">How many requests can I send in a month?</div>
<div class="faq-ans mgt2">We don't restrict you with any number of design requests. Once we are done with your current request we are ready to move on to your next.</div>
</div>

<div class="col-12 col-md-6 col-lg-6">
<div class="faq-ques mggt3">How do I speed up the process?</div>
<div class="faq-ans mgt2">You can change the priority status of your design according to your time constraints. For instance, if you need something urgent and have a design request already lined up, you can choose to change the priority status of the new request to urgent or high.</div>
</div>
</div>

<div class="media-container-row pdt1 mgb4">
<div class="col-12 col-md-6 col-lg-6">
<div class="faq-ques mggt3">What do you suggest I do when I want my multiple projects to be completed in an instant?</div>
<div class="faq-ans mgt2">You can happily hire more <a href="<?php echo site_url('pricing-multiple-designer'); ?>">dedicated designers</a> at an affordable price to complete your never-ending projects at a breakneck pace.</div>
</div>

<div class="col-12 col-md-6 col-lg-6">
<div class="faq-ques mggt3">&nbsp;</div>
<div class="faq-ans mgt2">&nbsp;</div>
</div>
</div>

</div>
</section>
<?php get_footer(); ?>
<?php

// Function to get the client ip address
function get_client_ip_env() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}


// Function to get the client ip address
function get_client_ip_server() {
    $ipaddress = '';
    if ($_SERVER['HTTP_CLIENT_IP'])
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if($_SERVER['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if($_SERVER['HTTP_X_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if($_SERVER['HTTP_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if($_SERVER['HTTP_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if($_SERVER['REMOTE_ADDR'])
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    return $ipaddress;
}

// Get the client ip address
$ipaddress = $_SERVER['REMOTE_ADDR'];

//echo 'Your IP address (using $_SERVER[\'REMOTE_ADDR\']) is ' . $ipaddress . '<br />';
//echo 'Your IP address (using get_client_ip_env function) is ' . get_client_ip_env() . '<br />';
//echo 'Your IP address (using get_client_ip_server function) is ' . get_client_ip_server() . '<br />';
?>

<?php
//echo "bala";
if ($_SERVER['HTTP_CLIENT_IP']!="") 
{
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} 
elseif ($_SERVER['HTTP_X_FORWARDED_FOR']!="") 
{
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
else 
{
    $ip = $_SERVER['REMOTE_ADDR'];
}
//echo $ip. "<br>";

$country = getCountryFromIP($ip, " NamE ");


	//echo $country. "<br>";
	$date = date('h:i a, d M Y', time());
	//echo $date;
	 

$link = mysqli_connect("spiceblue.cyynsionfwhu.us-west-2.rds.amazonaws.com", "wordpressuser", "root12345", "wordpressdb");
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

$sql = "INSERT INTO wp_emails (email, dtime, ip, country, page) VALUES ('$get_star_email', '$date', '$ip', '$country', 'home')";

if(mysqli_query($link, $sql)){
    echo "";
} else{
    echo "";
}
?>