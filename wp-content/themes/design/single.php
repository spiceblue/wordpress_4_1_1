 <!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php wp_title(''); ?></title>
  
  <?php do_action( 'wpseo_head' );  ?>
  <link rel="shortcut icon" type="image/png" href="favicon.png"/>
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/navbar.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/default.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/additional.css" type="text/css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/popup.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/other-pages.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/base.css" data-rel-css="" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/example2.css" data-rel-css="" />
  <link href='<?php echo get_template_directory_uri(); ?>/assets/css/nprogress.css' rel='stylesheet' />
  <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/other-pages.js"></script>
  <link rel='stylesheet prefetch' href='<?php echo get_template_directory_uri(); ?>/assets/css/select2.min.css'>
  <!--slide up-->
  
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		  <link rel="stylesheet" id="dm-style-css" href="<?php echo get_template_directory_uri(); ?>/blog/css/stylesf269.css" type="text/css" media="all">
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/blog/js/jquery-3.3.1.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/blog/js/jquery-migrate-3.0.0.js"></script>

  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css" type="text/css" />
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js" type="text/javascript"></script>
  <script src="https://js.stripe.com/v3/"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/index.js" data-rel-js></script>
  <!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-P2NJWB5');</script>
	<!-- End Google Tag Manager -->
	<style>body{overflow-y: scroll !important;}
	div#selectionSharerPopover{display:none !important;}
	.background:before {
  content: '';
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background-image: linear-gradient(to bottom right, #000000, #000000);
  opacity: .6;
}

.like-txt{color: #626366; font-family:SF Pro Display; font-size: 18px;line-height: 18px;}
ul#menu1 li {
    display:inline !important;
	margin-left:0px;
	margin:10px;
}
strong.mc_error_msg {color: red; font-weight:400 !important;}
strong.mc_success_msg{color: green; font-weight:400 !important;}

.shadow-s {box-shadow: 0 1px 0 rgba(0,0,0,.05), 0 1px 2px rgba(0,0,0,.05), 0 5px 15px rgba(0,0,0,.05); width:50px !important; height:50px !important;}
.radius-round, .radius-round>img, .radius-round>video {border-radius: 50% !important; width:50px !important; height:50px !important;}
.ls1{letter-spacing:1px; text-transform:capitalize; padding-top:10px;}
.bg-date{color:#aaa!important; font-weight:300; padding-top:5px; letter-spacing:.5px;}
p.light_font {
    font-weight: 100 !important;
    font-size: 14px !important;
}
</style>
</head>
<body class="full-article-page" style="opacity:0">
 
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P2NJWB5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<section class="menu design-menu">
    <nav class="navbar navbar-expand beta-menu align-items-center navbar-fixed-top navbar-toggleable-sm bg-color transparent">
  <div class="container dis">
  
  <?php if ( is_user_logged_in() ) { 
  $current_user = wp_get_current_user();
	$customer_status = get_user_meta( $current_user->ID, 'status', true);
  
	  if ( $customer_status == 'active' ){
			echo '<div class="only-mobile">
		  <a class="btn btn-sm btn-primary display-4 btn-top" href="'.site_url('dashboard').'">Make Request</a>
		  </div>';
			
		}else{
			echo '<div class="only-mobile">
		  <a class="btn btn-sm btn-primary display-4 btn-top" href="'.site_url('pricing/').'" >Get Started</a>
		  </div>';
		}
	}else{
		echo '<div class="only-mobile">
      <a class="btn btn-sm btn-primary display-4 btn-top" href="'.site_url('pricing/').'" >Get Started</a>
      </div>';
	}
  
  ?> 
  
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-caption-wrap">
        <a class="navbar-caption text-white display-4" href="<?php echo get_home_url(); ?>">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/Logo.svg">
		</a></span>
            </div>
        </div>

 <?php if ( is_user_logged_in() ) { ?>   
<div class="collapse navbar-collapse mg2" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
      
      
	  <?php 
	  $current_user = wp_get_current_user();
	$customer_status = get_user_meta( $current_user->ID, 'status', true);
	if ( $customer_status == 'active' ){
		echo '<li class="nav-item">
      <a class="nav-link link text-black display-4" href="'.site_url('dashboard').'" aria-expanded="true">Dashboard</a>
      </li>';
		
	}else{
		echo '<li class="nav-item">
      <a class="nav-link link text-black display-4" href="'.site_url('pricing/').'" aria-expanded="true">Pricing</a>
      </li>';
	}
	  ?>
            <li class="nav-item">
      <a class="nav-link link text-black display-4" href="<?php echo site_url('works/'); ?>" aria-expanded="false">Works</a>
      </li>
      <li class="nav-item pr1">
      <a class="nav-link link text-black display-4" href="<?php echo site_url('contact/'); ?>" aria-expanded="false">Contact</a>
      </li>
      <li class="nav-item">
		<a  class="nav-link link text-black display-4 looutuser"  aria-expanded="false">Log out</a>
      </li>
      </ul>
      <?php 
		$style_prc = ( get_the_ID() == 58 ) ? 'style="visibility: hidden;"' : '';
		if ( $customer_status == 'active' ){
			echo '<div class="navbar-buttons design-section-btn" '.$style_prc.'><a class="btn btn-sm btn-primary display-4" href="'.site_url('dashboard').'">Make Request</a></div>';		
		}else{
			echo '<div class="navbar-buttons design-section-btn" '.$style_prc.' ><a class="btn btn-sm btn-primary display-4" href="'.site_url('pricing/').'" >Get Started</a></div>';
		}
	  ?>   
      
</div>
<?php } else { ?>
<div class="collapse navbar-collapse mg2" id="navbarSupportedContent">
    <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
        <li class="nav-item">
		  <a class="nav-link link text-black display-4" href="<?php echo site_url('pricing/'); ?>" aria-expanded="true">Pricing</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link link text-black display-4" href="<?php echo site_url('works/'); ?>" aria-expanded="false">Works</a>
		</li>
		<li class="nav-item pr1">
		  <a class="nav-link link text-black display-4" href="<?php echo site_url('contact/'); ?>" aria-expanded="false">Contact</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link link text-black display-4" href="<?php echo site_url('sign-in/'); ?>" aria-expanded="false">Sign in</a>
		</li>
	</ul>
	<div class="navbar-buttons design-section-btn" <?php if( get_the_ID() == 58 ){  ?> style="visibility: hidden;" <?php } ?>>
		<a class="btn btn-sm btn-primary display-4" href="<?php echo site_url('pricing/'); ?>">Get Started</a>
	</div>
</div>
<?php } ?>

</div>
</nav>
</section>


<section class="features1 design-cont">
    <div class="container">
        <div class="row" align="center">
            <div class="col-md-12 main-info">
                <?php the_title(); ?>
            </div>
			
        </div>
    </div>
</section>

<div id="author" align="center" class="mgt1">
<div class="radius-round shadow-s"><?php userphoto_the_author_thumbnail() ?></div>
<p class="ls1"><?php the_author(); ?></p>
<p class="small strip-top color-gray bg-date"><?php echo get_the_date(); ?></p>
</div>

<article>
<section>
<div class="intro__item-title navbar__hide-trigger">

<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>
<div class="post" id="post-<?php the_ID(); ?>">
	

<!-- article -->
<article class="article full-article ae-1 do">
<div class="content" style="padding-bottom:20px !important; padding-top:50px !important;">

<div class="article-content">
<p><?php the_content(); ?><br><br>
</p>
</div>
</div>
</article>

<div align="center" class="mgb1">
<span class="like-txt">Like this article? Spread the word</span><br>
<ul id="menu1" class="mgt2">
<li><a href="http://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>" onClick="window.open(this.href, 'Snopzer',
'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;"><img src="<?php echo get_template_directory_uri(); ?>/blog/Twitter_btn.png"></a></li>
<li><a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" onClick="window.open(this.href, 'Snopzer',
'left=20,top=20,width=500,height=500,toolbar=1,resizable=0'); return false;"><img src="<?php echo get_template_directory_uri(); ?>/blog/Facebook_btn.png"></a></li>
</ul>
</div>


  <?php endwhile; ?>

    <div class="navigation" align="center" style="color: cadetblue; display:none;">
      <?php previous_post_link('%link') ?> <?php next_post_link(' %link') ?>
    </div>

  <?php endif; ?>

</div>
</section>
</article>

<section style="background-color:#fafafa;">
<div class="container" align="center"><div class="media-container-row pdlr1">

<div class="col-12 col-md-12 col-lg-12">
<div class="home-title">Want more articles?</div>
<div class="call-act mgt1a" style="font-size:20px;">Stay updated on marketing and design <br>trends by subscribing to us</div>

<div id='search-box' class="sub-search">
<?php mailchimpSF_signup_form(); ?>
</div>
			
</div>

</div></div>
</section>

<section class="features1" style="background-color:#000000;">
    <div class="container">
	
	       
	    <div class="media-container-row mgb2 pdt1 pdb4">
             <div class="card col-6 col-md-3 col-lg-3">
                <a class="navbar-caption text-white display-4 mggt1" href="<?php echo get_home_url(); ?>">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/Logo_Footer.png">
		</a>
            </div>
			
			<div class="card col-6 col-md-3 col-lg-3">
                <div class="card-box four-box" >
                   <ul>
				   <li><a href="<?php echo site_url('scope/'); ?>">Scope of work</a></li>
				   <li><a href="<?php echo site_url('blog/'); ?>">Blog</a></li>
				   </ul>
                </div>
            </div>
			
			<div class="card col-6 col-md-3 col-lg-3">
                <div class="card-box four-box">
                   <ul>
				   <li><a href="<?php echo site_url('pricing/'); ?>">Pricing</a></li>
				   <li><a href="<?php echo site_url('contact/'); ?>">Contact</a></li>
				   </ul>
                </div>
            </div>			
			
            <div class="card col-6 col-md-3 col-lg-3">
                <div class="card-box four-box">
                    <ul>
				   <li><a href="<?php echo site_url('terms/'); ?>">Term & Conditions</a></li>
				   <li><a href="<?php echo site_url('privacy/'); ?>">Privacy Policy</a></li>
				   </ul>
                </div>
            </div>
			
			<!--<div class="card p-3a col-12 col-md-3 col-lg-3">
                <div class="card-box four-box">
				<div class="ft-title">Contact</div>
                    <ul>
				   <li><a href="mailto:hello@alltimedesign.com">hello@alltimedesign.com</a></li>
				   <li>+0 347 8739 943</li>
				   </ul>
                </div>
            </div>-->
			

        </div>
		
<div class="media-container-row pdb2">
<div class="card col-12 col-md-12 col-lg-12 footer-copy">
All Rights Reserved &copy; | ALL TIME DESIGN &reg; | 2018
</div>
</div>
	
    </div>
</section>


<script type="text/javascript">
            new WOW().init();
        </script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/smooth-scroll.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/script.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jarallax.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/script.js"></script>
  <script  src="<?php echo get_template_directory_uri(); ?>/assets/js/popup.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/l10n.js" data-rel-js></script>
  <?php if( get_the_ID() == 58 ){ ?>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/example2.js" data-rel-js></script>
  <?php } ?>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/js/custom_spiceblue.js"></script>  
  <script src='<?php echo get_template_directory_uri(); ?>/assets/js/nprogress.js'></script>
  

<script src='<?php echo get_template_directory_uri(); ?>/assets/js/select2.full.js'></script>
<script  src="<?php echo get_template_directory_uri(); ?>/assets/js/select2.js"></script>

<!---lightbox--->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/hes-gallery.js"></script>
  
   <script>
  jQuery(document).ready(function($){
		var baseurl = window.location.origin;
		var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
		$('.pay_form_frm').on('submit', function(e){
			e.preventDefault();
			var fd = new FormData();
			var user_id = $(this).find('input[name=user_id]');
			var user_plan = $(this).find('input[name=user_plan]');
			var stripe_token = $(this).find('input[name=stripe_token]');
			var user_id1 = user_id.val();
			var user_plan1 = user_plan.val();
			var stripe_token1 = stripe_token.val();
			fd.append("user_id", user_id1);  
			fd.append("user_plan", user_plan1);  
			fd.append("stripe_token", stripe_token1);  
			fd.append('action', 'user_payment_option');
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: fd,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('.response_msge_gts').html('');
					$('.example2 .token').html('Fetching Payment......');
				},
				success: function(response){
					var obj = jQuery.parseJSON(response);
					if(obj.status){
						$('.stripe_check_pay').hide();
						$('.response_msge_gts').html('<div class="alert alert-success"> <strong>Thank you, Payment Completed Successfully!</strong></div>');
						$('.final_response').show();
						$('.final_response .make_request #user_email_req').val(obj.msge);
					}else{
						$('.response_msge_gts').html('<div class="alert alert-danger"> <strong>'+obj.msge+'</strong></div>');
					}
					$('.example2').removeClass('submitting');
				},
				error: function(response) { 
					alert("Error occured.please try again");
				},
				complete: function() {
					
				}
			});
		})	

		$('.make_request').on('submit', function(e){
			e.preventDefault();
			var fd = new FormData();
			var file = $(this).find('input[type="file"]');
			var msge = $(this).find('textarea[name=message]');
			var subject = $(this).find('select[name=subject]');
			var email = $(this).find('input[name=email]');
			var individual_file = file[0].files[0];
			fd.append("file", individual_file);
			var message = msge.val();
			var subject = subject.val();
			var email = email.val();
			fd.append("message", message);  
			fd.append("subject", subject);  
			fd.append("email", email);  
			fd.append('action', 'make_request_ticket');
			jQuery.ajax({
				type: 'POST',
				url: ajaxurl,
				data: fd,
				contentType: false,
				processData: false,
				beforeSend: function() {					
					NProgress.start();
				},
				success: function(response){
					console.log(response);
				},
				error: function(response) { 
					alert("Error occured.please try again");
				},
				complete: function() {
					NProgress.done();
					window.location.replace(baseurl+"/dashboard/");
				}
			});
		})
  })
  jQuery(document).ready(function($){
	  var baseurl = window.location.origin;
	var logouturl =  '<?php echo admin_url( 'admin-ajax.php' ); ?>';
	$('.looutuser').on('click', function(){		
		$.ajax({
			type:'post',
			url:logouturl,
			data:{
				action:'logout_user'
			},
			success:function(data){
				window.location.replace(baseurl+"/sign-in/");
			}
		})
	});

});
  </script>
</body>
</html>    
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/blog/js/scripts.min20b9.js?ver=1.0.2'></script>